using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MCES.Domain
{
	/// <summary>
    /// Table: dbo.mces_CadreNominateDetail 
	///---------------------COLUMNS--------------------
	///CadreNominateDetailId,[P],int(4),
	///CadreNominateId,,int(4),[NULL]
	///Nominee,,varchar(25),[NULL]
	///HeadshipType,,int(4),[NULL]
	///CurWorkInfo,,varchar(255),[NULL]
	///MajorCause,,varchar(255),[NULL]
	///Direction,,varchar(255),[NULL]
    /// </summary>
	[Serializable()]
	public class CadreNominateDetail : BaseObject< CadreNominateDetail,int>
	{
		#region "Private Members"
		
		private int _cadreNominateDetailId;
		private int _cadreNominateId;
		private string _nominee;
		private int _headshipType;
		private string _curWorkInfo;
		private string _majorCause;
		private string _direction;
		
		#endregion
		
		#region "Constructors"
		
        public CadreNominateDetail()
        {
		}

        public CadreNominateDetail(int cadreNominateDetailId, int cadreNominateId , string nominee , int headshipType , string curWorkInfo , string majorCause , string direction)
		{
            this.CadreNominateDetailId = cadreNominateDetailId;
            this.CadreNominateId = cadreNominateId;
            this.Nominee = nominee;
            this.HeadshipType = headshipType;
            this.CurWorkInfo = curWorkInfo;
            this.MajorCause = majorCause;
            this.Direction = direction;
        }
		
		#endregion
		
		#region "Public Properties"
		
		/// <summary>
        ///  Primary Column:CadreNominateDetailId
        /// </summary>
		[PrimaryKey]
		public int CadreNominateDetailId
		{
			get
			{
				
				return this._cadreNominateDetailId;
			}
			set
			{   
				
				this._cadreNominateDetailId = value;
			}
		}
		
		/// <summary>
        ///  Column: CadreNominateId
        /// </summary>
		public int CadreNominateId
		{
			get
			{
				
				return this._cadreNominateId;
			}
			set
			{
			    
				this._cadreNominateId = value;
			}
		}

		/// <summary>
        ///  Column: Nominee
        /// </summary>
		public string Nominee
		{
			get
			{
				
				return this._nominee;
			}
			set
			{
			    
				this._nominee = value;
			}
		}

		/// <summary>
        ///  Column: HeadshipType
        /// </summary>
		public int HeadshipType
		{
			get
			{
				
				return this._headshipType;
			}
			set
			{
			    
				this._headshipType = value;
			}
		}

		/// <summary>
        ///  Column: CurWorkInfo
        /// </summary>
		public string CurWorkInfo
		{
			get
			{
				
				return this._curWorkInfo;
			}
			set
			{
			    
				this._curWorkInfo = value;
			}
		}

		/// <summary>
        ///  Column: MajorCause
        /// </summary>
		public string MajorCause
		{
			get
			{
				
				return this._majorCause;
			}
			set
			{
			    
				this._majorCause = value;
			}
		}

		/// <summary>
        ///  Column: Direction
        /// </summary>
		public string Direction
		{
			get
			{
				
				return this._direction;
			}
			set
			{
			    
				this._direction = value;
			}
		}
		
		#endregion
	}
}
