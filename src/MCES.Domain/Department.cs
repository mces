using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MCES.Domain
{
	/// <summary>
    /// Table: dbo.mces_Department 
	///---------------------COLUMNS--------------------
	///DepartmentId,[P],int(4),
	///Name,,varchar(50),[NULL]
	///Category,,int(4),[NULL]
	///ParentId,,int(4),[NULL]
    /// </summary>
	[Serializable()]
	public class Department : BaseObject< Department,int>
	{
		#region "Private Members"
		
		private int _departmentId;
		private string _name;
		private int _category;
		private int _parentId;
		
		#endregion
		
		#region "Constructors"
		
        public Department()
        {
		}

        public Department(int departmentId, string name , int category , int parentId)
		{
            this.DepartmentId = departmentId;
            this.Name = name;
            this.Category = category;
            this.ParentId = parentId;
        }
		
		#endregion
		
		#region "Public Properties"
		
		/// <summary>
        ///  Primary Column:DepartmentId
        /// </summary>
		[PrimaryKey]
		public int DepartmentId
		{
			get
			{
				
				return this._departmentId;
			}
			set
			{   
				
				this._departmentId = value;
			}
		}
		
		/// <summary>
        ///  Column: Name
        /// </summary>
		public string Name
		{
			get
			{
				
				return this._name;
			}
			set
			{
			    
				this._name = value;
			}
		}

		/// <summary>
        ///  Column: Category
        /// </summary>
		public int Category
		{
			get
			{
				
				return this._category;
			}
			set
			{
			    
				this._category = value;
			}
		}

		/// <summary>
        ///  Column: ParentId
        /// </summary>
		public int ParentId
		{
			get
			{
				
				return this._parentId;
			}
			set
			{
			    
				this._parentId = value;
			}
		}
		
		#endregion

        //1-机关, 2-基层
        public string CategoryName
        {
            get
            {
               return this.Category == 1 ? "机关" : "基层";
            }
        }
	}
}
