﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCES.Domain
{
    public static class EvalPlanUserState
    {
        /// <summary>
        /// 0 - 未登录
        /// </summary>
        public const int WaitLogin = 0;

        /// <summary>
        /// 1 - 登录中
        /// </summary>
        public const int LoginIn = 1;

        /// <summary>
        /// 2- 已登录
        /// </summary>
        public const int Logined = 2;
    }
}
