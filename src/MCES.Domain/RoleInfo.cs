﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCES.Domain
{
    public class RoleInfo
    {
        private static string[] keys = new string[] { "SysAdmin", "StateLeader","LeadershipMember","MidCadre","Employee"};
        private static string[] names = new string[] { "系统管理员", "市公司领导","领导班子成员","中层干部","员工"};

        /// <summary>
        /// 系统管理员
        /// </summary>
        public const string SysAdmin = "SysAdmin";

        /// <summary>
        ///  市领导
        /// </summary>
        public const string StateLeader = "StateLeader";

        /// <summary>
        ///  基层\科室领导班子成员
        /// </summary>
        public const string LeadershipMember = "LeadershipMember";

        /// <summary>
        /// 中层干部
        /// </summary>
        public const string MidCadre = "MidCadre";

        /// <summary>
        /// 一般职员
        /// </summary>
        public const string Employee = "Employee"; 
        

        /// <summary>
        /// 
        /// </summary>
        public static TextValue[] GetTextValues()
        {
            List<TextValue> tvs = new List<TextValue>();
            for (int i = 0; i < Math.Min(keys.Length, names.Length); i++)
            {
                tvs.Add(new TextValue(names[i], keys[i]));
            }
            //
            return tvs.ToArray();
        }

        public static TextValue[] GetBizTextValues()
        {
            return GetTextValues().Skip(1).ToArray();
        }

        /// <summary>
        /// 获取Role 中文名称
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static string GetDisplayName(string role)
        {
            int index = 0;
            //
            for(int i = 0 ; i < keys.Length ; i ++)
            {
                string key = keys[i];
                //
                if (string.Equals(role, key, StringComparison.InvariantCultureIgnoreCase))
                {
                    index = i;
                }
            }
            //
            return names[index];
        }
    }
}
