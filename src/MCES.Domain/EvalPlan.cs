using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MCES.Domain
{
	/// <summary>
    /// Table: dbo.mces_EvalPlan 
	///---------------------COLUMNS--------------------
	///EvalPlanId,[P],int(4),
	///Title,,varchar(50),[NULL]
	///StartDate,,datetime(8),[NULL]
	///EndDate,,datetime(8),[NULL]
	///State,,int(4),[NULL]
    /// </summary>
	[Serializable()]
	public class EvalPlan : BaseObject< EvalPlan,int>
	{
		#region "Private Members"
		
		private int _evalPlanId;
		private string _title;
		private DateTime _startDate;
		private DateTime? _endDate;
		private int _state;
		
		#endregion
		
		#region "Constructors"
		
        public EvalPlan()
        {
		}

        public EvalPlan(int evalPlanId, string title , DateTime startDate , DateTime endDate , int state)
		{
            this.EvalPlanId = evalPlanId;
            this.Title = title;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.State = state;
        }
		
		#endregion
		
		#region "Public Properties"
		
		/// <summary>
        ///  Primary Column:EvalPlanId
        /// </summary>
		[PrimaryKey]
		public int EvalPlanId
		{
			get
			{
				
				return this._evalPlanId;
			}
			set
			{   
				
				this._evalPlanId = value;
			}
		}
		
		/// <summary>
        ///  Column: Title
        /// </summary>
		public string Title
		{
			get
			{
				
				return this._title;
			}
			set
			{
			    
				this._title = value;
			}
		}

		/// <summary>
        ///  Column: StartDate
        /// </summary>
		public DateTime StartDate
		{
			get
			{
				
				return this._startDate;
			}
			set
			{
			    
				this._startDate = value;
			}
		}

		/// <summary>
        ///  Column: EndDate
        /// </summary>
		public DateTime? EndDate
		{
			get
			{
				
				return this._endDate;
			}
			set
			{
			    
				this._endDate = value;
			}
		}

		/// <summary>
        ///  Column: State
        /// </summary>
		public int State
		{
			get
			{
				
				return this._state;
			}
			set
			{
			    
				this._state = value;
			}
		}
		
		#endregion

        //------------------------------------------------------------
        public string StateName
        {
            get
            {
                switch (this.State)
                {
                    case EvalPlanState.WillStart:
                        {
                            return "等待开始";
                        }
                    case EvalPlanState.Running:
                        {
                            return "运行中";
                        }
                    case EvalPlanState.Finished:
                        {
                            return "结束";
                        }
                }
                //
                return "未知";
            }
        }
	}
}
