using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MCES.Domain
{
	/// <summary>
    /// Table: dbo.mces_CadreNominate 
	///---------------------COLUMNS--------------------
	///CadreNominateId,[P],int(4),
	///DepartmentId,,int(4),[NULL]
	///NominatorId,,uniqueidentifier(16),[NULL]
	///NominateDate,,datetime(8),[NULL]
	///State,,int(4),[NULL]
    /// </summary>
	[Serializable()]
	public class CadreNominate : BaseObject< CadreNominate,int>
	{
		#region "Private Members"
		
		private int _cadreNominateId;
		private int _departmentId;
		private Guid _nominatorId;
		private DateTime _nominateDate;
		private int _state;
		
		#endregion
		
		#region "Constructors"
		
        public CadreNominate()
        {
		}

        public CadreNominate(int cadreNominateId, int departmentId , Guid nominatorId , DateTime nominateDate , int state)
		{
            this.CadreNominateId = cadreNominateId;
            this.DepartmentId = departmentId;
            this.NominatorId = nominatorId;
            this.NominateDate = nominateDate;
            this.State = state;
        }
		
		#endregion
		
		#region "Public Properties"
		
		/// <summary>
        ///  Primary Column:CadreNominateId
        /// </summary>
		[PrimaryKey]
		public int CadreNominateId
		{
			get
			{
				
				return this._cadreNominateId;
			}
			set
			{   
				
				this._cadreNominateId = value;
			}
		}
		
		/// <summary>
        ///  Column: DepartmentId
        /// </summary>
		public int DepartmentId
		{
			get
			{
				
				return this._departmentId;
			}
			set
			{
			    
				this._departmentId = value;
			}
		}

		/// <summary>
        ///  Column: NominatorId
        /// </summary>
		public Guid NominatorId
		{
			get
			{
				
				return this._nominatorId;
			}
			set
			{
			    
				this._nominatorId = value;
			}
		}

		/// <summary>
        ///  Column: NominateDate
        /// </summary>
		public DateTime NominateDate
		{
			get
			{
				
				return this._nominateDate;
			}
			set
			{
			    
				this._nominateDate = value;
			}
		}

		/// <summary>
        ///  Column: State
        /// </summary>
		public int State
		{
			get
			{
				
				return this._state;
			}
			set
			{
			    
				this._state = value;
			}
		}
		
		#endregion
	}
}
