using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MCES.Domain
{
	/// <summary>
    /// Table: dbo.mces_EvalPlanUser 
	///---------------------COLUMNS--------------------
	///EvalPlanUserId,[P],int(4),
	///EvalPlanId,,int(4),[NULL]
	///UserId,,uniqueidentifier(16),[NULL]
    /// </summary>
	[Serializable()]
	public class EvalPlanUser : BaseObject< EvalPlanUser,int>
	{
		#region "Private Members"
		
		private int _evalPlanUserId;
		private int _evalPlanId;
		private Guid _userId;
        private int _state;
		
		#endregion
		
		#region "Constructors"
		
        public EvalPlanUser()
        {
		}

        public EvalPlanUser(int evalPlanUserId, int evalPlanId , Guid userId, int state)
		{
            this.EvalPlanUserId = evalPlanUserId;
            this.EvalPlanId = evalPlanId;
            this.UserId = userId;
            this.State = state;
        }
		
		#endregion
		
		#region "Public Properties"
		
		/// <summary>
        ///  Primary Column:EvalPlanUserId
        /// </summary>
		[PrimaryKey]
		public int EvalPlanUserId
		{
			get
			{
				
				return this._evalPlanUserId;
			}
			set
			{   
				
				this._evalPlanUserId = value;
			}
		}
		
		/// <summary>
        ///  Column: EvalPlanId
        /// </summary>
		public int EvalPlanId
		{
			get
			{
				
				return this._evalPlanId;
			}
			set
			{
			    
				this._evalPlanId = value;
			}
		}

		/// <summary>
        ///  Column: UserId
        /// </summary>
		public Guid UserId
		{
			get
			{
				
				return this._userId;
			}
			set
			{
			    
				this._userId = value;
			}
		}

        public int State
        {
            get;
            set;
        }
		
		#endregion
	}
}
