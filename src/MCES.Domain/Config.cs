using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MCES.Domain
{
	/// <summary>
    /// Table: dbo.mces_Config 
	///---------------------COLUMNS--------------------
	///ConfigKey,[P],varchar(50),
	///ConfigName,,varchar(50),[NULL]
	///ConfigValue,,varchar(4000),[NULL]
    /// </summary>
	[Serializable()]
	public class Config : BaseObject< Config,string>
	{
		#region "Private Members"
		
		private string _configKey;
		private string _configName;
		private string _configValue;
		
		#endregion
		
		#region "Constructors"
		
        public Config()
        {
		}

        public Config(string configKey, string configName , string configValue)
		{
            this.ConfigKey = configKey;
            this.ConfigName = configName;
            this.ConfigValue = configValue;
        }
		
		#endregion
		
		#region "Public Properties"
		
		/// <summary>
        ///  Primary Column:ConfigKey
        /// </summary>
		[PrimaryKey]
		public string ConfigKey
		{
			get
			{
				
				return this._configKey;
			}
			set
			{   
				if(value == null || value.Length >50)
				{
				 	string message = "set [ConfigKey] value null? or over length(50)";
	 				throw new ArgumentException(message);
				}

				this._configKey = value;
			}
		}
		
		/// <summary>
        ///  Column: ConfigName
        /// </summary>
		public string ConfigName
		{
			get
			{
				
				return this._configName;
			}
			set
			{
			    
				this._configName = value;
			}
		}

		/// <summary>
        ///  Column: ConfigValue
        /// </summary>
		public string ConfigValue
		{
			get
			{
				
				return this._configValue;
			}
			set
			{
			    
				this._configValue = value;
			}
		}
		
		#endregion
	}
}
