﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCES.Domain
{
    public static class EvalPlanState
    {
        //0-未开始, 1-评测中, (2-挂起, 3-取消), 9-结束

        /// <summary>
        /// 等待开始
        /// </summary>
        public const int WillStart = 0;

        /// <summary>
        /// 进行中
        /// </summary>
        public const int Running = 1;

        /// <summary>
        /// 结束
        /// </summary>
        public const int Finished = 9;

    }
}
