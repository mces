using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MCES.Domain
{
	/// <summary>
    /// Table: dbo.mces_UserInDepartment 
	///---------------------COLUMNS--------------------
	///UserInDepartmentId,[P],int(4),
	///DepartmentId,,int(4),[NULL]
	///UserId,,uniqueidentifier(16),[NULL]
	///Position,,int(4),[NULL]
    /// </summary>
	[Serializable()]
	public class UserInDepartment : BaseObject< UserInDepartment,int>
	{
		#region "Private Members"
		
		private int _userInDepartmentId;
		private int _departmentId;
		private Guid _userId;
		private int _position;
		
		#endregion
		
		#region "Constructors"
		
        public UserInDepartment()
        {
		}

        public UserInDepartment(int userInDepartmentId, int departmentId , Guid userId , int position)
		{
            this.UserInDepartmentId = userInDepartmentId;
            this.DepartmentId = departmentId;
            this.UserId = userId;
            this.Position = position;
        }
		
		#endregion
		
		#region "Public Properties"
		
		/// <summary>
        ///  Primary Column:UserInDepartmentId
        /// </summary>
		[PrimaryKey]
		public int UserInDepartmentId
		{
			get
			{
				
				return this._userInDepartmentId;
			}
			set
			{   
				
				this._userInDepartmentId = value;
			}
		}
		
		/// <summary>
        ///  Column: DepartmentId
        /// </summary>
		public int DepartmentId
		{
			get
			{
				
				return this._departmentId;
			}
			set
			{
			    
				this._departmentId = value;
			}
		}

		/// <summary>
        ///  Column: UserId
        /// </summary>
		public Guid UserId
		{
			get
			{
				
				return this._userId;
			}
			set
			{
			    
				this._userId = value;
			}
		}

		/// <summary>
        ///  Column: Position
        /// </summary>
		public int Position
		{
			get
			{
				
				return this._position;
			}
			set
			{
			    
				this._position = value;
			}
		}
		
		#endregion
	}
}
