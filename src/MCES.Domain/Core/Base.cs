using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace MCES.Domain
{
    /// <summary>
    /// 实体类基类
    /// <example>
    ///   例如定义Student实体类, class Student : BaseObject&lt;int,Studnet&gt;
    ///   表示Student实体类,主键类型为int
    /// </example>
    /// </summary>
    /// <typeparam name="clsType"></typeparam>
    /// <typeparam name="pkType"></typeparam>
    [Serializable()]
    [System.ComponentModel.Description("__EntityClass__")]
    public abstract class BaseObject<clsType, pkType> where clsType : BaseObject<clsType, pkType>
    {
        private Dictionary<string, object> _dynamicProps = new Dictionary<string, object>();

        /// <summary>
        /// 动态属性索引器
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual object this[string key]
        {
            get { return this._dynamicProps[key]; }
            set { this._dynamicProps[key] = value; }
        }

        /// <summary>
        /// 获得PrimaryKeyValue
        /// </summary>
        /// <returns></returns>
        private  pkType GetPrimaryKeyValue()
        {
            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty;
            foreach (PropertyInfo pi in this.GetType().GetProperties(bindingFlags))
            {
                object[] pkattribs = pi.GetCustomAttributes(typeof(PrimaryKeyAttribute), false);
                if (pkattribs.Length > 0)
                {
                    return (pkType)pi.GetValue(this, null);
                }
            }
            //
            throw new ArgumentException("Please spec PrimaryKeyAttribute to one Property.");
        }

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public sealed override bool Equals(object obj)
        {
            clsType other = obj as clsType;
            if (other == null)
            {
                return false;
            }
            //
            if (object.Equals(this.GetPrimaryKeyValue(), default(pkType)) && object.Equals(other.GetPrimaryKeyValue(), default(pkType)))
            {
                return (object)this == other;
            }
            else
            {
                return object.Equals(this.GetPrimaryKeyValue(), other.GetPrimaryKeyValue());
            }
        }

        /// <summary>
        /// GetHashCode
        /// </summary>
        /// <returns></returns>
        public sealed override int GetHashCode()
        {
            if (object.Equals(this.GetPrimaryKeyValue(), default(pkType)))
            {
                return base.GetHashCode();
            }
            else
            {
                return string.Format("{0}#{1}", typeof(clsType).AssemblyQualifiedName, this.GetPrimaryKeyValue()).GetHashCode();
            }
        }

        /// <summary>
        ///  ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}#{1}", typeof(clsType).FullName, this.GetPrimaryKeyValue());
        }
    }

    [AttributeUsage(AttributeTargets.Property,AllowMultiple=false,Inherited=false)]
    internal class PrimaryKeyAttribute : System.Attribute
    {
    }
}
