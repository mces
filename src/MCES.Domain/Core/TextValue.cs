﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCES.Domain
{
    public class TextValue
    {
        public TextValue(string text, string value)
        {
            if (string.IsNullOrEmpty(text))
            {
                throw new ArgumentOutOfRangeException("text");
            }
            //
            this.Text = text;
            this.Value = value;
        }

        public string Text
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }
    }
}
