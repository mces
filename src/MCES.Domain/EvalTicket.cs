using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MCES.Domain
{
	/// <summary>
    /// Table: dbo.mces_EvalTicket 
	///---------------------COLUMNS--------------------
	///EvalTicketId,[P],int(4),
	///EvalPlanId,,int(4),[NULL]
	///EvalUserId,,uniqueidentifier(16),[NULL]
	///TargetDepartmentId,,int(4),[NULL]
	///TargetUserId,,uniqueidentifier(16),[NULL]
	///EvalTitle,,varchar(25),[NULL]
	///EvalContent,,varchar(25),[NULL]
	///EvalLevel,,varchar(5),[NULL]
	///EvalLayer,,int(4),[NULL]
	///SelfEval,,int(4),[NULL]
	///CrtTime,,datetime(8),[NULL]
    /// </summary>
	[Serializable()]
	public class EvalTicket : BaseObject< EvalTicket,int>
	{
		#region "Private Members"
		
		private int _evalTicketId;
		private int _evalPlanId;
		private Guid? _evalUserId;
		private int? _targetDepartmentId;
		private Guid? _targetUserId;
		private string _evalTitle;
		private string _evalContent;
		private string _evalLevel;
		private int _evalLayer;
		private int _selfEval;
		private DateTime _crtTime;
		
		#endregion
		
		#region "Constructors"
		
        public EvalTicket()
        {
		}

        public EvalTicket(int evalTicketId, int evalPlanId , Guid evalUserId , int targetDepartmentId , Guid targetUserId , string evalTitle , string evalContent , string evalLevel , int evalLayer , int selfEval , DateTime crtTime)
		{
            this.EvalTicketId = evalTicketId;
            this.EvalPlanId = evalPlanId;
            this.EvalUserId = evalUserId;
            this.TargetDepartmentId = targetDepartmentId;
            this.TargetUserId = targetUserId;
            this.EvalTitle = evalTitle;
            this.EvalContent = evalContent;
            this.EvalLevel = evalLevel;
            this.EvalLayer = evalLayer;
            this.SelfEval = selfEval;
            this.CrtTime = crtTime;
        }
		
		#endregion
		
		#region "Public Properties"
		
		/// <summary>
        ///  Primary Column:EvalTicketId
        /// </summary>
		[PrimaryKey]
		public int EvalTicketId
		{
			get
			{
				
				return this._evalTicketId;
			}
			set
			{   
				
				this._evalTicketId = value;
			}
		}
		
		/// <summary>
        ///  Column: EvalPlanId
        /// </summary>
		public int EvalPlanId
		{
			get
			{
				
				return this._evalPlanId;
			}
			set
			{
			    
				this._evalPlanId = value;
			}
		}

		/// <summary>
        ///  Column: EvalUserId
        /// </summary>
		public Guid? EvalUserId
		{
			get
			{
				
				return this._evalUserId;
			}
			set
			{
			    
				this._evalUserId = value;
			}
		}

		/// <summary>
        ///  Column: TargetDepartmentId
        /// </summary>
		public int? TargetDepartmentId
		{
			get
			{
				
				return this._targetDepartmentId;
			}
			set
			{
			    
				this._targetDepartmentId = value;
			}
		}

		/// <summary>
        ///  Column: TargetUserId
        /// </summary>
		public Guid? TargetUserId
		{
			get
			{
				
				return this._targetUserId;
			}
			set
			{
			    
				this._targetUserId = value;
			}
		}

		/// <summary>
        ///  Column: EvalTitle
        /// </summary>
		public string EvalTitle
		{
			get
			{
				
				return this._evalTitle;
			}
			set
			{
			    
				this._evalTitle = value;
			}
		}

		/// <summary>
        ///  Column: EvalContent
        /// </summary>
		public string EvalContent
		{
			get
			{
				
				return this._evalContent;
			}
			set
			{
			    
				this._evalContent = value;
			}
		}

		/// <summary>
        ///  Column: EvalLevel
        /// </summary>
		public string EvalLevel
		{
			get
			{
				
				return this._evalLevel;
			}
			set
			{
			    
				this._evalLevel = value;
			}
		}

		/// <summary>
        ///  Column: EvalLayer
        /// </summary>
		public int EvalLayer
		{
			get
			{
				
				return this._evalLayer;
			}
			set
			{
			    
				this._evalLayer = value;
			}
		}

		/// <summary>
        ///  Column: SelfEval
        /// </summary>
		public int SelfEval
		{
			get
			{
				
				return this._selfEval;
			}
			set
			{
			    
				this._selfEval = value;
			}
		}

		/// <summary>
        ///  Column: CrtTime
        /// </summary>
		public DateTime CrtTime
		{
			get
			{
				
				return this._crtTime;
			}
			set
			{
			    
				this._crtTime = value;
			}
		}
		
		#endregion
	}
}
