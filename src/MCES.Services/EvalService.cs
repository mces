﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MCES.Dao;
using System.Data;
using System.Collections;

namespace MCES.Services
{
    public class EvalService : BaseService
    {
        public virtual EvalTicketDao EvalTicketDao
        {
            private get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EvalPlanId"></param>
        /// <param name="UserName"></param>
        /// <param name="RoleName"></param>
        /// <param name="DepartmentId"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        public DataTable GetMidCadresTable(int EvalPlanId, string UserName, string RoleName, int DepartmentId, Hashtable @params)
        {
            return this.EvalTicketDao.GetMidCadresTable(EvalPlanId, UserName, RoleName, DepartmentId, @params);
        }
    }
}
