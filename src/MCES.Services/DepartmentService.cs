﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MCES.Dao;
using MCES.Domain;
using System.Collections;

namespace MCES.Services
{
    public partial class DepartmentService : BaseService
    {
        public virtual DepartmentDao DepartmentDao
        {
            private get;
            set;
        }

        public virtual UserInDepartmentDao UserInDepartmentDao
        {
            private get;
            set;
        }

        /// <summary>
        /// 判断是否存在根部门
        /// </summary>
        /// <returns></returns>
        public bool HasRootDepartment()
        {
            return GetRootDepartment() != null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Department GetRootDepartment()
        {
            var @params = new Hashtable();
            @params["ParentId"] = 0;
            //
            var depts = this.DepartmentDao.Select(@params);
            return depts.Length == 1 ? depts[0] : null;
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rDeptName"></param>
        /// <returns></returns>
        public bool AddRootDepartment(string rDeptName)
        {
            Department dept = new Department();
            //1-机关, 2-基层
            dept.Category = 1;
            dept.Name = rDeptName;
            dept.ParentId = 0;
            this.DepartmentDao.Insert(dept);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Department[] GetAllSubDepartments()
        {
            var @params = new Hashtable();
            @params["ParentId"] = GetRootDepartment().DepartmentId;
            return this.DepartmentDao.Select(@params);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <param name="deptName"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public bool AddDepartment(Department root, string deptName, int category)
        {
            if (root == null) return false;
            //
            Department dept = new Department();
            dept.Name = deptName;
            dept.Category = category;
            dept.ParentId = root.DepartmentId;
            //
            try
            {
                this.DepartmentDao.Insert(dept);
            }
            catch
            {
                return false;
            }
            //
            return true;
        }

        // ---------------------------------------------------------------
        public Department[] GetSubDepartments(int StartRowIndex, int MaxRows, Hashtable @params)
        {            
            return this.DepartmentDao.GetSubDepartments(StartRowIndex, MaxRows, @params);
        }

        public int GetSubDepartmentsCount(int StartRowIndex, int MaxRows, Hashtable @params)
        {
            return this.DepartmentDao.GetSubDepartmentsCount(StartRowIndex, MaxRows, @params);
        }

        /// <summary>
        ///  
        /// </summary>
        /// <returns></returns>
        public Department[] GetAllDepartments()
        {
            var @params = new Hashtable();            
            return this.DepartmentDao.Select(@params);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public Department GetUserInDepartment(string UserName)
        {
            var @params = new Hashtable();
            return this.UserInDepartmentDao.GetUserInDepartment(null, UserName, @params);
        }
    }
}
