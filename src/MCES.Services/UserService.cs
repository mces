﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using MCES.Dao;
using System.Web.Security;
using MCES.Domain;

namespace MCES.Services
{
    public partial class UserService : BaseService
    {
        public UserDao UserDao
        {
            private get;
            set;
        }
        public UserInDepartmentDao UserInDepartmentDao
        {
            private get;
            set;
        }
        
        public DataTable GetUsers(int StartRowIndex, int MaxRows, Hashtable @params)
        {
            return this.UserDao.GetUsers(StartRowIndex, MaxRows, @params);
        }

        public int GetUsersCount(int StartRowIndex, int MaxRows, Hashtable @params)
        {
            return this.UserDao.GetUsersCount(StartRowIndex, MaxRows, @params);
        }
        //
        public bool AddUser(string userName, string password, string roleName, int departmentId)
        {
            if (Membership.FindUsersByName(userName).Count > 0)
            {
                return false;
            }
            //
            MembershipUser user = Membership.CreateUser(userName, password);
            Roles.AddUserToRole(userName, roleName);
            //
            var userInDepartment = new UserInDepartment();
            userInDepartment.UserId = (Guid)user.ProviderUserKey;
            userInDepartment.DepartmentId = departmentId;
            userInDepartment.Position = 0; //职位, 暂时都为0
            //
            this.UserInDepartmentDao.Insert(userInDepartment);

            return true;
        }
    }
}
