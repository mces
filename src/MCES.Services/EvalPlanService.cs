﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MCES.Dao;
using MCES.Domain;
using System.Data;
using System.Collections;

namespace MCES.Services
{
    public  partial class EvalPlanService : BaseService
    {
        public virtual EvalPlanDao EvalPlanDao
        {
            private get;
            set;
        }

        public virtual EvalPlanUserDao EvalPlanUserDao
        {
            private get;
            set;
        }

        //-------------------------------------------------------------------------
        public EvalPlan AddEvalPlan(string title, DateTime startDate, DateTime? endDate, int state)
        {
            EvalPlan plan = new EvalPlan();
            plan.Title = title;
            plan.StartDate = startDate;
            plan.EndDate = endDate;
            plan.State = state;
            //
            int planId = this.EvalPlanDao.AddEvalPlan(plan);
            plan.EvalPlanId = planId;
            //
            return plan;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="StartRowIndex"></param>
        /// <param name="MaxRows"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        public DataTable GetEvalPlans(int StartRowIndex, int MaxRows, Hashtable @params)
        {
            return this.EvalPlanDao.GetEvalPlans(StartRowIndex, MaxRows, @params);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="StartRowIndex"></param>
        /// <param name="MaxRows"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        public int GetEvalPlansCount(int StartRowIndex, int MaxRows, Hashtable @params)
        {
            return this.EvalPlanDao.GetEvalPlansCount(StartRowIndex, MaxRows, @params);
        }
    }
}
