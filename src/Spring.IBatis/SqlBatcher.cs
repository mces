﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using IBatisNet.Common;


namespace Spring.IBatis
{
    /// <summary>
    /// 为 Sql Server 提供批量处理操作。
    /// </summary>
    internal class SqlBatcher
    {
        private MethodInfo m_AddToBatch;
        private MethodInfo m_ClearBatch;
        private MethodInfo m_InitializeBatching;
        private MethodInfo m_ExecuteBatch;
        private IDbDataAdapter m_Adapter;
        private bool _Started;      
        private IDbProvider m_dbProvider;
       
        public SqlBatcher(IDbProvider dbProvider)
        {
            this.m_dbProvider = dbProvider;
            Init();
        }

        private void Init()
        {
            Type type = Type.GetType(string.Format("{0},{1}", this.m_dbProvider.DataAdapterClass, this.m_dbProvider.AssemblyName));
            m_AddToBatch = type.GetMethod("AddToBatch", BindingFlags.NonPublic | BindingFlags.Instance);
            m_ClearBatch = type.GetMethod("ClearBatch", BindingFlags.NonPublic | BindingFlags.Instance);
            m_InitializeBatching = type.GetMethod("InitializeBatching", BindingFlags.NonPublic | BindingFlags.Instance);
            m_ExecuteBatch = type.GetMethod("ExecuteBatch", BindingFlags.NonPublic | BindingFlags.Instance);
        }

        /// <summary>
        /// 获得批处理是否正在批处理状态
        /// </summary>
        public bool Started
        {
            get { return _Started; }
        }       

       /// <summary>
       ///  开始批命令
       /// </summary>
       /// <param name="dbConnection"></param>
        public void StartBatch(IDbConnection dbConnection)
        {
            if (_Started) return;
            //
            m_Adapter = this.m_dbProvider.CreateDataAdapter();
            m_Adapter.InsertCommand = this.m_dbProvider.CreateCommand();
            m_Adapter.InsertCommand.Connection = dbConnection;
            m_InitializeBatching.Invoke(m_Adapter, null);
            //
            _Started = true;
        }

        /// <summary>
        /// 添加批命令
        /// </summary>
        /// <param name="command">命令</param>
        public void AddToBatch(IDbCommand command)
        {
            if (!_Started) throw new InvalidOperationException();
            m_AddToBatch.Invoke(m_Adapter, new object[] { command });
        }

        /// <summary>
        /// 执行批处理。
        /// </summary>
        /// <returns>影响的数据行数</returns>
        public int ExecuteBatch()
        {
            if (!_Started) throw new InvalidOperationException();
            return (int)m_ExecuteBatch.Invoke(m_Adapter, null);
        }

        /// <summary>
        /// 结束批处理
        /// </summary>
        public void EndBatch()
        {
            if (_Started)
            {
                ClearBatch();
                m_Adapter = null;
                _Started = false;
            }
        }

        /// <summary>
        /// 清空保存的批命令
        /// </summary>
        public void ClearBatch()
        {
            if (!_Started) throw new InvalidOperationException();
            m_ClearBatch.Invoke(m_Adapter, null);
        }
    }
}