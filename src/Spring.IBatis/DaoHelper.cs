﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections;
using System.Data;
using IBatisNet.DataMapper;
using System.Diagnostics;

namespace Spring.IBatis
{
    public static class DaoHelper
    {
        static Stopwatch _stopwatch = Stopwatch.StartNew();
        //Dao Methods
        static MethodInfo QueryForObjectMethod = typeof(BaseDaoImpl).GetMethod("QueryForObject");
        static MethodInfo QueryForArrayMethod = typeof(BaseDaoImpl).GetMethod("QueryForArray");
        static MethodInfo InsertMethod = typeof(BaseDaoImpl).GetMethod("_Insert");
        static MethodInfo UpdateMethod = typeof(BaseDaoImpl).GetMethod("_Update");
        static MethodInfo DeleteMethod = typeof(BaseDaoImpl).GetMethod("_Delete");
        //Helper Methods
        static MethodInfo EnsureParametersMethod = typeof(BaseDaoImpl).GetMethod("EnsureParameters", new Type[] { typeof(Hashtable) });
        static MethodInfo HashtableItemSetMethod = typeof(Hashtable).GetProperty("Item", typeof(object)).GetSetMethod();
        static ConstructorInfo HashtableCtorMethod = typeof(Hashtable).GetConstructor(new Type[] { });
        /// <summary>
        /// 获得Dao实现
        /// </summary>
        /// <param name="daoInterfaceName"></param>
        /// <param name="sqlMapper"></param>
        /// <returns></returns>
        public static object GetDaoImpl(string daoInterfaceName, ISqlMapper sqlMapper)
        {
            //
            _stopwatch.Reset();
            _stopwatch.Start();
            //----------------------------------------------------------------
            Type daoType = GetDaoType(daoInterfaceName, string.Empty);
            object obj = CreateDaoInstance(daoType);
            BaseDaoImpl baseDao = obj as BaseDaoImpl;
            baseDao.SqlMapper = sqlMapper;
            //-----------------------------------------------------------------
            _stopwatch.Stop();
            Debug.WriteLine(string.Format("got dao type: {0} elapsed {1}ms", daoType, _stopwatch.ElapsedMilliseconds));
            //
            return obj;
        }

        private static Type GetDaoType(string _DaoInterfaceName, string _DaoAssemblyName)
        {
            //解析_DaoItfClassName
            if (string.IsNullOrEmpty(_DaoInterfaceName))
            {
                throw new Exception("DaoInterfaceName");
            }
            //
            if (_DaoInterfaceName.IndexOf(",") > 0)
            {
                string[] _cas = _DaoInterfaceName.Split(',');
                _DaoInterfaceName = _cas[0].Trim();
                _DaoAssemblyName = _cas[1].Trim();
            }
            //
            if (string.IsNullOrEmpty(_DaoAssemblyName))
            {
                throw new Exception("DaoInterfaceName");
            }
            //           
            Assembly daoAssembly = Assembly.Load(new AssemblyName(_DaoAssemblyName));
            Type daoType = daoAssembly.GetType(_DaoInterfaceName);
            //
            return daoType;
        }

        public static object CreateDaoInstance(Type daoType /*dao interface*/)
        {
            string daoTypeName = daoType.Name;
            string daoClassName = string.Format("<{0}Impl${1:N}>", daoTypeName, Guid.NewGuid());
            //
            AssemblyName name = new AssemblyName();
            name.Name = daoClassName;
            AppDomain ad = System.Threading.Thread.GetDomain();
            //Debug.WriteLine("current appDomain: " + ad);            
            AssemblyBuilder builder;
            builder = ad.DefineDynamicAssembly(name, AssemblyBuilderAccess.RunAndSave);
            //
            ModuleBuilder mb;
            mb = builder.DefineDynamicModule("<Module>");
            //
            TypeBuilder tb;
            TypeAttributes typeAttrs = TypeAttributes.Public | TypeAttributes.Class;
            tb = mb.DefineType(daoClassName, typeAttrs, typeof(BaseDaoImpl), new Type[] { daoType });
            tb.AddInterfaceImplementation(daoType);
            foreach (Type itf in daoType.GetInterfaces())
            {
                tb.AddInterfaceImplementation(itf);
            }
            //
            MethodAttributes methodAttrs = MethodAttributes.Virtual | MethodAttributes.Public;
            //
            foreach (MethodInfo mi in GetAllMethodInfos(daoType))
            {
                ParameterInfo[] pis = mi.GetParameters();
                Type[] argTypes = GetParametersTypes(pis);
                int paramsLen = pis.Length;
                bool hasRet = mi.ReturnType != typeof(void);
                //
                MethodBuilder m = tb.DefineMethod(mi.Name, methodAttrs, mi.ReturnType, argTypes);
                ILGenerator gen = m.GetILGenerator();
                LocalBuilder p = gen.DeclareLocal(typeof(Hashtable));
                LocalBuilder tmpRetVal = gen.DeclareLocal(typeof(object));
                //返回值
                LocalBuilder retVal = hasRet ? gen.DeclareLocal(mi.ReturnType) : null;

                if (IsSelectMethod(mi))
                {
                    gen.Emit(OpCodes.Ldarg_0); //this
                    gen.Emit(OpCodes.Ldstr, daoTypeName);
                    gen.Emit(OpCodes.Ldstr, mi.Name);
                    //
                    if (paramsLen == 0)
                    {
                        gen.Emit(OpCodes.Ldloc, p);

                    }
                    if (paramsLen == 1)
                    {
                        gen.Emit(OpCodes.Ldarg_1); //参数
                        if (argTypes[0].IsValueType)
                        {
                            gen.Emit(OpCodes.Box, argTypes[0]); //box
                        }
                    }
                    else
                    {
                        //若参数中最后一个参数为Hashtable
                        bool lastParamIsHashtable = EndWithHastableType(pis);
                        if (lastParamIsHashtable)
                        {
                            gen.Emit(OpCodes.Ldarg_0); //this
                            gen.Emit(OpCodes.Ldarg_S, pis.Length); //加载最后一个参数
                            gen.Emit(OpCodes.Call, EnsureParametersMethod);
                        }
                        else
                        {
                            //newobj Hashtable                  
                            gen.Emit(OpCodes.Newobj, HashtableCtorMethod);
                        }
                        //
                        //设置 @params
                        gen.Emit(OpCodes.Stloc, p); //p
                        for (int i = 0; i < (lastParamIsHashtable ? paramsLen - 1 : paramsLen); i++)
                        {
                            ParameterInfo pi = pis[i];
                            gen.Emit(OpCodes.Ldloc, p); //p
                            gen.Emit(OpCodes.Ldstr, pi.Name);
                            LoadArg(gen, i + 1);
                            //类型box
                            if (argTypes[i].IsValueType)
                            {
                                gen.Emit(OpCodes.Box, argTypes[i]);
                            }
                            //执行 p[:Key] = value;
                            gen.Emit(OpCodes.Callvirt, HashtableItemSetMethod); //p[key] = value;
                        }
                        //
                        gen.Emit(OpCodes.Ldloc, p);
                    }
                    //
                    if (mi.ReturnType.IsArray)
                    {
                        gen.Emit(OpCodes.Call, QueryForArrayMethod.MakeGenericMethod(mi.ReturnType.GetElementType()));
                    }
                    else
                    {
                        gen.Emit(OpCodes.Call, QueryForObjectMethod.MakeGenericMethod(mi.ReturnType));
                    }
                    //
                    gen.Emit(OpCodes.Stloc, retVal);
                    gen.Emit(OpCodes.Ldloc, retVal);
                    gen.Emit(OpCodes.Ret);
                }
                //

                if (IsInsertMethod(mi) || IsUpdateMethod(mi) || IsDeleteMethod(mi))
                {
                    //this , t.Name, statementName, @params
                    gen.Emit(OpCodes.Ldarg_0); //this
                    gen.Emit(OpCodes.Ldstr, daoTypeName);
                    gen.Emit(OpCodes.Ldstr, mi.Name);
                    //
                    if (paramsLen == 1)
                    {
                        gen.Emit(OpCodes.Ldarg_1); //参数
                        if (argTypes[0].IsValueType)
                        {
                            gen.Emit(OpCodes.Box, argTypes[0]); //box
                        }

                    }
                    else //paramsLen > 1
                    {
                        //若参数中最后一个参数为Hashtable
                        bool lastParamIsHashtable = EndWithHastableType(pis);
                        if (lastParamIsHashtable)
                        {
                            gen.Emit(OpCodes.Ldarg_0); //this
                            gen.Emit(OpCodes.Ldarg_S, pis.Length); //加载最后一个参数
                            gen.Emit(OpCodes.Call, EnsureParametersMethod);
                        }
                        else
                        {
                            //newobj Hashtable                  
                            gen.Emit(OpCodes.Newobj, HashtableCtorMethod);
                        }
                        //
                        //设置 @params
                        gen.Emit(OpCodes.Stloc, p); //p
                        for (int i = 0; i < (lastParamIsHashtable ? paramsLen - 1 : paramsLen); i++)
                        {
                            ParameterInfo pi = pis[i];
                            gen.Emit(OpCodes.Ldloc, p); //p
                            gen.Emit(OpCodes.Ldstr, pi.Name);
                            LoadArg(gen, i + 1);
                            //类型box
                            if (argTypes[i].IsValueType)
                            {
                                gen.Emit(OpCodes.Box, argTypes[i]);
                            }
                            //执行 p[:Key] = value;
                            gen.Emit(OpCodes.Callvirt, HashtableItemSetMethod); //p[key] = value;
                        }
                        //
                        gen.Emit(OpCodes.Ldloc, p);
                    }
                    //                 
                    //调用方法
                    if (IsInsertMethod(mi))
                    {
                        gen.Emit(OpCodes.Call, InsertMethod);
                    }
                    else if (IsUpdateMethod(mi))
                    {
                        gen.Emit(OpCodes.Call, UpdateMethod);
                    }
                    else if (IsDeleteMethod(mi))
                    {
                        gen.Emit(OpCodes.Call, DeleteMethod);
                    }
                    //设置返回值
                    gen.Emit(OpCodes.Stloc, tmpRetVal);

                    //函数返回
                    if (mi.ReturnType != typeof(void))
                    {
                        gen.Emit(OpCodes.Ldloc, tmpRetVal);
                        //
                        if (mi.ReturnType.IsValueType)
                        {
                            gen.Emit(OpCodes.Unbox, mi.ReturnType);
                            gen.Emit(OpCodes.Ldobj, mi.ReturnType);
                        }
                        else if (mi.ReturnType.IsClass)
                        {
                            gen.Emit(OpCodes.Castclass, mi.ReturnType);
                        }
                        //
                        gen.Emit(OpCodes.Stloc, retVal);
                        gen.Emit(OpCodes.Ldloc, retVal);
                    }
                    //
                    gen.Emit(OpCodes.Ret);
                }
            }
            //
            Type implType = tb.CreateType();
            object ins = Activator.CreateInstance(implType);
            return ins;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gen"></param>
        /// <param name="index"></param>
        private static void LoadArg(ILGenerator gen, int index)
        {
            switch (index)
            {
                case 0:
                    gen.Emit(OpCodes.Ldarg_0);
                    break;
                case 1:
                    gen.Emit(OpCodes.Ldarg_1);
                    break;
                case 2:
                    gen.Emit(OpCodes.Ldarg_2);
                    break;
                case 3:
                    gen.Emit(OpCodes.Ldarg_3);
                    break;
                default:
                    if (index < 128)
                    {
                        gen.Emit(OpCodes.Ldarg_S, index);
                    }
                    else
                    {
                        gen.Emit(OpCodes.Ldarg, index);
                    }
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pis"></param>
        /// <returns></returns>
        private static Type[] GetParametersTypes(ParameterInfo[] pis)
        {
            List<Type> paramsTypes = new List<Type>();
            //
            foreach (ParameterInfo pi in pis)
            {
                paramsTypes.Add(pi.ParameterType);
            }
            //
            return paramsTypes.ToArray();
        }

        private static bool EndWithHastableType(ParameterInfo[] pis)
        {
            return pis[pis.Length - 1].ParameterType == typeof(Hashtable);
        }

        public static MethodInfo[] GetAllMethodInfos(Type daoType)
        {
            List<MethodInfo> methodInfos = new List<MethodInfo>();
            methodInfos.AddRange(daoType.GetMethods());
            foreach (Type itf in daoType.GetInterfaces())
            {
                methodInfos.AddRange(itf.GetMethods());
            }
            //
            return methodInfos.ToArray();
        }

        #region MethodInfo 名称属性判断

        private static bool IsSelectMethod(MethodInfo mi)
        {
            string name = mi.Name.ToLower();
            return (name.StartsWith("get")
                    || name.StartsWith("find")
                    || name.StartsWith("fetch")
                    || name.StartsWith("select")
                    || name.StartsWith("list"));
        }

        private static bool IsInsertMethod(MethodInfo mi)
        {
            string name = mi.Name.ToLower();
            return (name.StartsWith("add")
                    || name.StartsWith("insert")
                    || name.StartsWith("save"));
        }

        private static bool IsUpdateMethod(MethodInfo mi)
        {
            string name = mi.Name.ToLower();
            return (name.StartsWith("update") || name.StartsWith("set"));
        }

        private static bool IsDeleteMethod(MethodInfo mi)
        {
            string name = mi.Name.ToLower();
            return (name.StartsWith("delete") || name.StartsWith("remove") || name.StartsWith("clear"));
        }

        #endregion
    }
}
