using System;
using System.Collections.Generic;
using System.Text;
using Spring.Objects.Factory.Config;
using System.Reflection;
using System.Collections;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using Spring.Objects.Factory;
using System.IO;
using System.Data;

namespace Spring.IBatis
{
    /// <summary>
    /// Dao实现体工厂对象
    /// </summary>
    public class DaoImplFactoryObject 
    {
        /// <summary>
        /// 获得Dao实现
        /// </summary>
        /// <param name="daoInterfaceName"></param>
        /// <param name="sqlMapper"></param>
        /// <returns></returns>
        public static object GetDaoImpl(string daoInterfaceName, IBatisNet.DataMapper.ISqlMapper sqlMapper)
        {
            //Type _DaoImplType = GenerateDaoImpl(daoInterfaceName, string.Empty);
            //object daoImplObj = Activator.CreateInstance(_DaoImplType);
            //((__BaseDao)daoImplObj).SqlMapper = sqlMapper;
            //return daoImplObj;
            return DaoHelper.GetDaoImpl(daoInterfaceName, sqlMapper);
        }

        protected static Type GenerateDaoImpl(string _DaoInterfaceName, string _DaoAssemblyName)
        {
            //
            //解析_DaoItfClassName
            if (string.IsNullOrEmpty(_DaoInterfaceName))
            {
                throw new Exception("DaoInterfaceName");
            }
            //
            if (_DaoInterfaceName.IndexOf(",") > 0)
            {
                string[] _cas = _DaoInterfaceName.Split(',');
                _DaoInterfaceName = _cas[0].Trim();
                _DaoAssemblyName = _cas[1].Trim();
            }
            //
            if (string.IsNullOrEmpty(_DaoAssemblyName))
            {
                throw new Exception("DaoInterfaceName");
            }
            //
            Assembly daoAssembly = Assembly.Load(new AssemblyName(_DaoAssemblyName));
            Type _DaoImplType = DaoImplBuilder.CreateDaoImplType(_DaoInterfaceName, _DaoAssemblyName, typeof(__BaseDao));
            return _DaoImplType;
        }
    }

    #region DaoImplBuilder Class

    static class DaoImplBuilder
    {
        public static Type CreateDaoImplType(string daoInterfaceName, string daoAssemblyName, Type daoBaseType)
        {
            int index = daoInterfaceName.LastIndexOf(".");
            string daoImpl = daoInterfaceName.Insert(index + 1, "__Impl_");
            Type daoImplType = Type.GetType(daoImpl);
            if (daoImplType == null)
            {
                Assembly _assembly = CreateAssembly(daoInterfaceName, daoAssemblyName, daoBaseType);
                daoImplType = _assembly.GetType(daoImpl);
            }
            //
            return daoImplType;
        }

        public static Type GetDaoInterfaceType(string daoInterfaceName, string daoAssemblyName)
        {
            Assembly daoAssembly = Assembly.Load(new AssemblyName(daoAssemblyName));
            Type daoType = daoAssembly.GetType(daoInterfaceName);
            return daoType;
        }

        //---------------------------------------------------------------------------------------------------
        private static Assembly CreateAssembly(string daoInterfaceName, string daoAssemblyName, Type daoBaseType)
        {
            Hashtable needRefers = new Hashtable();
            //
            Type daoType = GetDaoInterfaceType(daoInterfaceName, daoAssemblyName);
            string methodImpl = GenerateDaoMethodSource(needRefers, daoType, daoBaseType);
            System.Diagnostics.Trace.Write(methodImpl);
            //
            string baseDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
            CodeDomProvider complier = CSharpCodeProvider.CreateProvider("C#");
            CompilerParameters cps = new CompilerParameters();            
            cps.GenerateExecutable = false;
            cps.GenerateInMemory = true;
            cps.CompilerOptions = "/optimize";
            //            
            cps.ReferencedAssemblies.Add("System.dll");
            cps.ReferencedAssemblies.Add("System.Data.dll");
            cps.ReferencedAssemblies.Add("System.Xml.dll");
            cps.ReferencedAssemblies.Add(Path.Combine(baseDir, "IBatisNet.DataMapper.dll"));
            cps.ReferencedAssemblies.Add(Path.Combine(baseDir,"Spring.Core.dll"));
            cps.ReferencedAssemblies.Add(Path.Combine(baseDir,"Spring.Data.dll"));           
            TryAppendReferences(cps, baseDir, needRefers);
            //
            CompilerResults cr = complier.CompileAssemblyFromSource(cps, methodImpl);        
            if (cr.Errors.HasErrors)
            {
                StringBuilder exmsg = new StringBuilder("Compile Error:\r\n");
                foreach (CompilerError ce in cr.Errors)
                {
                    System.Diagnostics.Trace.WriteLine(ce.ErrorText);
                    exmsg.Append(ce.ErrorText);
                }
                //
                throw new Exception(exmsg.ToString());
            }
            //
            Assembly assembly = cr.CompiledAssembly;
            return assembly;
        }

        private static string GenerateDaoMethodSource(Hashtable needRefers, Type daoType, Type daoBaseType)
        {
            needRefers[daoType] = daoType;
            //
            string extendTypes = daoBaseType == null ? string.Format(": {0}",daoType) : string.Format(" : {0},{1}", daoBaseType,daoType);
            if (!string.IsNullOrEmpty(extendTypes))
            {
                needRefers[daoBaseType] = daoBaseType;
            }
            //
            string methodBody = GenerateDaoMethodBodyImplSource(needRefers, daoType);
            //
            string namespaces = TryGetUsingNamespaces(needRefers);
            //
            StringBuilder _method = new StringBuilder();
            _method.Append(namespaces).Append(Environment.NewLine);
            _method.Append(string.Format("namespace {0} {{\n", daoType.Namespace));
            _method.Append(string.Format("public sealed class __Impl_{0} {1}\n", daoType.Name, extendTypes));
            _method.Append("{\n").Append(methodBody).Append("\n}};");
            //
            return _method.ToString();
        }

        private static string TryGetUsingNamespaces(Hashtable needRefers)
        {
            StringBuilder ns = new StringBuilder("\n");
            Hashtable _usedNS = new Hashtable();
            //
            foreach (Type type in needRefers.Keys)
            {
                string uns = type.Namespace;
                if (!_usedNS.ContainsKey(uns))
                {
                    _usedNS.Add(uns, uns);
                    ns.Append(string.Format("using {0};", uns)).Append(Environment.NewLine);
                }
            }
            //
            return ns.ToString();
        }

        private static void TryAppendReferences(CompilerParameters cps, string baseDir, Hashtable needRefers)
        {
            foreach (Type type in needRefers.Keys)
            {
                if (!cps.ReferencedAssemblies.Contains(type.Assembly.FullName))
                {
                    if (!type.Assembly.GlobalAssemblyCache)
                    {
                        //"mscorlib"
                        string dllName = string.Format("{0}.dll", type.Assembly.FullName.Split(',')[0]);
                        string dllPath = Path.Combine(baseDir, dllName);
                        cps.ReferencedAssemblies.Add(dllPath);
                    }
                }
            }
        }

        private static string GenerateDaoMethodBodyImplSource(Hashtable needRefers, Type daoType)
        {
            StringBuilder methodImpl = new StringBuilder();
            List<MethodInfo> mis = new List<MethodInfo>();
            mis.AddRange(daoType.GetMethods());
            foreach (Type intf in daoType.GetInterfaces())
            {
                mis.AddRange(intf.GetMethods());
            }

            foreach (MethodInfo mi in mis)
            {
                if (IsSelect(mi))
                {
                    methodImpl.Append(GenerateSelectMethod(daoType,needRefers, mi));
                }
                else if (IsInsert(mi))
                {
                    methodImpl.Append(GenerateInsertMethod(daoType,needRefers, mi));
                }
                else if (IsUpdate(mi))
                {
                    methodImpl.Append(GenerateUpdateMethod(daoType,needRefers, mi));
                }
                else if (IsDelete(mi))
                {
                    methodImpl.Append(GenerateDeleteMethod(daoType,needRefers, mi));
                }
            }
            //
            return methodImpl.ToString();
        }

        private static bool IsSelect(MethodInfo mi)
        {
            string name = mi.Name.ToLower();
            return (   name.StartsWith("get") 
                    || name.StartsWith("find") 
                    || name.StartsWith("fetch") 
                    || name.StartsWith("select") 
                    || name.StartsWith("list"));
        }
        private static string GenerateSelectMethod(Type daoType,Hashtable needRefers, MethodInfo mi)
        {
            StringBuilder _methodImpl = new StringBuilder();
            string mns = GenerateMethodNameString(mi);
            _methodImpl.Append(mns).Append(Environment.NewLine).Append("{").Append(Environment.NewLine);
            //
            Type rtnType = mi.ReturnType;
            string s0 = GenerateMethodParametersString(needRefers, mi);
            _methodImpl.Append(s0);
            _methodImpl.Append(Environment.NewLine);

            string stm = string.Format("string stmId = SqlMapper.MappedStatements.Contains(\"{0}\") ? \"{0}\" : \"{1}.{0}\";\n", mi.Name, daoType.Name);
            //(0):
            //Primitive: Boolean、Byte、SByte、Int16、UInt16、Int32、UInt32、Int64、UInt64、
            //Char、Double 和 Single
            //string
            if (rtnType.IsPrimitive || rtnType.Equals(typeof(string)))
            {
                //this.SqlMapper.QueryForObject<T>(statementId, p0);
                string s1 = string.Format("{0} return this.SqlMapper.QueryForObject<{1}>(stmId,p0);", stm, rtnType);
                _methodImpl.Append(s1).Append(Environment.NewLine);
                goto __END;
            }

			 //(0-1)
            // NOT{基础类型,泛型类，数组，枚举，值类,DataTable} AND {类}
           /* if (!rtnType.IsPrimitive && !rtnType.IsGenericType && !rtnType.IsArray && !rtnType.IsEnum 
                && !rtnType.IsValueType
                && !rtnType.Equals(typeof(DataTable))
                && rtnType.IsClass)
            */
            if(IsEntityClassType(rtnType))
            {
                string s1 = string.Format("{0} return this.SqlMapper.QueryForObject<{1}>(stmId,p0);", stm, rtnType);
                _methodImpl.Append(s1).Append(Environment.NewLine);
                goto __END;
            }

            //(1): IList<T>
            if (rtnType.IsGenericType && rtnType.GetGenericTypeDefinition().Equals(typeof(IList<>)))
            {
                Type[] gargs = rtnType.GetGenericArguments();
                //
                needRefers[typeof(IList<>)] = typeof(IList<>);
                needRefers[gargs[0]] = gargs[0];
                //              
                string s1 = string.Format("{0} return this.SqlMapper.QueryForList<{1}>(stmId,p0);", stm,gargs[0]);
                _methodImpl.Append(s1).Append(Environment.NewLine);
                goto __END;
            }

            //(2): IDictionary<K, V> 
            if (rtnType.IsGenericType && rtnType.GetGenericTypeDefinition().Equals(typeof(IDictionary<,>)))
            {
                Type[] gargs = rtnType.GetGenericArguments();
                needRefers[typeof(IDictionary<,>)] = typeof(IDictionary<,>);
                needRefers[gargs[0]] = gargs[0];
                needRefers[gargs[1]] = gargs[1];
                //
                string s1 = string.Format("{0} return this.SqlMapper.QueryForDictionary<{1},{2}>(stmId, p0, \"key\", \"value\");",
                   stm, gargs[0], gargs[1]);
                _methodImpl.Append(s1).Append(Environment.NewLine);
                goto __END;
            }

            //(3): Array
            if (rtnType.IsArray)
            {
                Type eleType = rtnType.GetElementType();
                needRefers[eleType] = eleType;
                //
                string s1 = string.Format("System.Collections.Generic.IList<{0}> r0 = this.SqlMapper.QueryForList<{0}>(stmId,p0);", eleType);
                string s2 = string.Format("System.Collections.Generic.List<{0}> r1 = new System.Collections.Generic.List<{0}>();", eleType);
                string s3 = "r1.AddRange(r0);return r1.ToArray();";
                _methodImpl.Append(stm).Append(Environment.NewLine);
                _methodImpl.Append(s1).Append(Environment.NewLine);
                _methodImpl.Append(s2).Append(Environment.NewLine);
                _methodImpl.Append(s3).Append(Environment.NewLine);
                goto __END;
            }

            //(4): System.Data.DataTable
            if (rtnType.Equals(typeof(System.Data.DataTable)))
            {
                string s1 = string.Format("{0} return this.SqlMapper.QueryForDataTable(stmId,p0);",stm, rtnType);
                _methodImpl.Append(s1).Append(Environment.NewLine);
                goto __END;
            }

            //(5): System.Void
            if (rtnType.Equals(typeof(void)))
            {
                _methodImpl.Append("return;");
                goto __END;
            }

            //对其他返回类型不支持
            throw new NotSupportedException(string.Format("return type:{0}", rtnType));

            //
        __END:
            _methodImpl.Append(Environment.NewLine).Append("}").Append(Environment.NewLine);
            return _methodImpl.ToString();
        }

        /// <summary>
        /// 判断是否为 自定义实体类 System.ComponentModel.DescriptionAttribute && Description == "__EntityClass__"
        /// </summary>
        /// <param name="rtnType"></param>
        /// <returns></returns>
        private static bool IsEntityClassType(Type rtnType)
        {
            object[] attrs = rtnType.GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), true);
            foreach (Attribute var in attrs)
            {
                System.ComponentModel.DescriptionAttribute descAttr = var as System.ComponentModel.DescriptionAttribute;
                if (descAttr != null)
                {
                    return string.Equals(descAttr.Description, "__EntityClass__");
                }
            }
            //
            return false;
        }

        private static bool IsInsert(MethodInfo mi)
        {
            string name = mi.Name.ToLower();
            return (name.StartsWith("add") || name.StartsWith("insert") || name.StartsWith("save"));
        }

        private static string GenerateInsertMethod(Type daoType, Hashtable needRefers, MethodInfo mi)
        {
            StringBuilder _methodImpl = new StringBuilder();
            string mns = GenerateMethodNameString(mi);
            _methodImpl.Append(mns).Append(Environment.NewLine).Append("{").Append(Environment.NewLine);
            //
            string stm = string.Format("string stmId = SqlMapper.MappedStatements.Contains(\"{0}\") ? \"{0}\" : \"{1}.{0}\";\n", mi.Name, daoType.Name);
            //
            Type rtnType = mi.ReturnType;
            string s0 = GenerateMethodParametersString(needRefers, mi);
            _methodImpl.Append(s0);
            _methodImpl.Append(Environment.NewLine);
            //
            string s1 = null;
            //
            if (IsBatchInsertParameters(needRefers, mi))
            {
                s1 = string.Format("{0} this.BatchExecute(stmId,p0 as System.Collections.IEnumerable);", stm);
            }
            else
            { 
                s1 = string.Format("{0} this.SqlMapper.Insert(stmId,p0);", stm);
            }
            //
            _methodImpl.Append(s1).Append("\n}").Append(Environment.NewLine);
            return _methodImpl.ToString();
        }

        /// <summary>
        /// 判断是否 批插入操作
        /// </summary>
        /// <param name="needRefers"></param>
        /// <param name="mi"></param>
        /// <returns></returns>
        private static bool IsBatchInsertParameters(Hashtable needRefers, MethodInfo mi)
        {
            ParameterInfo[] pis = mi.GetParameters();
            if (pis.Length == 1)
            {
                Type paramType =  pis[0].ParameterType;
                needRefers[pis[0].ParameterType] = paramType;
                //
                //now only support Array
                if (paramType.IsArray)
                {
                    return true;
                }
                //若paramType为 Hashtable, implemented IEnumerable inteface
                //if(typeof(IEnumerable).IsAssignableFrom(paramType))
                //{
                //    
                //}
            }
            //
            return false;
        }

        private static bool IsUpdate(MethodInfo mi)
        {
            string name = mi.Name.ToLower();
            return (name.StartsWith("update"));
        }

        private static string GenerateUpdateMethod(Type daoType, Hashtable needRefers, MethodInfo mi)
        {
            StringBuilder _methodImpl = new StringBuilder();
            string mns = GenerateMethodNameString(mi);
            _methodImpl.Append(mns).Append(Environment.NewLine).Append("{").Append(Environment.NewLine);
            //
            string stm = string.Format("string stmId = SqlMapper.MappedStatements.Contains(\"{0}\") ? \"{0}\" : \"{1}.{0}\";\n", mi.Name, daoType.Name);
            //
            Type rtnType = mi.ReturnType;
            string s0 = GenerateMethodParametersString(needRefers, mi);
            _methodImpl.Append(s0);
            _methodImpl.Append(Environment.NewLine);
            string s1 = string.Format("{0} this.SqlMapper.Update(stmId,p0);", stm);
            _methodImpl.Append(s1).Append("\n}").Append(Environment.NewLine);
            return _methodImpl.ToString();
        }


        private static bool IsDelete(MethodInfo mi)
        {
            string name = mi.Name.ToLower();
            return (name.StartsWith("delete") || name.StartsWith("remove"));
        }

        private static string GenerateDeleteMethod(Type daoType, Hashtable needRefers, MethodInfo mi)
        {
            StringBuilder _methodImpl = new StringBuilder();
            string mns = GenerateMethodNameString(mi);
            _methodImpl.Append(mns).Append(Environment.NewLine).Append("{").Append(Environment.NewLine);
            //
            string stm = string.Format("string stmId = SqlMapper.MappedStatements.Contains(\"{0}\") ? \"{0}\" : \"{1}.{0}\";\n", mi.Name, daoType.Name);
            //
            Type rtnType = mi.ReturnType;
            string s0 = GenerateMethodParametersString(needRefers, mi);
            _methodImpl.Append(s0);
            _methodImpl.Append(Environment.NewLine);
            string s1 = string.Format("{0} this.SqlMapper.Delete(stmId,p0);",stm);
            _methodImpl.Append(s1).Append("\n}").Append(Environment.NewLine);
            return _methodImpl.ToString();
        }

        private static string GenerateMethodParametersString(Hashtable needRefers, MethodInfo mi)
        {
            ParameterInfo[] pis = mi.GetParameters();
            string s0 = "object p0 = {0};";
            //
            switch (pis.Length)
            {
                case 0:
                    {
                        s0 = string.Format(s0, "null");

                    } break;
                case 1:
                    {
                        needRefers[pis[0].ParameterType] = pis[0].ParameterType;
                        s0 = string.Format(s0, (pis[0].Name == "params" ? "@params" : pis[0].Name));

                    } break;
                default:
                    {
                        StringBuilder _sb = new StringBuilder();
                        _sb.Append("System.Collections.Hashtable _p = null;");
                        _sb.Append(Environment.NewLine);
                        bool lastParaeIsHashtable = false;
                        //若最后一个参数为 @params 且为Hashtable 则(0,... N-1)参数加入至@params中
                        ParameterInfo _pi = pis[pis.Length - 1];
                        if (_pi.ParameterType == typeof(Hashtable) && _pi.Name == "params")
                        {
                            _sb.Append(Environment.NewLine).Append("_p = @params;");
                            lastParaeIsHashtable = true;
                        }
						//
						_sb.Append(Environment.NewLine);
                        _sb.Append("_p = (_p == null) ? new System.Collections.Hashtable() : _p;");
                        _sb.Append(Environment.NewLine);

                        for (int i = 0; i < (lastParaeIsHashtable ? pis.Length -1 : pis.Length); i++)
                        {
                            ParameterInfo pi = pis[i];
                            needRefers[pi.ParameterType] = pi.ParameterType;                            
                            _sb.Append(Environment.NewLine).Append(string.Format("_p[\"{0}\"] = {0};", pi.Name));
                            _sb.Append(Environment.NewLine);
                        }                     
                       
                        //
                        s0 = _sb.ToString() + Environment.NewLine + string.Format(s0, "_p");
                        s0 += Environment.NewLine + "System.Diagnostics.Trace.Write(p0);";

                    } break;
            }
            //
            return s0;
        }

        private static string GenerateMethodNameString(MethodInfo mi)
        {
            string piName = string.Empty;
            //
            StringBuilder _pis = new StringBuilder();
            foreach (ParameterInfo pi in mi.GetParameters())
            {
                piName = (pi.Name == "params") ? "@params" : pi.Name;
                _pis.Append(string.Format("{0} {1}", pi.ParameterType, piName)).Append(",");
            }
            string @params = _pis.Length > 0 ? _pis.Remove(_pis.Length - 1, 1).ToString() : string.Empty;
            //
            string retTypeString = string.Empty;
            retTypeString = mi.ReturnType == typeof(void) ? "void" : mi.ReturnType.ToString();
            //
            if (mi.ReturnType.IsGenericType)
            {
                string fn = mi.ReturnType.FullName;
                List<string> argTypes = new List<string>();
                foreach (Type argType in mi.ReturnType.GetGenericArguments())
                {
                    argTypes.Add(argType.ToString());
                }
                //
                retTypeString = string.Format("{0}<{1}>", fn.Substring(0, fn.IndexOf("`")), string.Join(",", argTypes.ToArray()));
            }
            //
            string methodNameString = string.Format("public {0} {1}({2})", retTypeString, mi.Name, @params);
            return methodNameString;
        }

    }
    //
    #endregion   
}
