﻿using System;
using System.Collections.Generic;
using System.Text;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.MappedStatements;
using IBatisNet.DataMapper.Configuration.Statements;
using IBatisNet.DataMapper.Commands;
using IBatisNet.DataMapper.Scope;
using System.Collections;
using System.Reflection;
using System.Data;

namespace Spring.IBatis
{
    #region BaseDao 基础数据访问类

    /// <summary>
    /// BaseDao 基础数据访问类
    /// </summary>
    public abstract class __BaseDao
    {
        private IBatisNet.DataMapper.ISqlMapper _sqlMapper;
        private SqlBatcher m_Batcher = null;
        /// <summary>
        /// SqlMapper Property
        /// </summary>
        public IBatisNet.DataMapper.ISqlMapper SqlMapper
        {
            get
            {
                return this._sqlMapper;
            }
            set
            {
                this._sqlMapper = value;                
            }
        }

        /// <summary>
        /// 批处理
        /// </summary>
        /// <param name="statementId"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        protected int BatchExecute(string statementId, IEnumerable @params)
        {
            int ret = 0;
            //
            if (this.m_Batcher == null)
            {
                this.m_Batcher = new SqlBatcher(this.SqlMapper.DataSource.DbProvider);
            }
            //

            ISqlMapSession session = this.SqlMapper.OpenConnection();          
            IMappedStatement mappedStmt = this.SqlMapper.GetMappedStatement(statementId);
            IStatement stmt = mappedStmt.Statement;
            IPreparedCommand preCmd = mappedStmt.PreparedCommand;
            //
            object param0 = null;
            IEnumerator etor = @params.GetEnumerator();
            while (etor.MoveNext())
            {
               param0 = etor.Current;
               if (param0 != null)
               {
                   break;
               }
            }
                //
            if (param0 != null)
            {
               RequestScope request = stmt.Sql.GetRequestScope(mappedStmt, param0, session);
               @params.GetEnumerator().Reset();
               //
               m_Batcher.StartBatch(session.Connection);
               foreach (object param in @params)
               {
                   if (param == null) continue;
                   preCmd.Create(request, session, stmt, param);
                   m_Batcher.AddToBatch(GetInnerCommand(request.IDbCommand));
               }
               //
               session.OpenConnection();
               ret = m_Batcher.ExecuteBatch();
               m_Batcher.EndBatch();
               //session.CloseConnection(); //不需要手动维护
            }     
            //
            return ret;
        }


        //由于IBatisNet 用 DbCommandDecorator 包装了 DbCommand，因此必须使用反射取得该字段
        private static readonly FieldInfo m_InnerCommandField = typeof(DbCommandDecorator).GetField("_innerDbCommand", BindingFlags.Instance | BindingFlags.NonPublic);

        // 转化 IBatis 包装后的 DbCommand 为 原始的 DbCommand
        private static IDbCommand GetInnerCommand(IDbCommand ibatCommand)
        {
            return (IDbCommand)m_InnerCommandField.GetValue(ibatCommand);
        }
    }

    #endregion
}
