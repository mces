﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using IBatisNet.DataMapper;
using System.Data;
using System.Collections;
using IBatisNet.DataMapper.MappedStatements;
namespace Spring.IBatis
{
    public abstract class BaseDaoImpl
    {
        public ISqlMapper SqlMapper
        {
            get;
            set;
        }

        #region Select Methods

        public T QueryForObject<T>(string daoClassName, string statementName, object @params)
        {
            string _stmtName = EnsureStatementName(daoClassName, statementName);
            //
            if (typeof(T) == typeof(DataTable))
            {
                return (T)(object)this.SqlMapper.QueryForDataTable(_stmtName, @params);
            }
            else
            {
                return this.SqlMapper.QueryForObject<T>(_stmtName, @params);
            }
            //return default(T);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="daoClassName"></param>
        /// <param name="statementName"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        public T[] QueryForArray<T>(string daoClassName, string statementName, object @params)
        {
            string _stmtName = EnsureStatementName(daoClassName, statementName);
            IList<T> list = this.SqlMapper.QueryForList<T>(_stmtName, @params);
            return new List<T>((IEnumerable<T>)list).ToArray();
            //return new T[] { };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="daoClassName"></param>
        /// <param name="statementName"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        public object _Insert(string daoClassName, string statementName, object @params)
        {
            string _stmtName = EnsureStatementName(daoClassName, statementName);
            return this.SqlMapper.Insert(_stmtName, @params);
            //return (object)0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="daoClassName"></param>
        /// <param name="statementName"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        public object _Update(string daoClassName, string statementName, object @params)
        {
            string _stmtName = EnsureStatementName(daoClassName, statementName);
            return this.SqlMapper.Update(_stmtName, @params);
            //return (object)0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="daoClassName"></param>
        /// <param name="statementName"></param>
        /// <param name="params"></param>
        /// <returns></returns>
        public object _Delete(string daoClassName, string statementName, object @params)
        {
            string _stmtName = EnsureStatementName(daoClassName, statementName);
            return this.SqlMapper.Delete(_stmtName, @params);
            //return (object)0;
        }
        #endregion

        #region Helper Methods

        public string EnsureStatementName(string daoClassName, string statementName)
        {
            string _statementName = string.Concat(daoClassName, ".", statementName);
            //
            //先检测 daoClassName.statementName
            IMappedStatement stmt = this.SqlMapper.GetMappedStatement(_statementName);
            if (stmt == null)
            {
                _statementName = statementName;
                stmt = this.SqlMapper.GetMappedStatement(_statementName);
                if (stmt == null)
                {
                    string msg = string.Format("not found ibatis mappedstatement of {0} or {1}.{0} ", statementName, daoClassName);
                    throw new ArgumentException(msg);
                }
            }
            //
            return _statementName;
        }

        public Hashtable EnsureParameters(Hashtable @params)
        {
            return @params == null ? new Hashtable() : @params;
        }

        #endregion
    }
}
