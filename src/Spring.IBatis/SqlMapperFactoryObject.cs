#region License

/*
 * Copyright © 2002-2005 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#endregion

#region Imports

using System;
using System.IO;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;

#endregion

namespace Spring.IBatis
{
    /// <summary>
    /// Factory object for <see cref="IBatisNet.DataMapper.SqlMapper"/> instances.
    /// </summary>
    /// <remarks>
    /// <p>
    /// This implementation operates in singleton fashion by default. That is,
    /// repeated calls to the
    /// <see cref="Spring.Objects.Factory.IFactoryObject.GetObject()"/> method
    /// will always return the same instance of the configured
    /// <see cref="IBatisNet.DataMapper.SqlMapper"/>. This seems a reasonable
    /// default... if you want multiple instances of an
    /// <see cref="IBatisNet.DataMapper.SqlMapper"/> for the same config file
    /// (and really, why would you?), then be sure to set the
    /// <see cref="Spring.Objects.Factory.IFactoryObject.IsSingleton"/>
    /// property value to <see langword="false"/>, or create multiple
    /// definitions of this object factory in your configuration (XML, etc).
    /// </p>
    /// </remarks>
    /// <author>Rick Evans</author>
    /// <version>$Id: SqlMapperFactoryObject.cs,v 1.1 2006/05/19 21:01:30 markpollack Exp $</version>
    public class SqlMapperFactoryObject : AbstractIBatisFactoryObject
    {

        #region Constructor (s) / Destructor

        public SqlMapperFactoryObject()
        {
            string workStatus = "Constructor ... ";
            System.Diagnostics.Trace.Write(workStatus);
        }

        #endregion 


        private DomSqlMapBuilder sqlMapBuilder;

        /// <summary>
        /// Return the <see cref="System.Type"/> of object that this
        /// <see cref="Spring.Objects.Factory.IFactoryObject"/> creates, or
        /// <see langword="null"/> if not known in advance.
        /// </summary>
        /// <remarks>
        /// <p>
        /// This implementation <b>always</b> returns the
        /// <see cref="IBatisNet.DataMapper.SqlMapper"/>
        /// <see cref="System.Type"/>.
        /// </p>
        /// </remarks>
        /// <seealso cref="Spring.Objects.Factory.IFactoryObject.ObjectType"/> 
        public override Type ObjectType
        {
            get { return typeof(ISqlMapper); }
        }

        /// <summary>
        /// Sets the <see cref="IBatisNet.DataMapper.Configuration.DomSqlMapBuilder"/>
        /// instance to be used to configure an <see cref="IBatisNet.DataMapper.SqlMapper"/>
        /// instance.
        /// </summary>
        /// <remarks>
        /// <p>
        /// Not required. The default
        /// <see cref="IBatisNet.DataMapper.Configuration.DomSqlMapBuilder"/> is 
        /// suitable for most cases.
        /// </p>
        /// </remarks>
        public DomSqlMapBuilder SqlMapBuilder
        {
            get
            {
                if (sqlMapBuilder == null)
                {
                    sqlMapBuilder = new DomSqlMapBuilder();
                }
                return sqlMapBuilder;
            }
            set { sqlMapBuilder = value; }
        }

        /// <summary>
        /// Like it says on the tin, creates and configures an
        /// <see cref="IBatisNet.DataMapper.SqlMapper"/> instance using
        /// the default iBatis.NET configuration file.
        /// </summary>
        /// <returns>
        /// An <see cref="IBatisNet.DataMapper.SqlMapper"/>.
        /// </returns>
        protected override object CreateUsingDefaultConfig()
        {
            if (ConfigWatcher != null)
            {
                return SqlMapBuilder.ConfigureAndWatch(ConfigWatcher);
            }
            else
            {
                return SqlMapBuilder.Configure();
            }
        }

        /// <summary>
        /// Creates and configures a
        /// <see cref="IBatisNet.DataMapper.SqlMapper"/> instance using
        /// a non-default iBatis.NET configuration file.
        /// </summary>
        /// <returns>
        /// An <see cref="IBatisNet.DataMapper.SqlMapper"/>.
        /// </returns>
        protected override object CreateUsingCustomConfig()
        {
            ISqlMapper mapper = null;
            if (ConfigWatcher != null)
            {
                mapper = SqlMapBuilder.ConfigureAndWatch(ConfigLocation.File, ConfigWatcher);
            }
            else
            {
                using (Stream stream = ConfigLocation.InputStream)
                {
                    SqlMapBuilder.Properties = GetSqlMapperProperties();
                    mapper = SqlMapBuilder.Configure(stream);
                }
            }
            //
            mapper.SessionStore = new IBatisNet.DataMapper.SessionStore.HybridWebThreadSessionStore("SQLMAPPER."+Guid.NewGuid().ToString());
            return mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private System.Collections.Specialized.NameValueCollection GetSqlMapperProperties()
        {
            System.Collections.Specialized.NameValueCollection props = new System.Collections.Specialized.NameValueCollection();
            //           
            if (SqlMapperPropertiesHolder != null)
            {
                if (!string.IsNullOrEmpty(SqlMapperPropertiesHolder.DbProvidersResourceName))
                {
                    props.Add("dbProvidersResourceName", SqlMapperPropertiesHolder.DbProvidersResourceName);
                }

                if (!string.IsNullOrEmpty(SqlMapperPropertiesHolder.DbProviderName))
                {
                    props.Add("dbProviderName", SqlMapperPropertiesHolder.DbProviderName);
                }

                if (!string.IsNullOrEmpty(SqlMapperPropertiesHolder.ConnectionString))
                {
                    props.Add("connectionString", SqlMapperPropertiesHolder.ConnectionString);
                }                
            }
            //
            return props;
        }

        private ISqlMapperPropertiesHolder _SqlMapperPropertiesHolder;

        public ISqlMapperPropertiesHolder SqlMapperPropertiesHolder
        {
            get { return _SqlMapperPropertiesHolder; }
            set { _SqlMapperPropertiesHolder = value; }
        }

        public override void  AfterPropertiesSet()
        {
 	        base.AfterPropertiesSet();
        }  

    }
}

