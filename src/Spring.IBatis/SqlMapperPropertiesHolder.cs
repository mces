using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Spring.IBatis
{
    public interface ISqlMapperPropertiesHolder
    {
        string DbProvidersResourceName { get;}
        string ConnectionString { get;}
        string DbProviderName { get;}
    }

    public class AppConfigSqlMapperPropertiesHolder : ISqlMapperPropertiesHolder
    {

        public AppConfigSqlMapperPropertiesHolder(string dbProvidersResourceName,string dbProviderName, string connectionStringName)
        {
            this._DbProvidersResourceName = dbProvidersResourceName;
            this._DbProviderName = dbProviderName;
            this.ParseConnectionStringName(connectionStringName);
        }

        private void ParseConnectionStringName(string connectionStringName)
        {
            this._ConnectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
        }

        #region ISqlMapperPropertiesHolder ��Ա
        private string _ConnectionString;
        public string ConnectionString
        {
            get
            {
                return _ConnectionString;
            }           
        }

        private string _DbProviderName;
        public string DbProviderName
        {
            get
            {
                return _DbProviderName;
            }       
        }

        #endregion

        #region ISqlMapperPropertiesHolder ��Ա
        private string _DbProvidersResourceName;
        public string DbProvidersResourceName
        {
            get
            {
                return this._DbProvidersResourceName;
            }   
        }

        #endregion
    }
}
