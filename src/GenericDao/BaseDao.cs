﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

//namespace GenericDao
//{
    /// <summary>
    /// BaseDao, support CRUD(SelectDao,InsertDao,UpdateDao,DeleteDao)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface BaseDao<T> : SelectDao<T>, InsertDao<T>, UpdateDao<T>, DeleteDao<Hashtable>
    {

    }

    /// <summary>
    /// SelectDao
    /// </summary>
    /// <typeparam name="T">Domain Type</typeparam>
    public interface SelectDao<T>
    {
        /// <summary>
        ///  Select object array
        /// </summary>
        /// <param name="params">the parameter can be null.</param>
        /// <returns></returns>
        T[] Select(Hashtable @params);
    }

    /// <summary>
    /// InsertDao
    /// </summary>
    /// <typeparam name="T">Domain Type</typeparam>
    public interface InsertDao<T>
    {
        /// <summary>
        /// Insert object
        /// </summary>
        /// <param name="obj"></param>
        void Insert(T obj);
    }

    /// <summary>
    /// UpdateDao
    /// </summary>
    /// <typeparam name="T">Domain Type</typeparam>
    public interface UpdateDao<T>
    {
        /// <summary>
        /// Update object
        /// </summary>
        /// <param name="obj"></param>
        void Update(T obj);
    }

    /// <summary>
    /// DeleteDao
    /// </summary>
    /// <typeparam name="T">
    ///  Parameter Type, such as: <see cref="Hashtable"/>, or int,Guid etc.
    ///  PrimaryKey type of datatable.
    /// </typeparam>
    public interface DeleteDao<T>
    {
        /// <summary>
        /// Delete objects by params
        /// </summary>
        /// <param name="params">if T is Hashtable, the parameter can be null.</param>
        void Delete(T @params);
    }
//}
