using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MCES.Domain;

namespace MCES.Dao
{
	///
	///UserInDepartmentDao
	///
    public partial interface UserInDepartmentDao : BaseDao<UserInDepartment>
    {
        Department GetUserInDepartment(Guid? UserId, string UserName, Hashtable @params);
    }
}

