using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MCES.Domain;
using System.Data;

namespace MCES.Dao
{
	///
	///EvalTicketDao
	///
    public partial interface EvalTicketDao : BaseDao<EvalTicket>
    {
        DataTable GetMidCadresTable(int EvalPlanId, string UserName, string RoleName, int DepartmentId, Hashtable @params);
    }
}

