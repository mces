﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

namespace MCES.Dao
{
    public interface UserDao
    {
       DataTable GetUsers(int StartRowIndex, int MaxRows, Hashtable @params);
       int GetUsersCount(int StartRowIndex, int MaxRows, Hashtable @params);
    }
}
