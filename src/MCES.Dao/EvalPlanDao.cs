using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MCES.Domain;
using System.Data;

namespace MCES.Dao
{
	///
	///EvalPlanDao
	///
    public partial interface EvalPlanDao : BaseDao<EvalPlan>
    {
        int AddEvalPlan(EvalPlan plan);
        //
        DataTable GetEvalPlans(int StartRowIndex, int MaxRows, Hashtable @params);
        int GetEvalPlansCount(int StartRowIndex, int MaxRows, Hashtable @params);
    }
}

