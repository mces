using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MCES.Domain;

namespace MCES.Dao
{
	///
	///DepartmentDao
	///
    public partial interface DepartmentDao : BaseDao<Department>
    {
        Department[] GetSubDepartments(int StartRowIndex, int MaxRows, Hashtable @params);
        int GetSubDepartmentsCount(int StartRowIndex, int MaxRows, Hashtable @params);
    }
}

