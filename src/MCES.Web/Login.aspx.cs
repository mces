﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.Configuration;
using MCES.Domain;


namespace MCES.Web
{
    public partial class LoginPage : BasePage, IPageAuthorizeless
    {
        private string loginErrorMessage = string.Empty;
        //
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                InitPageNotPostBack();     
            }
        }

        private void InitPageNotPostBack()
        {
            TextBox ctlUserName = this.loginCtrl.FindControl("UserName") as TextBox;
            TextBox ctlPassword = this.loginCtrl.FindControl("Password") as TextBox;
            TryGetCookiedUserName(ctlUserName);
            if (!string.IsNullOrEmpty(ctlUserName.Text))
            {
                ctlPassword.Focus();
            }
            else
            {
                ctlUserName.Focus();
            }
        }
        private string UserNameCookieName
        {
            get
            {
                return string.Format("{0}/{1}", Request.ApplicationPath.ToUpper(), "Login.UserName");
            }
        }
        private void TryGetCookiedUserName(TextBox ctlUserName)
        {
            if (ctlUserName == null) return;
            //                        
            HttpCookie cookie = Request.Cookies[UserNameCookieName];
            if (cookie != null)
            {
                //必须设置loginCtl.UserName, 只设置ctlUserName.Text 无效! 
                //可能是ASP.NET BUG
                this.loginCtrl.UserName = Server.UrlDecode(cookie.Value);
                ctlUserName.Text = Server.UrlDecode(cookie.Value);
            }
        }

        private void SetCookiedUserName(string userName)
        {
            HttpCookie cookie = new HttpCookie(UserNameCookieName, Server.UrlEncode(userName));
            cookie.Path = Request.ApplicationPath;
            cookie.Expires = DateTime.Now.AddYears(1);
            cookie.HttpOnly = true;
            Response.Cookies.Set(cookie);
        }
   
        //
        protected void loginCtrl_Authenticate(object sender, AuthenticateEventArgs e)
        {
            e.Authenticated = false;
            //
            if (!Membership.ValidateUser(loginCtrl.UserName, loginCtrl.Password))
            {
                loginErrorMessage =  "登录名或密码错误，请重新登录。";
                return;
            }
            //Roles?
            string[] roles = Roles.GetRolesForUser(loginCtrl.UserName);
            if (roles.Length == 0)
            {
                loginErrorMessage = "非法用户，无授权任何角色。";
                return;
            }

            //对非系统管理员进行业务判断?
            if(! Roles.IsUserInRole(loginCtrl.UserName, RoleInfo.SysAdmin))
            {
                ///TODO: 判断是否可访问?
                
                ///TODO: 判断是否已提交评测内容(仅能提交一次评测内容)?
            }
            //
            e.Authenticated = true;
        }
        protected void loginCtrl_LoginError(object sender, EventArgs e)
        {
            ShowLoginErrorMessage();
        }

        private void ShowLoginErrorMessage()
        {
            if (!string.IsNullOrEmpty(loginErrorMessage))
            {
                this.PageOnLoadScriptsBlock("登录-提示", loginErrorMessage, "function(){$('" + this.loginCtrl.FindControl("Password").ClientID + "').focus();}");
            }
        }
}
}
