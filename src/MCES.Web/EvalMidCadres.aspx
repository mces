﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EvalMidCadres.aspx.cs" Inherits="MCES.Web.EvalMidCadresPage" %>

<%-- E1  --%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <link rel="stylesheet" type="text/css" href="assets/skins/vista/core.css"/> 
 <style type="text/css">
 .vista-grid .datatable .header0 TH
 {
 	background:#e9e9eb url(assets/skins/vista/img/header-bg.png);
 	border-bottom:solid 1px #A7BAC5;
 	height:22px;
 	line-height:22px;
 	padding-left:6px;
 	color:#666666;
 	text-align:center;
 	font-weight:600;
 }
 .rl {border-right:solid 1px #A7BAC5;}
 </style>
 <script type="text/javascript">
     function SetTicket(self, col, val, evalplanId, userId) {
         if (!self.checked) return;
         [1, 2, 3, 4].each(function(i) {
             var o = $(col + i + '-' + userId);
             if (o && o != self) o.checked = false;
         });      
     }
 </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
     <table cellpadding="0"  cellspacing="0" style=" width:100%" border="1">
    <tr><td colspan="2" style="height:24px; padding-left:6px; border-bottom:1px solid gray; border-left:1px solid gray;">
     评测任务&#160;&raquo;&#160;中层干部评测
    </td></tr>
    <tr>
    <td valign="top" style="border:0px;border-right:gray 1px solid;width:100%">
    <!-- 列表区 -->
    <div id="demo-grid" class="vista-grid" style="width:100%">
    <div class="titlebar">      
       &#160;&#160; 
    </div>
    
    <asp:ListView ID="lvwMidCadres" runat="server" DataSourceID="odsMidCadres" EnableViewState="false">
                                    <LayoutTemplate>
                                    <table class="datatable" cellpadding="0" cellspacing="0">
 <tr class="header0">
  <td rowspan="2" style="border-bottom:solid 1px #A7BAC5;padding-left:6px;color:#666666;text-align:left;">单位</td>
  <td rowspan="2" style="border-bottom:solid 1px #A7BAC5;padding-left:6px;color:#666666;text-align:left;" class="rl">姓名</td>
  <th colspan="3" class="rl">政治素质</th>
  <th colspan="3" class="rl">领导水平</th>
  <th colspan="3" class="rl">业务能力</th>
  <th colspan="3" class="rl">工作业绩</th>
  <th colspan="3" class="rl">敬业精神</th>
  <th colspan="4" class="rl">总体评价</th>
 </tr>
 <tr class="header">
  <th>优秀</th>
  <th>良好</th>
  <th class="rl">一般</th>
  
  <th>优秀</th>
  <th>良好</th>
  <th class="rl">一般</th>

  <th>优秀</th>
  <th>良好</th>
  <th class="rl">一般</th>

  <th>优秀</th>
  <th>良好</th>
  <th class="rl">一般</th>

  <th>优秀</th>
  <th>良好</th>
  <th class="rl">一般</th>
 
  <th>优秀</th>
  <th>称职</th>
  <th>基本称职</th>
  <th class="rl">不称职</th>
  
  </tr>
                                         <tr id="itemPlaceholder" runat="server" />
                                        </table>
                                    <table class="datapager" cellpadding="0" cellspacing="0">
                                        <tr>
                                           <asp:DataPager ID="pager" runat="server" PageSize="25">
                                                <Fields>
                                                 <asp:TemplatePagerField OnPagerCommand="PagerCommand">
                                                        <PagerTemplate>
                                                            <td class="commands">
                                                                <asp:LinkButton ID="btnFirst" runat="server" CommandName="First" CssClass="first-page page-command" AlternateText="First Page" ToolTip="首页" />
                                                                <asp:LinkButton ID="btnPrevious" runat="server" CommandName="Previous" CssClass="prev-page page-command" AlternateText="Previous Page" ToolTip="前一页" />    
                                                                <asp:LinkButton ID="btnNext" runat="server" CommandName="Next" CssClass="next-page page-command" AlternateText="Next Page" ToolTip="下一页" />
                                                                <asp:LinkButton ID="btnLast" runat="server" CommandName="Last" CssClass="last-page page-command" AlternateText="Last Page" ToolTip="尾页" />                                                                                                           
                                                            </td>
                                                            <td class="info">
                                                                第
                                                                <b>
                                                                    <%# Container.TotalRowCount > 0 ? Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) : 0 %>
                                                                </b>
                                                                /
                                                                <b>
                                                                    <%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows)%>
                                                                </b>
                                                                页 (共<%# Container.TotalRowCount %> 项)  
                                                            </td>                                                          
                                                        </PagerTemplate>
                                                    </asp:TemplatePagerField>
                                                </Fields>
                                            </asp:DataPager>
                                        </tr>                                 
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr class='<%# Container.DisplayIndex % 2 == 0 ? "item" : "altitem" %>'>
                                        <td class="first"><%# Eval("DepartmentName")%></td>
                                        <td class="rl"><%# Eval("UserName") %></td>                                      
                                      
                                        <td><input type="checkbox" id="A1-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'A',1,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td><input type="checkbox" id="A2-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'A',1,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td class="rl"><input type="checkbox" id="A3-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'A',1,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        
                                        <td><input type="checkbox" id="B1-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'B',1,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td><input type="checkbox" id="B2-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'B',2,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td class="rl"><input type="checkbox" id="B3-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'B',3,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td> 
                                          
                                        <td><input type="checkbox" id="C1-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'C',1,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td><input type="checkbox" id="C2-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'C',2,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td class="rl"><input type="checkbox" id="C3-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'C',3,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        
                                        <td><input type="checkbox" id="D1-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'D',1,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td><input type="checkbox" id="D2-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'D',2,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td class="rl"><input type="checkbox" id="D3-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'D',3,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td> 

                                        <td><input type="checkbox" id="E1-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'E',1,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td><input type="checkbox" id="E2-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'E',2,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td class="rl"><input type="checkbox" id="E3-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'E',3,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>     
                                        
                                        <td><input type="checkbox" id="F1-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'F',1,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td><input type="checkbox" id="F2-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'F',2,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>
                                        <td><input type="checkbox" id="F3-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'F',3,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td>   
                                        <td class="rl"><input type="checkbox" id="F4-<%#Eval("UserId","{0:N}") %>" onclick="javascript:SetTicket(this,'F',4,<%#Eval("EvalPlanId") %>,'<%#Eval("UserId","{0:N}")%>')"/></td> 
                                                                                                                                                                                                         
                                    </tr>
                                </ItemTemplate>                              
                                
                            </asp:ListView>                           
                                   
    </div>    
    <!-- 列表区 -->
    </td></tr></table>     
    
      <asp:ObjectDataSource runat="server" ID="odsMidCadres" EnablePaging="False" 
             onobjectcreating="odsMidCadres_ObjectCreating" 
             onselecting="odsMidCadres_Selecting"
             SelectMethod="GetMidCadresTable"          
             TypeName="MCES.Services.EvalService" 
           >
    </asp:ObjectDataSource>
</asp:Content>

