﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace MCES.Web
{
    public partial class MgtMyPasswordPage : BasePage, IPageAuthorizeless
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            //
            string msg = string.Empty;
            string fn = "function(){}";
            //
            string oldPassword = this.txtPassword.Text;
            if (! Membership.ValidateUser(User.Identity.Name, oldPassword))
            {
                msg = "当前帐号与原密码不匹配。";
                goto _SHOWMSG;
            }
            //
            //判断NewPassword 是否一致,且满足>=6
            if (this.txtNewPassword.Text.Length < 6 || !string.Equals(this.txtNewPassword.Text, this.txtNewPassword2.Text))
            {
                msg = "新密码长度至少6位，且要求与新密码确认一致。";
                goto _SHOWMSG;
            }

            try
            {
                //判断成功，执行更新，并强制重新登录
                if (Membership.GetUser().ChangePassword(oldPassword, this.txtNewPassword.Text))
                {
                    FormsAuthentication.SignOut();
                    //
                    msg = "更新帐号密码成功，系统强制退出，请重新登录。";
                    fn = "function(){LogoutAndClearUserNameCookie();}";
                }
            }
            catch (Exception ex)
            {
                msg = string.Format("更新帐号密码失败，可能原因:{0}。", ex.Message);
            }

            //显示错误信息
        _SHOWMSG:
            string script = string.Format("Ext.MessageBox.alert('{0}','{1}',{2});", "帐号密码-提示", msg, fn);
            Anthem.Manager.AddScriptForClientSideEval(script);
        }
}
}
