<%@ Page Language="C#" ContentType="text/css" %>
<%@ Import Namespace="Common.Utils"%>
<%@ OutputCache Location="ServerAndClient" Duration="9000" VaryByParam="*"%>
<script runat="server">
</script>

.NavBar
{ 
  background-color: #C3C7D3; 
  border: 1px solid black; 
  border-top:0px;
  border-bottom:0px;
  padding: 7px;
  padding-left:0px;
  padding-top:0px;  
  cursor: default; 
} 

.TopItem
{
  color:white;
  background-image: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/item_bg.gif")%>); /*top_level_bg.gif*/
  font-family: tahoma, verdana; 
  font-size: 11px; 
  font-weight: bold;
  padding-top: 4px; 
  padding-bottom: 4px; 
  width:185px;
  cursor:hand;
}

.TopItemHover
{
  /*color:Gray;*/
  color:White;
  background-image: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/item_hoverBg.gif")%>); /*top_level_bg.gif*/
  font-family: tahoma, verdana; 
  font-size: 11px; 
  font-weight: bold;
  padding-top: 4px; 
  padding-bottom: 4px; 
  width:185px;
  cursor:hand;
}

.Level2Item
{
  font-family: tahoma, verdana; 
  font-size: 11px; 
  text-align: center;
  cursor:hand;
}

.Level2ItemHover
{
  font-family: tahoma, verdana; 
  font-size: 11px; 
  text-align: center;
  text-decoration:underline;
  cursor:hand;
}

.Level2Group
{
  background-color:#F0F1F5;
  border: 1px white solid;
  border-top-color: #F0F1F5;
  padding:5px;
  width:185px;
}

.Empty
{
}


