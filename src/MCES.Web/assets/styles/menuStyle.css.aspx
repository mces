<%@ Page Language="C#" ContentType="text/css" %>
<%@ Import Namespace="Common.Utils"%>
<%@ OutputCache Location="ServerAndClient" Duration="9000" VaryByParam="*"%>
<script runat="server">
</script>

.TopGroup
{
  background-color:#D9D9E6; 
  border:solid 1px gray; 
  cursor:default; 
}

.MenuGroup
{
  background-image:url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/group_background.gif")%>);
  background-color:white;
  border:solid 1px #7E7E81; 
  cursor:default; 
}

.TopMenuItem
{
  background-color:#D9D9E6; 
  color:black; 
  font-family:tahoma; 
  font-size:11px; 
  border:solid 1px #D9D9E6; 
  cursor:default; 
}

.TopMenuItemHover 
{
  background-image:url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/item_hover_bg.gif")%>);
  background-color:#E5E6EF; 
  color:black; 
  font-family:tahoma; 
  font-size:11px; 
  border:solid 1px #7C7C94; 
  cursor:default; 
}

.TopMenuItemExpanded
{
  background-image:url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/top_group_background.gif")%>);
  background-color:#E5E6EF; 
  color:black; 
  font-family:tahoma; 
  font-size:11px; 
  border:solid 1px #7C7C94; 
  border-bottom-color:#BAB9CD; 
  cursor:default; 
}

.MenuItem
{
  color:black; 
  font-family:tahoma; 
  font-size:11px; 
  margin:1px; 
  cursor:default; 
}

.MenuItemHover 
{
  background-color:#FFEEC2; 
  color:black; 
  font-family:tahoma; 
  font-size:11px; 
  border:solid 1px #4B4B6F; 
  cursor:default; 
}

.MenuBreak
{
  background-image:url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/break_bg.gif")%>);
  width:100%;
  height:1px;
}