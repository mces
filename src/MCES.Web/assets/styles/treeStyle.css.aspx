<%@ Page Language="C#" ContentType="text/css" %>
<%@ Import Namespace="Common.Utils"%>
<%@ OutputCache Location="ServerAndClient" Duration="9000" VaryByParam="*"%>
<script runat="server">
</script>

.TreeViewHeader
{
  background: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/treeview_headerBg.gif")%>);
  background-color: #737294;
  color: #FFFFFF; 
  border: #57566F 1px solid; 
  border-bottom-width: 0px; 
  font-family: tahoma; 
  font-size: 11px;
  font-weight: bold; 
  padding: 5px; 
  cursor: default; 
}

.TreeView 
{ 
  background-color: #FFFFFF;
  padding-top: 4px; 
  padding-left: 1px; 
  /*border: #57566F 1px solid; */
  border: gray 1px solid;
  cursor: default; 
}

.TreeViewMargin
{ 
  background:url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/treeview_bg.gif")%>);
  background-color: #FFFFFF;
  padding-top:4px; 
  padding-left:1px; 
  border: #57566F 1px solid; 
  cursor:default; 
}

.TreeNode 
{ 
  font-family: tahoma; 
  font-size: 11px; 
  padding-top:2px;
  padding-bottom:1px;
  padding-left: 3px; 
  padding-right: 3px; 
}

.GrayedTreeNode 
{ 
  font-family: tahoma; 
  font-size: 11px; 
  padding-top:2px;
  padding-bottom:1px;
  padding-left: 3px; 
  padding-right: 3px; 
  color:gray; 
  cursor:default;
}

.HoverTreeNode 
{ 
  font-family: tahoma; 
  font-size: 11px; 
  text-decoration:underline; 
  padding-top:2px;
  padding-bottom:1px;
  padding-left: 3px; 
  padding-right: 3px; 
  cursor: default; 
}

.SelectedTreeNode 
{ 
  font-family: tahoma; 
  font-size: 11px; 
  background-color: gray; 
  color:white; 
  padding-top:1px;
  padding-bottom:0px;
  padding-left: 2px; 
  padding-right: 2px; 
  cursor: default; 
  border: 1px solid #3F3F3F;
}

.MultipleSelectedTreeNode
{
  font-family: tahoma; 
  font-size: 11px; 
  background-color: gray; 
  color:white; 
  padding-top:2px;
  padding-bottom:1px;
  padding-left: 3px; 
  padding-right: 3px; 
  cursor: default; 
}

.CutTreeNode
{
  font-family: tahoma; 
  font-size: 11px; 
  background-color: silver; 
  color:white; 
  padding-top:2px;
  padding-bottom:1px;
  padding-left: 3px; 
  padding-right: 3px; 
  cursor: default; 
}

.NodeEdit 
{
  border:1px gray solid;
  padding-top:0px;
  padding-bottom:0px;
  padding-left: 2px; 
  padding-right: 2px; 
  font-family: tahoma; 
  font-size: 11px;
}

.Margin
{
  background-color: #E6E6F0;
}
