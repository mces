<%@ Page Language="C#" ContentType="text/css" %>
<%@ Import Namespace="Common.Utils"%>
<%@ OutputCache Location="ServerAndClient" Duration="9000" VaryByParam="*"%>
<script runat="server">
</script>

.HorizontalSplitterBar
{
  background: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/splitter_horBg.gif")%>); 
}

.CollapsedHorizontalSplitterBar
{
  background: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/splitter_horBg.gif")%>); 
  border: 1px solid #000000;
  border-right-width: 0px;
}

.VerticalSplitterBar
{
  background: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/splitter_verBg.gif")%>); 
}

.CollapsedVerticalSplitterBar
{
  background: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/main/splitter_verBg.gif")%>); 
  border: 1px solid #000000;
  border-bottom-width: 0px;
}

.ActiveSplitterBar
{
  background-color: #000000;  
  filter:progid:DXImageTransform.Microsoft.Alpha(opacity=40); 
  opacity: 0.4;
}

.SplitterPane
{
  /*border: 1px solid #333333;*/
  border: 1px solid gray;
  border-top: 0px;
  border-bottom:0px
}
