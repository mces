<%@ Page Language="C#" ContentType="text/css" %>
<%@ Import Namespace="Common.Utils"%>
<%@ OutputCache Location="ServerAndClient" Duration="9000" VaryByParam="*"%>
<script runat="server">
</script>

.TopGroup
{
  z-index:99;
  position:relative;
}

.DefaultTab 
{
  color:black; 
  background-image: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/tab/tab_bg.gif")%>);
  font-family:MS Sans Serif, Verdana; 
  font-size:11px; 
  cursor:default;
}

.DefaultTabHover 
{
  color:black; 
  background-image: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/tab/hover_tab_bg.gif")%>);
  font-family:MS Sans Serif, Verdana; 
  font-size:11px; 
  cursor:default;
}

.SelectedTab 
{
  color:black; 
  background-image: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/tab/selected_tab_bg.gif")%>);
  font-family:MS Sans Serif, Verdana; 
  font-size:11px; 
  cursor:default;
}

.MultiPage
{
  background-color:White;
  border: 1px solid #919B9C;
  width:487px;
  /*height:240px;*/
  height:200px;
  position:relative;
  top:-3px;
  left:1px;
  z-index:98;
}