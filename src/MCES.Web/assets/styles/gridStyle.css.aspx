<%@ Page Language="C#" ContentType="text/css" %>
<%@ Import Namespace="Common.Utils"%>
<%@ OutputCache Location="ServerAndClient" Duration="9000" VaryByParam="*"%>
<script runat="server">
</script>
.Grid 
{ 
  border: 1px solid #57566F; 
  border-bottom: 1px solid #57566F; 
  background-color: #FFFFFF;
  cursor: pointer;
  border-top: 0px;
}

.GridHeader
{
  background-image: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/grid/grid_headerBg.gif")%>);
  background-color: #8988A5; 
  border-bottom: 1px solid #57566F; 
  height: 28px;
  padding-left: 3px;
  cursor: default; 
  color: #FFFFFF;  
  font-family: verdana; 
  font-size: 10px;
  font-weight: bold;
  vertical-align: center;
}

.GridHeaderText
{
  color: #FFFFFF;  
  font-family: verdana; 
  font-size: 10pt;
}

.GridFooter
{
  cursor: default; 
  padding: 5px;
  height: 48px;
  vertical-align: bottom;
}

.GridFooterText
{
  color: #000000;  
  font-family: verdana; 
  font-size: 11px;
}

.HeadingRow 
{ 
  background-color: #E9E9EB; 
  background-image: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/grid/header_rowBg.gif")%>);   
}

.HeadingCell 
{ 
  background-image: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/grid/header_bg.gif")%>);   
  background-color: #E9E9EB; 
  padding: 3px; 
  padding-top: 2px; 
  padding-bottom: 2px; 
}

.HeadingCellHover
{ 
  background-image: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/grid/header_hoverBg.gif")%>);   
  background-color: #F6F6F7; 
}

.HeadingCellActive
{ 
  background-image: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/grid/header_activeBg.gif")%>);   
  background-color: #F6F6F7;  
}

.HeadingRow td.FirstHeadingCell
{ 
  background-image: url(<%=WebHelper.GetFullWebAppUrl("/assets/images/grid/header_bg.gif")%>); 
}

.HeadingCellText
{
  font-family: verdana; 
  font-size: 11px; 
  font-weight: bold; 
  text-align: left;
  padding-top: 3px;
  padding-bottom: 5px;
}

.Row 
{ 
  background-color: #FFFFFF; 
  cursor: default;
}

.Row td.DataCell 
{ 
  padding: 3px; 
  padding-top: 2px; 
  padding-bottom: 1px; 
  border-bottom: 1px solid #EAE9E1; 
  font-family: verdana; 
  font-size: 10px; 
} 

.SortedDataCell 
{ 
  background-color: #F5F5F5; 
} 


.Row td.LastDataCell 
{ 
  background-color: #EFEFF4; 
} 

.SelectedRow 
{ 
}

.SelectedRow td.DataCell 
{ 
  background-color: #FFEEC2; 
  padding: 2px; 
  padding-left: 3px; 
  padding-top: 1px; 
  padding-bottom: 1px; 
  font-family: verdana; 
  font-size: 10px; 
  border-bottom: 1px solid #4B4B6F; 
  border-top: 1px solid #4B4B6F; 
  border-right: 0px; 
}

.SelectedRow td.FirstDataCell 
{ 
  background-color: #FFEEC2; 
  border-left: 1px solid #4B4B6F; 
  border-right: 0px; 
  padding-left: 2px; 
  padding-right: 3px; 
}

.SelectedRow td.LastDataCell 
{ 
  background-color: #FFEEC2; 
  border-right: 1px solid #4B4B6F; 
}

.GroupHeading
{
  color: #706F91; 
  background-color: #FFFFFF; 
  font-family: verdana; 
  font-weight: normal;
  font-size: 11px; 
  border-bottom: 2px solid #A5A4BD; 
  padding-top: 10px;
  padding-bottom: 3px;
}

.GroupByCell
{
  cursor: pointer;
}

.GroupByText
{
  color: #FFFFFF; 
  font-size: 11px;   
  font-weight: normal; 
  padding-right: 5px;
}


.SliderPopup
{
  background-color: #FFF4D7; 
  border: 1px solid #4B4B6F; 
  border-top-color: #9495A2; 
  border-left-color: #9495A2; 
  font-size: 11px; 
  width: 150px;
  height: 30px;
}