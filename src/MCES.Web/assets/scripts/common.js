//-------------------------------------------------------------------------------
//Browser Info
var isGecko=navigator.userAgent.indexOf("Gecko")>0;
var isIE=navigator.userAgent.indexOf("MSIE")>0;
var isSafari=navigator.userAgent.indexOf("Safari")>0;
var isFirefox=navigator.userAgent.indexOf("Firefox")>0;
var isCamino=navigator.userAgent.indexOf("Camino")>0;
var isMozilla=navigator.userAgent.indexOf("Gecko/")>0;
 //浏览器版本检测
 //获取浏览器的名字Netscape或者Microsoft Internet Explorer
 var  browserName=navigator.appName;
 //获取浏览器的版本信息
 var browserVersion=parseInt(navigator.appVersion);
 var browser;
 if(browserName=="Netscape"&&browserVersion==5)
 {browser="nn6";//netspace6
 }
 else if(browserName=="Netscape"&&browserVersion==4){
  browser="nn4";      //netspace4
 }else if(browserName=="Microsoft Internet Explorer"&&
  browserVersion==4&&navigator.appVersion.indexOf("MSIE 7.0")!=-1){
  browser="ie7";      //IE 7.0
 }else if(browserName=="Microsoft Internet Explorer"&&
  browserVersion==4&&navigator.appVersion.indexOf("MSIE 6.0")!=-1){
  browser="ie6";      //IE 6.0
 }else if(browserName=="Microsoft Internet Explorer"&&
  browserVersion==4&&
   navigator.appVersion.indexOf("MSIE 5.5")!=-1){
  browser="ie55";     //IE5.5
 }
 else if(browserName=="Microsoft Internet Explorer"&&
  browserVersion==4&&
   navigator.appVersion.indexOf("MSIE 5.0")!=-1){
  browser="ie5";       //IE5.0
 }
 else if(browserName=="Microsoft Internet Explorer"&&
  browserVersion==4){
  browser="ie4";       //IE4
 } 
 //
 var isIE55 = (browser == "ie55");
 var isIE6 = (browser == "ie6");
 var isIE7 = (browser == "ie7");
 //--------------------------------------------------------------------------------------
//page set center title function
function SetCenterTitle()
{
	var title = document.title;
	//
	if(typeof parent !=  'undefined' && typeof parent.Home != 'undefined')
	{
		title = (typeof window.__Title != 'undefined') ? window.__Title : title;
		var index = title.indexOf('['); // TITLE_INFO [SOFTWARE VERSION INFO]
		title = index != -1 ? title.substring(0,index) : title;
		parent.Home.setCenterTitle(title);
	}
}

//Is ListView ViewMode?
function IsListView()
{
   var s = window.location.href;
   return s.indexOf('.Edit.aspx') == -1 || s.indexOf('.List.aspx') > 0
}

//Get List Checkbox Ctl
function GetCheckedCheckBoxs(gvId,chkId)
{
    var eles = document.getElementsByTagName('INPUT');
    var ele;
    var objs = new Array();
    var count = 0;
    //
    for(var i = 0 ; i < eles.length ;  i ++)
    {
        ele = eles[i];
        var index = '';
        if(ele.id.substring(ele.id.lastIndexOf('_')+1) == chkId)
        {
            index = ele.id.substr(ele.id.lastIndexOf('_')-2,2)
        }    
        if( ele.type == 'checkbox' && ele.id.indexOf('ctl00_content_'+gvId+'_')==0 && ele.id.indexOf('_ctl'+index+'_'+chkId)>0 && ele.checked)
        {
            objs[count ++] = ele;
        }
    }
    //
    return objs;
}

function GetCheckedCheckboxsLikeId(idLike)
{
    var count = 0;
    var objs = new Array();
    var obj = document.getElementById(idLike);
    if(obj && obj.type == 'checkbox')
    {
       objs[count++] = obj;
       return;
    }
    var eles = document.getElementsByTagName('INPUT');
    for(var i = 0 ; i < eles.length ;  i ++)
    {
        ele = eles[i];
        if( ele.type == 'checkbox' && (ele.id == idLike || ele.id.indexOf(idLike) > -1) && ele.checked)
        {
            objs[count++] = ele;
        }
    }
    //
    return objs;
}

function UncheckedCheckboxs(idLike)
{
    var obj = document.getElementById(idLike);
    if(obj && obj.type == 'checkbox')
    {
       obj.checked = false;
       return;
    }
    var eles = document.getElementsByTagName('INPUT');
    for(var i = 0 ; i < eles.length ;  i ++)
    {
        ele = eles[i];
        if( ele.type == 'checkbox' && (ele.id == idLike || ele.id.indexOf(idLike) > -1) && ele.checked)
        {
            ele.checked = false;
        }
    }
}
var UncheckedAllCheckboxs = UncheckedCheckboxs;
function CheckedAllCheckboxs(idLike)
{
    var obj = document.getElementById(idLike);
    if(obj && obj.type == 'checkbox')
    {
       obj.checked = true;
       return;
    }
    var eles = document.getElementsByTagName('INPUT');
    for(var i = 0 ; i < eles.length ;  i ++)
    {
        ele = eles[i];
        if( ele.type == 'checkbox' && (ele.id == idLike || ele.id.indexOf(idLike) > -1) && ! ele.checked)
        {
            ele.checked = true;
        }
    }
}
//Get Values Function
function GetValues(objArray,propName)
{
  var rs = new Array();
  //
  for(var i = 0 ; i < objArray.length; i ++)
  {
     var sc = "objArray["+i+"]."+propName;
     rs[i] = eval(sc);   
  }
  //
  return rs;
}
//--------------------------------------------------------------------------------
/********************************************
*  方法:CheckNum(chkValue)
*  功能:验证是否数字或者小数点，event.keyCode=46时为小数点
*		event.keyCode=48->0;event.keyCode=57->9
*  参数:chkValue：需验证的值
*  返回:
********************************************/
function CheckNum(chkValue)
{
	if((event.keyCode>=48 && event.keyCode<=57)||(event.keyCode==46))
	{
		//第一位不允许为小数点
		if((chkValue.length==0)&&(event.keyCode==46))
		{
			event.keyCode = 0;
			return false;
		}
		//整数部分限制为10位
		if((chkValue.length == 10)&&(event.keyCode!=46))
		{
			var temtry = 0;
			//循环已有字符串
			for(var i=0;i<chkValue.length;i++)
			{
			   //如果已有小数点				
				if(chkValue.substring(i, i+1) == ".")
				{
					temtry = 1;
					//if(isIE7 || isFirefox)break;
				}
			}
			//在字符串长度已等于10位，且没有小数点的情况下，退出函数
			if(temtry == 0)
			{
				event.keyCode = 0;
				return false;
			}
		}
		//
		for(var i=0;i<chkValue.length;i++)
		{
			//小数点不允许重复输入
			if((chkValue.substring(i, i+1) == ".")&&(event.keyCode==46))
			{
				event.keyCode = 0;
				return false;
			}
			//小数点后限制为4位
			if((chkValue.substring(i, i+1) == ".")&&(chkValue.substring(chkValue.indexOf(".")+1).length > 1))
			{
				event.keyCode = 0;
				return false;
			}
		}
	}else
	{
		if(event.keyCode != 8)
		{
			event.keyCode = 0;
			
		}else
		{
			//
		}
	}
}
//--------------------------------------------------------------------------------
//
function ltrim(str)
{
    var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(0)) != -1)
    {
        var j=0, i = s.length;
        while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
        {
            j++;
        }
        s = s.substring(j, i);
    }
    return s;
}

function rtrim(str)
{
    var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(s.length-1)) != -1)
    {
        var i = s.length - 1;
        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
        {
            i--;
        }
        s = s.substring(0, i+1);
    }
    return s;
}

function trim(str)
{
   return rtrim(ltrim(str));
}
//--------------------------------------------------------------------------------
//Preload Images
function PreloadImgs()
{ var d = document;
  if(d.images)
  {
    if(! d.Images) d.Images = new Array();
    var i, j = d.Images.length, a = PreloadImgs.arguments;
    for(var i = 0 ; i < a.length; i ++)
    {
       if(a[i].indexOf("#") != 0)
       { 
           d.Images[j] = new Image;
           d.Images[j++].src= a[i];
        }
     }
    }
}
var PreloadImages = PreloadImgs;
//--------------------------------------------------------------------------------
function PageOnLoad()
{
   if(typeof __SaveDataSucceed != 'undefined')
   {
       __SaveDataSucceed();
   }
   if(typeof __SaveDataFailed != 'undefined')
   {
       __SaveDataFailed();
   }
   
   for(var i = 1 ; i <= 5; i ++)
   {
      if(eval("typeof __PageOnLoadScriptsBlock"+i+" !='undefined'"))
      {
          eval("__PageOnLoadScriptsBlock"+i+"();");
      }
   }  
}
var Page$OnLoad = PageOnLoad;
var Page_OnLoad = PageOnLoad;

//-----------------------------------------------------------------------------
function Anthem_PreCallBack() 
{
	var loading = document.createElement("div");
	loading.id = "loading";
	loading.style.height = "22px";
	loading.style.verticalAlign='middle';
	loading.style.color = "black";
    loading.style.backgroundColor = "#ffffe1";//"#9594af",//#ffffe1
	loading.style.paddingLeft = "5px";
	loading.style.paddingRight = "5px";
	loading.style.position = "absolute";
	loading.style.right = "5px";
	loading.style.top = "5px";
	loading.style.zIndex = "9999";
	url = '/assets/ext/resources/images/default/grid/loading.gif';
    url = (typeof(window.ApplicationPath) != 'undefined') ? window.ApplicationPath+url : '..'+url;
	loading.innerHTML = "<img src='"+url+"' style='width:16px;height:16px;' align='absmiddle'>正在处理数据...";
	document.body.appendChild(loading);
}

function Anthem_PostCallBack() 
{
	var loading = document.getElementById("loading");
	document.body.removeChild(loading);
}

function Anthem_Error(res)
{
    if(typeof Ext.MessageBox.alert != 'undefined')
	{
       Ext.MessageBox.alert('Anthem Error',res.error);

	}else
	{
		alert("Anthem Error:\\n"+ res.error);
	}
}

//-------------------------------------------------------------------------------------------------
GoUrl = function(url,target)
{
   window.open(url,target);
}
// This will stop image flickering in IE6 when elements with images are moved
try {document.execCommand("BackgroundImageCache", false, true);} catch(e) {}

//--------------------------------------------------------------------------------
ActiveWindow = function(ele)
{
  parent.myLightWindow.activate({parentWindow:window}, ele);
}
ActivateWindow = ActiveWindow;
OpenWindow = ActiveWindow;
//
DeactiveWindow = function()
{
  parent.myLightWindow.deactivate();
}
DeactivateWindow = DeactiveWindow;
CloseWindow = DeactiveWindow;