//----------------------------------------------------------
function createForm(p, name, action)
{
    var frm = document.createElement("form");
	frm.setAttribute("method","POST");
	frm.setAttribute("name",name);
	frm.setAttribute("action",action);
	frm.setAttribute("target","_self");
	p.appendChild(frm);
	p.style.display = 'none';
	return frm;
}
function createTextbox(frm, name, value)
{
   var child = document.createElement("input");
   child.setAttribute("type", "text");
   child.setAttribute("name", name);
   child.setAttribute("value", value);
   //
   frm.appendChild(child);
}
function createElement(frm, type, name, value)
{
  var child = document.createElement("input");
   child.setAttribute("type", type);
   child.setAttribute("name", name);
   child.setAttribute("value", value);
   //
   frm.appendChild(child);
}
function setCookie(cookieName,cookieValue)
{    
	var dat=new Date();
    dat.setTime(dat.getTime()+365*24*3600*1000);//在当时时间上加365天        
    window.document.cookie  = cookieName + "=" + escape(cookieValue)+";expires="+ dat.toGMTString()+";path=/;";
}
function readCookie(cookieName)
{            
    var cookieString = document.cookie;    
    var start = cookieString.indexOf(cookieName + '=');
        
    if (start == -1) //若不存在该名字的Cookie
        return null;
          
    start += cookieName.length + 1;
    var end = cookieString.indexOf(';', start);
    if (end == -1) //防止最后没有加“;”冒号的情况            
        return unescape(cookieString.substring(start));
             
    return unescape(cookieString.substring(start, end));
}