﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MgtUser.aspx.cs" Inherits="MCES.Web.MgtUserPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <link rel="stylesheet" type="text/css" href="assets/skins/vista/core.css"/>   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
 <div>
     <table cellpadding="0"  cellspacing="0" style=" width:100%" border="1">
    <tr><td colspan="2" style="height:24px; padding-left:6px; border-bottom:1px solid gray; border-left:1px solid gray;">
     系统设置&#160;&raquo;&#160;人员设置
    </td></tr>
    <tr>
    <td valign="top" style="border:0px;border-right:gray 1px solid;width:100%">
    <!-- 列表区 -->
    <div id="demo-grid" class="vista-grid" style="width:100%">
    <div class="titlebar">
       部门: <asp:DropDownList ID="ddlDepartments" runat="server" AutoPostBack="True" 
            onselectedindexchanged="BindUsers" AppendDataBoundItems="true">            
            </asp:DropDownList>
       -
       角色: <asp:DropDownList ID="ddlRoles" runat="server" AutoPostBack="True" 
            onselectedindexchanged="BindUsers" AppendDataBoundItems="true">
             <asp:ListItem Value="" Text="--全部--"></asp:ListItem>
            </asp:DropDownList>       
       &#160;&#160;
       <asp:LinkButton runat="server" ID="lbNew" Text="新建" ToolTip="新建员工" 
            onclick="lbNew_Click"></asp:LinkButton>  
    </div>
    
    <asp:ListView ID="lvwUsers" runat="server" DataSourceID="odsUsers" 
            DataKeyNames="UserId" oniteminserting="lvwUsers_ItemInserting" 
            onitemcanceling="lvwUsers_ItemCanceling" oniteminserted="lvwUsers_ItemInserted">
                                    <LayoutTemplate>
                                    <table class="datatable" cellpadding="0" cellspacing="0">
                                        <tr class="header">
                                            <th>#</th>
                                            <th>用户名</th>                                            
                                            <th>角色</th>
                                            <th>部门</th>
                                            <th>操作</th>
                                        </tr>
                                         <tr id="itemPlaceholder" runat="server" />
                                        </table>
                                    <table class="datapager" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <asp:DataPager ID="pager" runat="server" PageSize="15">
                                                <Fields>
                                                 <asp:TemplatePagerField OnPagerCommand="PagerCommand">
                                                        <PagerTemplate>
                                                            <td class="commands">
                                                                <asp:LinkButton ID="btnFirst" runat="server" CommandName="First" CssClass="first-page page-command" AlternateText="First Page" ToolTip="首页" />
                                                                <asp:LinkButton ID="btnPrevious" runat="server" CommandName="Previous" CssClass="prev-page page-command" AlternateText="Previous Page" ToolTip="前一页" />    
                                                                <asp:LinkButton ID="btnNext" runat="server" CommandName="Next" CssClass="next-page page-command" AlternateText="Next Page" ToolTip="下一页" />
                                                                <asp:LinkButton ID="btnLast" runat="server" CommandName="Last" CssClass="last-page page-command" AlternateText="Last Page" ToolTip="尾页" />                                                                                                           
                                                            </td>
                                                            <td class="info">
                                                                第
                                                                <b>
                                                                    <%# Container.TotalRowCount > 0 ? Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) : 0 %>
                                                                </b>
                                                                /
                                                                <b>
                                                                    <%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows)%>
                                                                </b>
                                                                页 (共<%# Container.TotalRowCount %> 项)  
                                                            </td>                                                          
                                                        </PagerTemplate>
                                                    </asp:TemplatePagerField>
                                                </Fields>
                                            </asp:DataPager>
                                        </tr>                                 
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr class='<%# Container.DisplayIndex % 2 == 0 ? "item" : "altitem" %>'>    
                                        <td><%# Container.DataItemIndex+1 %></td>                                    
                                        <td><%# Eval("UserName") %></td>
                                        <td><%# Eval("RoleName") %></td>
                                        <td><%# Eval("DepartmentName")%></td>
                                        <td>
                                          <asp:LinkButton ID="btnEdit" runat="server" Text="编辑" CommandName="Edit" />
                                          &#160;
                                          <asp:LinkButton ID="btnDelete" runat="server" Text="删除" CommandName="Delete" />
                                        </td>
                                    </tr>
                                </ItemTemplate>                               
                                <InsertItemTemplate>
                                  <tr class="altitem">            
                                       <td>&#160;</td>                            
                                       <td>
                                         <asp:TextBox runat="server" ID="txtUserName"></asp:TextBox>
                                         (缺省密码:123456)
                                        </td>
                                        <td>
                                           <asp:DropDownList runat="server" ID="ddlRoles" DataSourceId="odsRoles" DataTextField="Text" DataValueField="Value" AppendDataBoundItems="true">
                                           </asp:DropDownList>                                           
                                         </td>   
                                         <td>
                                           <asp:DropDownList runat="server" ID="ddlDepartments" DataSourceID="odsDepartments" DataTextField="Name" DataValueField="DepartmentId"/>                                           
                                         </td>   
                                        <td>
                                          <asp:LinkButton runat="server" ID="lbInsert" CommandName="Insert" Text="保存"></asp:LinkButton>
                                          &#160;
                                          <asp:LinkButton runat="server" ID="lbCancel" Text="取消" CausesValidation="false" CommandName="Cancel"></asp:LinkButton>
                                        </td>                                                                       
                                  </tr>
                                </InsertItemTemplate>
                            </asp:ListView>                           
                                   
    </div>    
    <!-- 列表区 -->
    </td></tr></table>     
    </div>
    
      <asp:ObjectDataSource runat="server" ID="odsUsers" EnablePaging="True" 
             StartRowIndexParameterName="StartRowIndex" 
             MaximumRowsParameterName="MaxRows" 
             onobjectcreating="odsUsers_ObjectCreating" 
             onselecting="odsUsers_Selecting" SelectCountMethod="GetUsersCount" 
             SelectMethod="GetUsers"  
             InsertMethod="AddUser"           
             TypeName="MCES.Services.UserService" 
            oninserting="odsUsers_Inserting">
     <SelectParameters>
        <asp:Parameter Name="StartRowIndex" Type="Int32" />
        <asp:Parameter Name="MaxRows" Type="Int32" />
     </SelectParameters>
    </asp:ObjectDataSource>  
    
    <asp:ObjectDataSource runat="server" ID="odsRoles"                         
             SelectMethod="GetBizTextValues"  
             TypeName="MCES.Domain.RoleInfo"/>
             
    <asp:ObjectDataSource runat="server" ID="odsDepartments"
    SelectMethod="GetAllDepartments" TypeName="MCES.Services.DepartmentService" 
         onobjectcreating="odsDepartments_ObjectCreating" />            
    
    
</asp:Content>

