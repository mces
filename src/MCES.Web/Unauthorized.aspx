﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Unauthorized.aspx.cs" Inherits="MCES.Web.UnauthorizedPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
      body{ font: 9pt/12pt 宋体 }
      h1{ font: 12pt/15pt 宋体 }
      h2{ font: 9pt/12pt 宋体 }
      a:link { color: red }
      a:visited { color: maroon }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="background-color:InfoBackground; margin-top:0px; margin-left:0px">
      <table cellpadding="0" cellspacing="0" border="0" style="margin-left:4px">
       <tr>
       <td><img src="assets/images/main/warning.gif" alt="warning"/></td>
       <td style="padding-left:4px"><h2>您未被授权查看该页，如果您认为自己应该能够查看该页面，请与系统管理员联系，
       或<a href="javascript:void(parent.location.href='logout.aspx?clrCookie=true')">退出</a>以其他用户名登录。</h2></td>
       </tr>
      </table>
    </div>
    </form>
</body>
</html>
