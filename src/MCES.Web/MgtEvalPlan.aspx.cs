﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MCES.Services;
using MCES.Domain;
using System.Diagnostics;
using System.Collections;
using System.Collections.Specialized;
using Common.Utils;

namespace MCES.Web
{
    public partial class MgtEvalPlanPage : BasePage, IPageAuthorize
    {
        #region  Properties 

        public virtual EvalPlanService EvalPlanService
        {
            internal get;
            set;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //EvalPlan plan = this.EvalPlanService.AddEvalPlan("2008年度评测", DateTime.Today, DateTime.Today.AddDays(21), EvalPlanState.Running);
                //
                //Debug.WriteLine("plan:" + plan + ", id:" + plan.EvalPlanId);
            }
        }

        protected void odsEvalPlans_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = this.EvalPlanService;
        }
        protected void odsEvalPlans_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["params"] = GetParameters();
        }

        private Hashtable GetParameters()
        {
            DataPager pager = (DataPager)lvwEvalPlans.FindControl("pager");
            //
            Hashtable @params = new Hashtable();
            @params["EndIndex"] = pager.StartRowIndex;
            //
            return @params;
        }
        protected void odsEvalPlans_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            HashtableHelper.Copy(this._inputParameters, e.InputParameters, true);
        }

        protected void lbNew_Click(object sender, EventArgs e)
        {
            lvwEvalPlans.EditIndex = -1;
            lvwEvalPlans.InsertItemPosition = InsertItemPosition.FirstItem;
            this.lbNew.Visible = false;
            this.lvwEvalPlans.DataBind();
        }

        private void CloseInsertMode()
        {
            lvwEvalPlans.InsertItemPosition = InsertItemPosition.None;
            this.lvwEvalPlans.DataBind();
            this.lbNew.Visible = true;
        }

        protected void lvwEvalPlans_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            if (e.CancelMode == ListViewCancelMode.CancelingEdit)
            {
                lvwEvalPlans.EditIndex = -1;
                this.lvwEvalPlans.DataBind();
            }
            else if (e.CancelMode == ListViewCancelMode.CancelingInsert)
            {
                CloseInsertMode();
            }
        }

        private OrderedDictionary _inputParameters = new OrderedDictionary();
        //      
        protected void lvwEvalPlans_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
            _inputParameters.Clear();
            //
            string title = ((TextBox)e.Item.FindControl("txtTitle")).Text;
            string startDate = ((TextBox)e.Item.FindControl("txtStartDate")).Text;
            string endDate = ((TextBox)e.Item.FindControl("txtEndDate")).Text;
            //
            //(string title, DateTime startDate, DateTime? endDate, int state)
            _inputParameters["title"] = title;
            _inputParameters["startDate"] = ConvertHelper.TryDateTime(startDate, DateTime.Today);
            _inputParameters["endDate"] = ConvertHelper.TryDateTime(endDate);
            _inputParameters["state"] = EvalPlanState.Running;
        }
        protected void lvwEvalPlans_ItemInserted(object sender, ListViewInsertedEventArgs e)
        {
            CloseInsertMode();
        }
        protected void lvwEvalPlans_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ManageUsers":
                    {
                    } break;
            }
        }
}
}
