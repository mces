﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace MCES.Web
{
    public partial class DefaultPage : BasePage, IPageAuthorizeless
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {                
            }
        }

        protected void NavBar_PreRender(object sender, EventArgs e)
        {
            this.NavBar.SiteMapXmlFile = "~/NavData.xml";
        }

        protected string GetUserInfo()
        {
            return string.Format("{0}/{1}[{2}]", User.Identity.Name, UserInDepartment.Name, Roles.GetRolesForUser()[0]);
        }
    }
}
