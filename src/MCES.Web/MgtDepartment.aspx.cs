﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace MCES.Web
{
    public partial class MgtDepartmentPage : BasePage, IPageAuthorize
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                InitPage();
            }
        }

        protected void InitPage()
        {
            Bind_lvDepartments();
        }

        private void Bind_lvDepartments()
        {
            this.lvDepartments.DataBind();
        }

        protected void CancelInsertMode()
        {
            lvDepartments.InsertItemPosition = InsertItemPosition.None;
            this.lbNew.Visible = true;
            Bind_lvDepartments();
        }


        protected void odsDepartments_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = this.DepartmentService;
        }
        protected void odsDepartments_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {            
            e.InputParameters["params"] = GetParameters();
        }

        private Hashtable GetParameters()
        {
            DataPager pager = (DataPager)lvDepartments.FindControl("pager");
            //
            Hashtable @params = new Hashtable();
            @params["EndIndex"] = pager.StartRowIndex;
            @params["ParentId"] = this.DepartmentService.GetRootDepartment().DepartmentId;
            //
            return @params;
        }
        
        //
        private int _category = 0;
        private string _deptName = string.Empty;
        //
        protected void lvDepartments_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
            //判断以及准备参数,用于 ods_inserting method
            _deptName = (e.Item.FindControl("txtName") as TextBox).Text;
            _category = int.Parse((e.Item.FindControl("ddlCategory") as DropDownList).SelectedValue);         
        }
        protected void lvDepartments_ItemInserted(object sender, ListViewInsertedEventArgs e)
        {            
            this.lvDepartments.InsertItemPosition = InsertItemPosition.None;
            this.lbNew.Visible = true;
            //
            this.Bind_lvDepartments();
        }

        protected void lvDepartments_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
        }
        protected void lvDepartments_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            if (e.CancelMode == ListViewCancelMode.CancelingEdit)
            {
                lvDepartments.EditIndex = -1;
                Bind_lvDepartments();
            }
            else if (e.CancelMode == ListViewCancelMode.CancelingInsert)
            {
                CancelInsertMode();
            }
        }
        protected void lvDepartments_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvDepartments.EditIndex = e.NewEditIndex;
            this.Bind_lvDepartments();     
        }
        protected void odsDepartments_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {            
            var @params = e.InputParameters;
            //Department root, string deptName, int category
            @params["root"] = this.DepartmentService.GetRootDepartment();
            @params["deptName"] = _deptName;
            @params["category"] = _category;
            //
        }
        protected void lbNew_Click(object sender, EventArgs e)
        {
            lvDepartments.EditIndex = -1;
            lvDepartments.InsertItemPosition = InsertItemPosition.FirstItem;
            this.lbNew.Visible = false;
            this.Bind_lvDepartments();
        }
}
}
