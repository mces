﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using MCES.Domain;

namespace MCES.Web
{
    /// <summary>
    /// Summary description for WebApplicationStartup
    /// </summary>
    public static class WebApplicationStartup
    {
        public static void Init()
        {
            //判断系统角色是否存在，不存在则生成之
            foreach (TextValue tv in RoleInfo.GetTextValues())
            {
                if (!Roles.RoleExists(tv.Value))
                {
                    Roles.CreateRole(tv.Value);
                }
            }

            //若角色 SysAdmin 用户不存在, 则创建 admin/12qwaszx
            if (Roles.GetUsersInRole(RoleInfo.SysAdmin).Length == 0)
            {
                MembershipUser user = Membership.CreateUser("admin", "12qwaszx");
                Roles.AddUserToRole(user.UserName, RoleInfo.SysAdmin);
            }
        }
    }
}
