﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.IO;
using System.Text;
using Common.Utils;
using System.ComponentModel;

namespace MCES.Web
{
    /// <summary>
    /// Summary description for WebApplication
    /// </summary>
    public class WebApplication : HttpApplication
    {
        private const string sessionCookie = "ASP.NET_SessionId";

        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private IContainer components = null;

        public WebApplication()
        {
            InitializeComponent();
        }

        #region Web 窗体设计器生成的代码
        /// <summary>
        /// 设计器支持所需的方法 - 不要使用代码编辑器修改
        /// 此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new Container();
        }
        #endregion

        protected void Application_Start(object sender, EventArgs e)
        {
            //log4net.Config.XmlConfigurator.Configure();
            //
            WebApplicationStartup.Init();
        }

        protected void Application_End(object sender, EventArgs e)
        {
            //在应用程序关闭时运行的代码

        }

        [System.Diagnostics.DebuggerHidden]
        protected void Application_Error(object sender, EventArgs e)
        {
            // 在出现未处理的错误时运行的代码
            Exception objErr = Server.GetLastError().GetBaseException();
            string err = "==================================================================\r\n";
            err += "\tSystem Error Report({0:yyyy-MM-dd HH:mm:ss})\r\n";
            err += "==================================================================\r\n";
            err += "Url:  {1}\r\n";
            err += "Client: {2}/{3}\r\n";
            err += "User: {4}\r\n";
            err += "Error Message:{5}\r\n";
            err += "Stack Trace:{6}\r\n";
            err += "\r\n";
            string logMsg = string.Format(err, DateTime.Now, Request.Url, Request.UserHostName, Request.UserHostAddress, User.Identity.Name, objErr.Message, objErr.StackTrace);
            WriteLog(logMsg);

            //支持清除错误信息, ClearErrorEnabled
            if (ConfigHelper.GetConfigBool("ClearErrorEnabled", false))
            {
                Server.ClearError();
            }
            //支持跳转, RedirectErrorPageEnabled
            if (ConfigHelper.GetConfigBool("RedirectErrorPageEnabled", false))
            {
                string defaultErrorPageUrl = ConfigHelper.GetConfig("DefaultErrorPageUrl", "~/ErrorPage.aspx");
                Response.Redirect(defaultErrorPageUrl);
            }
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void WriteLog(string logMsg)
        {
            if (string.IsNullOrEmpty(logMsg))
            {
                return;
            }
            //
            byte[] logMsgBytes = Encoding.UTF8.GetBytes(logMsg);
            //
            string logFileName = string.Format("~/log/{0:yyyyMMdd}.log", DateTime.Now);
            string logFilePath = Server.MapPath(logFileName);
            //
            FileStream fs = null;
            try
            {
                fs = !File.Exists(logFilePath) ? File.Create(logFilePath) : File.OpenWrite(logFilePath);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(logMsgBytes, 0, logMsgBytes.Length);
                fs.Flush();
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs = null;
                }
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            // 在新会话启动时运行的代码

        }

        protected void Session_End(object sender, EventArgs e)
        {
            // 在会话结束时运行的代码。 
            // 注意: 只有在 Web.config 文件中的 sessionstate 模式设置为
            // InProc 时，才会引发 Session_End 事件。如果会话模式设置为 StateServer 
            // 或 SQLServer，则不会引发该事件。

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            //
            if (FormsAuthentication.RequireSSL)
            {
                HttpCookie cookie = Response.Cookies[sessionCookie];
                if (cookie != null)
                {
                    cookie.Secure = true;
                }
                //
                string authCookie = FormsAuthentication.FormsCookieName;
                foreach (string sCookie in Response.Cookies)
                {
                    if (sCookie.Equals(authCookie))
                    {
                        // 設定安全 Cookie瀏覽器只將 Cookie
                        // 傳送至以 https 要求的網頁
                        Response.Cookies[sCookie].Secure = true;
                    }
                }
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            //从验证票据获取Cookie的名字
            string cookieName = FormsAuthentication.FormsCookieName;
            //取得Cookie.
            HttpCookie authCookie = Context.Request.Cookies[cookieName];
            if (null == authCookie)
            {
                return;
            }
            //
            FormsAuthenticationTicket authTicket = null;
            //获取验证票据
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                if (null == authTicket)
                {
                    return;
                }
                //
            }
            catch
            {
                return;
            }

            //验证票据的UserData中存放的是用户角色信息
            //UserData本来存放用户自定义信息。此处用来存放用户角色
            string[] roles = authTicket.UserData.Split(new char[] { ',' });
            FormsIdentity id = new FormsIdentity(authTicket);
            GenericPrincipal principal = new GenericPrincipal(id, roles);

            //把生成的验证票信息和角色信息赋给当前用户．
            Context.User = principal;
        }

    }

}