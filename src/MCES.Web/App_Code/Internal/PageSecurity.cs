﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace MCES.Web
{
    /// <summary>
    /// 需要认证(BasePage 缺省)
    /// </summary>
    public interface IPageAuthorize
    {

    }

    /// <summary>
    /// 不需要认证
    /// </summary>
    public interface IPageAuthorizeless
    {

    }
}
