﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Collections.Specialized;
using System.Collections.Generic;

namespace MCES.Web
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false)]
    public class AutoBindAttribute : System.Attribute
    {
        public AutoBindAttribute()
        {
        }
    }

    public static class AutoBindHelper
    {
        const string _PageAutoBindPropertiesHolder = "!__PageAutoBindPropertiesHolder";

        public static void ExecuteAutoBind(HttpContext ctx, NameValueCollection @params, object target)
        {
            foreach (PropertyInfo pi in GetPageAutoBindProperties(ctx, target.GetType()))
            {
                //从Request中获取数据
                string val = @params[pi.Name];

                //从Session中获取数据
                if (string.IsNullOrEmpty(val))
                {
                    val = ctx.Session[pi.Name] as string;
                }

                if (val != null)
                {
                    ///TODO: 自动类型转化需要优化
                    pi.SetValue(target, Convert.ChangeType(val, pi.PropertyType), new object[] { });
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="pageType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static PropertyInfo[] GetPageAutoBindProperties(HttpContext ctx, Type pageType)
        {
            PropertyInfo[] propInfos = new PropertyInfo[0];
            IDictionary dict = ctx.Application[_PageAutoBindPropertiesHolder] as IDictionary;
            if (dict == null)
            {
                dict = new HybridDictionary(64);
                ctx.Application[_PageAutoBindPropertiesHolder] = dict;
            }
            //
            if (dict.Count > 0 && dict.Contains(pageType))
            {
                propInfos = dict[pageType] as PropertyInfo[];
            }
            else
            {
                propInfos = ResolvePageAutoBindProperties(pageType);
                dict[pageType] = propInfos;
            }
            //
            return propInfos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageType"></param>
        /// <returns></returns>
        internal static PropertyInfo[] ResolvePageAutoBindProperties(Type pageType)
        {
            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.SetProperty | BindingFlags.Public; //| BindingFlags.NonPublic;
            PropertyInfo[] propInfos = pageType.GetProperties(bindingFlags);
            List<PropertyInfo> list = new List<PropertyInfo>();
            foreach (PropertyInfo propInfo in propInfos)
            {
                object[] autoBindAttr = propInfo.GetCustomAttributes(typeof(AutoBindAttribute), true);
                if (autoBindAttr != null && autoBindAttr.Length > 0)
                {
                    list.Add(propInfo);
                }
            }
            //
            return list.ToArray();
        }
    }
}
