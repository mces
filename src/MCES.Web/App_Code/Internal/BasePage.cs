﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Diagnostics;
using Common.Utils;
using System.IO;
using System.Web.Caching;
using System.Xml;

namespace MCES.Web
{
    /// <summary>
    /// BasePage 的摘要说明
    /// </summary>
    public partial class BasePage
    {
        private UserRoleMenuHelper UserRoleMenuHelper;
        //
        [DebuggerHidden()]
        public BasePage()
        {
            this.PreInit += new EventHandler(BasePage_PreInit);
            this.Load += new EventHandler(BasePage_Load);
            this.Error += new EventHandler(BasePage_Error);
        }

        void BasePage_PreInit(object sender, EventArgs e)
        {
            if (!this.DepartmentService.HasRootDepartment())
            {
                this.DepartmentService.AddRootDepartment("南京供电公司");
            }
            //
            if (!CheckUserPermission())
            {
                Server.Transfer("~/Unauthorized.aspx", true);
            }
        }

        [DebuggerHidden()]
        void BasePage_Load(object sender, EventArgs e)
        {
            Anthem.Manager.Register(this);
            //          
            InitAutoBinds();
        }

        [DebuggerHidden()]
        void BasePage_Error(object sender, EventArgs e)
        {
        }


        private bool CheckUserPermission()
        {
            if (typeof(IPageAuthorizeless).IsInstanceOfType(this))//实现IPageAuthorizeless接口
            {
                return true;
            }
            else if (typeof(IPageAuthorize).IsInstanceOfType(this)) //IPageAuhtorize 接口实例
            {
                if (UserRoleMenuHelper == null)
                {
                    this.UserRoleMenuHelper = new UserRoleMenuHelper();
                }
                //              
                return UserRoleMenuHelper.CurPageAuthorized();
            }
            //
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        [DebuggerHidden()]
        protected virtual void InitAutoBinds()
        {
            AutoBindHelper.ExecuteAutoBind(this.Context, Request.Params, this);
        }

        //------------------------------------------------------------------------------------------------
        public void PageOnLoadScriptsBlock(string title, string message)
        {
            _PageOnLoadScriptsBlockCount++;
            //
            string script = "function __PageOnLoadScriptsBlock{0}(){{Ext.MessageBox.alert('{1}','{2}')}}";
            script = string.Format(script, _PageOnLoadScriptsBlockCount, title, message);
            //
            string key = "PageOnLoadScriptsBlock" + _PageOnLoadScriptsBlockCount;
            ClientScript.RegisterClientScriptBlock(this.GetType(), key, script, true);
            //
        }

        public void PageOnLoadScriptsBlock(string title, string message, string callBackScript)
        {
            _PageOnLoadScriptsBlockCount++;
            //
            string script = "function __PageOnLoadScriptsBlock{0}(){{Ext.MessageBox.alert('{1}','{2}',{3})}}";
            script = string.Format(script, _PageOnLoadScriptsBlockCount, title, message, callBackScript);
            //
            string key = "PageOnLoadScriptsBlock" + _PageOnLoadScriptsBlockCount;
            ClientScript.RegisterClientScriptBlock(this.GetType(), key, script, true);
        }
        //
        private int _PageOnLoadScriptsBlockCount = 0;
        //--------------------------------------------------------------------------------------------

        public void PageShowMessage()
        {
            string script = "var msgContainerId='msgContainer'; if($(msgContainerId)){$(msgContainerId).style.display='';}";
            script += "if($(msgContainerId)){setTimeout('$(msgContainerId).style.display=\"none\"',1300)}";
            if (!ClientScript.IsStartupScriptRegistered("pageShowMessage"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "pageShowMessage", script, true);
            }
        }
    }
}
