﻿using System;

namespace MCES.Web
{
    /// <summary>
    /// 用于Anthem 调用后返回
    /// 结果编号, 结果信息
    /// 其中0编号用于成功信息
    /// </summary>
    [Serializable()]
    public class ResultMessage
    {
        /// <summary>
        /// 成功编号
        /// </summary>
        public const int SUCCODE = 0;

        public ResultMessage(int code, string message)
        {
            this.Code = code;
            this.Message = message;
        }

        private int _code;
        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _message = string.Empty;
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

    }
}