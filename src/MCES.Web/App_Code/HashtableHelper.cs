﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Collections.Specialized;

/// <summary>
/// Summary description for HashtableHelper
/// </summary>
public static class HashtableHelper
{
    public static void Copy(IOrderedDictionary src, IOrderedDictionary dest, bool overWrite)
    {
        if (src == null) throw new ArgumentNullException("src");
        if (dest == null) throw new ArgumentNullException("dest");
        //
        foreach (string key in src.Keys)
        {
            if (!dest.Contains(key))
            {
                dest[key] = src[key];
            }
            else if(overWrite)
            {
                dest[key] = src[key];
            }
        }
    }
}
