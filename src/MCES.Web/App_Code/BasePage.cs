﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Utils;
using MCES.Services;
using System.Diagnostics;
using System.Web.UI;
using MCES.Domain;

namespace MCES.Web
{
    /// <summary>
    /// Summary description for BasePage
    /// </summary>
    public partial class BasePage : Spring.Web.UI.Page
    {
     
        public string SystemName
        {
            get
            {
                return ConfigHelper.GetConfig("System:Name", "中层干部测评系统");
            }
        }

        public string SystemVersion
        {
            get
            {
                return ConfigHelper.GetConfig("System:Version", "V1.0");
            }
        }

        
        /// <summary>
        ///  GridView export xls
        /// </summary>
        /// <param name="control"></param>
        public override void VerifyRenderingInServerForm(Control control)
        {

        }
      
        //---------------------------------------------------------------------------------------------------
        // 公共属性

        public virtual DepartmentService DepartmentService
        {
            get;
            set;
        }

        public virtual UserService UserService
        {
            get;
            set;
        }


        private Department _UserInDepartment;
        public virtual Department UserInDepartment
        {
            get
            {
                if (_UserInDepartment == null && User.Identity.IsAuthenticated)
                {
                    _UserInDepartment = this.DepartmentService.GetUserInDepartment(User.Identity.Name);
                }
                return _UserInDepartment;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        protected int ComputeEndIndex(int PageIndex, int PageSize)
        {
            if (PageIndex < 0) { throw new ArgumentOutOfRangeException("PageIndex"); }
            if (PageSize < 0) { throw new ArgumentOutOfRangeException("PageSize"); }
            //
            //
            if (PageSize > 1)
            {
                return (PageIndex * PageSize + 1) / PageSize * PageSize;
            }
            else
            {
                return PageIndex;
            }
        }

        //----------------------------------------------------------------------------------------------------
        //分页 PageCommand
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected virtual void PagerCommand(object sender, System.Web.UI.WebControls.DataPagerCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Next":
                    //  guard against going off the end of the list
                    //到达或超出最后一页时
                    if (e.Item.Pager.StartRowIndex + e.Item.Pager.MaximumRows >= e.Item.Pager.TotalRowCount)
                    {
                        e.NewStartRowIndex = e.Item.Pager.StartRowIndex;
                    }
                    else
                    {
                        e.NewStartRowIndex = Math.Min(e.Item.Pager.StartRowIndex + e.Item.Pager.MaximumRows, e.Item.Pager.TotalRowCount); //- e.Item.Pager.MaximumRows);
                    }
                    //
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "Previous":
                    //  guard against going off the begining of the list
                    e.NewStartRowIndex = Math.Max(0, e.Item.Pager.StartRowIndex - e.Item.Pager.MaximumRows);
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "Last":
                    //  the
                    e.NewStartRowIndex = e.Item.Pager.MaximumRows * (e.Item.Pager.TotalRowCount / e.Item.Pager.MaximumRows);
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "First":
                default:
                    e.NewStartRowIndex = 0;
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
            }
            //
            e.Item.Pager.SetPageProperties(e.NewStartRowIndex, e.NewMaximumRows, true);
        }

    }
}
