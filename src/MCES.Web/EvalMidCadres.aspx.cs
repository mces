﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MCES.Services;
using System.Web.Security;
using System.Collections;

namespace MCES.Web
{
    public partial class EvalMidCadresPage : BasePage, IPageAuthorize
    {
        public virtual EvalService EvalService
        {
            private get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void odsMidCadres_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = this.EvalService;
        }

        protected void odsMidCadres_Selecting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            //int EvalPlanId, string UserName, string RoleName, int DepartmentId, Hashtable @params
            e.InputParameters["EvalPlanId"] = 1;
            e.InputParameters["UserName"] = User.Identity.Name;
            e.InputParameters["RoleName"] = Roles.GetRolesForUser()[0];
            e.InputParameters["DepartmentId"] = UserInDepartment == null ? 0 : UserInDepartment.DepartmentId;
            var @params = new Hashtable();
            if (UserInDepartment != null)
            {
                @params["DepartmentCategory"] = UserInDepartment.Category;
            }
            //
            e.InputParameters["params"] = @params;
        }
    }
}