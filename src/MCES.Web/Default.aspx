﻿<%@ Page Language="C#" Title="::工作区::" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="MCES.Web.DefaultPage" MasterPageFile="~/MasterPage.master" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <link type="text/css" href="assets/styles/splitterStyle.css.aspx" rel="stylesheet"/>
   <link type="text/css" href="assets/styles/navStyle.css.aspx" rel="stylesheet"/>
   <link type="text/css" href="assets/styles/treeStyle.css.aspx" rel="stylesheet"/>
   
   <style type="text/css">
     #header{height:26px; text-align:left; font-family:Tahoma, Verdana, Arial, Helvetica, sans-serif;font-size:13px;border:1px solid gray;padding-top:2px;padding-left:3px;background-color:#fff; color:#000;}
     #copyright{height:22px; text-align:center;font-family:Tahoma,Verdana;font-size:11px;border:1px solid gray;background-color:#fff;color:#000;}
   </style>
   
    <link type="text/css" href="assets/styles/lightwindow.css" rel="stylesheet"/>
    <script type="text/javascript" src="assets/scripts/effects.js"></script>
	<script type="text/javascript" src="assets/scripts/lightwindow.js"></script>
	
	<script type="text/javascript">
	AutoMaxSize = function()
	{
	  var scr_w = screen.availWidth;
      var scr_h = screen.availHeight;   
      window.resizeTo(scr_w, scr_h);
      window.moveTo(0, 0);
	}
	</script>
	<script type="text/javascript">
     Ext.onReady(function() {
	        AutoMaxSize();
	    });
	</script>  
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<script language="javascript" type="text/javascript">
      // Forces the treeview to adjust to the new size of its container          
      function resizeNavBar(DomElementId, NewPaneHeight, NewPaneWidth)
      { 
         //<%=NavBar.ClientID%>.Render();
         <%=Splitter.ClientID%>.Panes[0].Width = <%=NavBar.ClientID%>.Width;
         <%=NavBar.ClientID%>.Render();
      }
      function resizeDetailsContentPane(DomElementId, NewPaneHeight, NewPaneWidth)
      {
          //window.fmWorkplaceHeight = NewPaneHeight - 54;
          window.fmWorkplaceHeight = NewPaneHeight - 42;
          //alert("NewPaneWidth:" + NewPaneWidth);
      }
      function Logout()
      {
         var msg = '&#160;确定退出'+"<%=SystemName%>[<%=SystemVersion%>]"+'？&#160;'
         Ext.MessageBox.confirm('提示',msg,function(btn){
            if(btn == "yes"){               
                window.location.replace('logout.aspx');
            }
         });
      }
      function ShowHelp()
      {
      }
      function ShowWorkplace()
      {
         $('fmWorkplace').src = 'Workplace.htm';
      }
      function NavBarNavigate(itemId,pItemId)
      {
         <%=NavBar.ClientID%>.SelectItemById(pItemId);
         <%=NavBar.ClientID%>.SelectItemById(itemId);
      }
</script>
     <!-- Header go ----------------------------------------------------------------------->
    <table style="width:100%; height:27px;" cellpadding="0" cellspacing="0">
       <tr>
          <td id="header">
          <table cellpadding="0" cellspacing="0" width="100%" border="0">
           <tr><td > &#160;&#160; <asp:Image ID="Image1" runat="server" Visible="false" 
           src="assets/images/favicon.ico" alt="" width="16" height="16"/>&#160;<%=SystemName %>-<%=SystemVersion%>
            <%=GetUserInfo()%>
            &#160;&#160;  <a href="javascript:ShowWorkplace();">主界面</a>
           </td>
           <td align="right" style="padding-right:15px"><span>
           <a href="javascript:ShowHelp();" title="显示帮助"><img src="assets/images/main/icon_help.gif" style="vertical-align:bottom" alt="Help"/>帮助</a>
           &#160;&#160;
           <a href="javascript:Logout();" title="退出系统"><img src="assets/images/main/icon_exit.gif" style="vertical-align:bottom" alt="Logout"/>退出</a>&#160;</span></td>
           </tr>
          </table>
          </td>
        </tr>
       <tr>
          <td style="height:1px;"></td>
        </tr>
      </table>
      <!-- Header no ----------------------------------------------------------------------->      
      <ComponentArt:Splitter runat="server" id="Splitter" FillHeight="true" FillWidth="true" ImagesBaseUrl="~/assets/images/main">
         <Layouts>
          <ComponentArt:SplitterLayout>
            <Panes Orientation="Horizontal" SplitterBarCollapseImageUrl="splitter_horCol.gif" SplitterBarCollapseHoverImageUrl="splitter_horColHover.gif" SplitterBarExpandImageUrl="splitter_horExp.gif" SplitterBarExpandHoverImageUrl="splitter_horExpHover.gif" SplitterBarCollapseImageWidth="5" SplitterBarCollapseImageHeight="116" SplitterBarCssClass="HorizontalSplitterBar" SplitterBarCollapsedCssClass="CollapsedHorizontalSplitterBar" SplitterBarActiveCssClass="ActiveSplitterBar" SplitterBarWidth="5">
              <ComponentArt:SplitterPane PaneContentId="TreeViewContent" Width="185" MinWidth="100" MaxWidth="350" ClientSideOnResize="resizeNavBar" CssClass="SplitterPane" />
              <ComponentArt:SplitterPane PaneContentId="DetailsContent" Width="825" MaxWidth="85%" CssClass="DetailsPane" AllowScrolling="False" ClientSideOnResize="resizeDetailsContentPane"/>
            </Panes>
          </ComponentArt:SplitterLayout>        
        </Layouts>
        <Content>        
          <ComponentArt:SplitterPaneContent id="TreeViewContent">
            <table class="HeadingCell" style="width:100%; height:100%;padding-top:0px; margin-top:0px;" cellpadding="0" cellspacing="0" border="0">          
            <tr><td style="padding-left:0px;padding-top:0px;margin-top:0px;">
            <!-- SiteMapXmlFile="~/NavData.xml" -->
            <ComponentArt:NavBar id="NavBar" Width="100%" Height="100%"
      CssClass="NavBar"
      DefaultItemLookID="TopItemLook"      
      ExpandSinglePath="true"
      FullExpand="true"
      ImagesBaseUrl="~/assets/images/main"
      ShowScrollBar="false"
      ExpandTransition="Fade"
      ExpandDuration="200"
      CollapseTransition="Fade"
      CollapseDuration="200"
      ScrollUpImageUrl="scrollup.gif"
      ScrollUpHoverImageUrl="scrollup_hover.gif"
      ScrollUpActiveImageUrl="scrollup_active.gif"
      ScrollDownImageUrl="scrolldown.gif"
      ScrollDownHoverImageUrl="scrolldown_hover.gif"
      ScrollDownActiveImageUrl="scrolldown_active.gif"
      ScrollUpImageWidth="16"
      ScrollUpImageHeight="16"
      ScrollDownImageWidth="16"
      ScrollDownImageHeight="16"
      EnableViewState = "true"
      runat="server" OnPreRender="NavBar_PreRender">
    <ItemLooks>
      <ComponentArt:ItemLook LookID="TopItemLook" CssClass="TopItem" HoverCssClass="TopItemHover" LeftIconUrl="top_folder.gif" LabelPaddingLeft="15" LeftIconWidth="30" LeftIconHeight="24" />
      <ComponentArt:ItemLook LookID="Level2ItemLook" LabelPaddingLeft="10" CssClass="Level2Item" HoverCssClass="Level2ItemHover" LeftIconWidth="16" LeftIconHeight="16" />
      <ComponentArt:ItemLook LookID="EmptyLook" CssClass="Empty" />
    </ItemLooks>
    <ServerTemplates>
    
        <ComponentArt:NavigationCustomTemplate id="CalendarTemplate">
          <Template>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td style="padding-top:2px;">
                <%--<ComponentArt:Calendar id="Calendar1" runat="server"
                  ControlType="Calendar"
                  CalendarTitleCssClass="title" 
                  DayHeaderCssClass="dayheader" 
                  DayCssClass="day" 
                  DayHoverCssClass="dayhover" 
                  OtherMonthDayCssClass="othermonthday" 
                  SelectedDayCssClass="selectedday" 
                  CalendarCssClass="calendar" 
                  NextPrevCssClass="nextprev" 
                  MonthCssClass="month"
                  SwapSlide="Linear"
                  SwapDuration="200"
                  DayNameFormat="FirstLetter"
                  PrevImageUrl="~/assets/images/main/cal_prevMonth.gif" 
                  NextImageUrl="~/assets/images/main/cal_nextMonth.gif"
                  >
                </ComponentArt:Calendar>--%>
              </td>
            </tr>
            </table>
          </Template>
        </ComponentArt:NavigationCustomTemplate>          
    </ServerTemplates>
    </ComponentArt:NavBar>    
           </td></tr> </table>
          </ComponentArt:SplitterPaneContent>
                   
          <ComponentArt:SplitterPaneContent id="DetailsContent">
		   <iframe id="fmWorkplace" name="fmWorkplace" style="width:100%;height:100%;border:1px solid gray;" 
		   frameborder="0" bordercolor="gray" src="Workplace.htm" scrolling="auto">
		   </iframe>
          </ComponentArt:SplitterPaneContent>
        </Content>          
      </ComponentArt:Splitter>
      
      <!-- Footer go ----------------------------------------------------------------------->
      <table style="width:100%;height:23px;" cellpadding="0" cellspacing="0">
        <tr>
          <td style="height:1px;"></td>
        </tr>
        <tr>
          <td id="copyright">
           &copy; 版权所有(C) 2006-<%=DateTime.Now.Year%>, 保留所有权利。
          </td>
        </tr>
      </table>      
      <!-- Footer no ----------------------------------------------------------------------->     
</asp:Content>