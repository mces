﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Common.Utils;
using System.Diagnostics;

namespace MCES.Web
{
    public partial class MasterPage : BaseMasterPage
    {
        [DebuggerHidden()]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ModifyPageTitle();
            }
        }

        protected string ApplicationPath
        {
            get
            {
                string ap = Page.Request.ApplicationPath;
                ap = ap == "/" ? "" : ap;
                return ap;
            }
        }

        [DebuggerHidden()]
        private void ModifyPageTitle()
        {
            string systemName = ConfigHelper.GetConfig("System:Name", "中层干部测评系统");
            string version = ConfigHelper.GetConfig("System:Version", "V1.0");
            //
            string _title = this.Page.Title;       
            if (string.IsNullOrEmpty(_title) && this.head.HasControls())
            {
                HtmlGenericControl ctrl = this.head.FindControl("title") as HtmlGenericControl;
                _title = ctrl != null ? ctrl.InnerText : string.Empty;                
            }
            //
            this.Page.Title = string.Format("[{0}-{1}]{2}", systemName, version, _title);
        }
    }
}