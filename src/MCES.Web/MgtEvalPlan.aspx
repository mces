﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MgtEvalPlan.aspx.cs" Inherits="MCES.Web.MgtEvalPlanPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  <link rel="stylesheet" type="text/css" href="assets/skins/vista/core.css"/> 
  <script type="text/javascript">
      ManagerUsers = function(evalPlanId) {
      ActivateWindow($('#evalplan' + evalPlanId));
      }
      //
      UpdatePage = function() {
      }
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
      <div>
     <table cellpadding="0"  cellspacing="0" style=" width:100%" border="1">
    <tr><td colspan="2" style="height:24px; padding-left:6px; border-bottom:1px solid gray; border-left:1px solid gray;">
     系统管理&#160;&raquo;&#160;评测设置
    </td></tr>
    <tr>
    <td valign="top" style="border:0px;border-right:gray 1px solid;width:100%">
    <!-- 列表区 -->
     <div id="demo-grid" class="vista-grid" style="width:100%">
                                    <div class="titlebar">                                       
                                      <asp:LinkButton runat="server" ID="lbNew" CommandName="New" Text="新建" 
                                            onclick="lbNew_Click" ToolTip="新建评测计划"/>                                      
                                    </div>
    <asp:ListView ID="lvwEvalPlans" runat="server" DataSourceID="odsEvalPlans" 
                                        onitemcanceling="lvwEvalPlans_ItemCanceling" 
                                        oniteminserted="lvwEvalPlans_ItemInserted" 
                                        oniteminserting="lvwEvalPlans_ItemInserting" 
                                        onitemcommand="lvwEvalPlans_ItemCommand">
                                <LayoutTemplate>
                                    <table class="datatable" cellpadding="0" cellspacing="0">
                                        <tr class="header">
                                            <th>#</th>
                                            <th>名称</th>
                                            <th>开始时间</th>
                                            <th>结束时间</th>
                                            <th>状态</th>
                                            <th>操作</th>
                                        </tr>
                                         <tr id="itemPlaceholder" runat="server" />
                                        </table>
                                    <table class="datapager" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <asp:DataPager ID="pager" runat="server" PageSize="15">
                                                <Fields>
                                                 <asp:TemplatePagerField OnPagerCommand="PagerCommand">
                                                        <PagerTemplate>
                                                            <td class="commands">
                                                                <asp:LinkButton ID="btnFirst" runat="server" CommandName="First" CssClass="first-page page-command" AlternateText="First Page" ToolTip="首页" />
                                                                <asp:LinkButton ID="btnPrevious" runat="server" CommandName="Previous" CssClass="prev-page page-command" AlternateText="Previous Page" ToolTip="前一页" />    
                                                                <asp:LinkButton ID="btnNext" runat="server" CommandName="Next" CssClass="next-page page-command" AlternateText="Next Page" ToolTip="下一页" />
                                                                <asp:LinkButton ID="btnLast" runat="server" CommandName="Last" CssClass="last-page page-command" AlternateText="Last Page" ToolTip="尾页" />                                                                                                           
                                                            </td>
                                                            <td class="info">
                                                                第
                                                                <b>
                                                                    <%# Container.TotalRowCount > 0 ? Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) : 0 %>
                                                                </b>
                                                                /
                                                                <b>
                                                                    <%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows)%>
                                                                </b>
                                                                页 (共<%# Container.TotalRowCount %> 项)  
                                                            </td>                                                          
                                                        </PagerTemplate>
                                                    </asp:TemplatePagerField>
                                                </Fields>
                                            </asp:DataPager>
                                        </tr>                                 
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr class='<%# Container.DisplayIndex % 2 == 0 ? "item" : "altitem" %>'>                                        
                                        <td><%# Eval("EvalPlanId") %></td>
                                        <td><%# Eval("Title")%></td>
                                        <td><%# Eval("StartDate","{0:yyyy-MM-dd}")%> &#160;</td>
                                        <td><%# Eval("EndDate","{0:yyyy-MM-dd}")%> &#160;</td>
                                        <td><%# Eval("StateName")%></td>
                                        <td>
                                           <a id='#evalplan<%#Eval("EvalPlanId")%>' href="MgtEvalPlanUsers.aspx?Action=New" 
                                           class="lightwindow_iframe_link"
                                           params="lightwindow_width=880,lightwindow_height=560,lightwindow_type=external" 
                                           title="设置参与测评的员工"/>
                                          <a href="javascript:ManagerUsers(<%#Eval("EvalPlanId")%>)" title="设置参与测评的员工">参与员工</a>
                                          &#160;
                                          <asp:LinkButton ID="btnEdit" runat="server" Text="编辑" CommandName="Edit" Visible="false"/>
                                          &#160;
                                          <asp:LinkButton ID="btnDelete" runat="server" Text="删除" CommandName="Delete"/>                                         
                                        </td>
                                    </tr>
                                </ItemTemplate>                               
                                <InsertItemTemplate>
                                  <tr class="altitem">            
                                       <td>&#160;</td>                            
                                       <td>
                                         <asp:TextBox runat="server" ID="txtTitle"></asp:TextBox>
                                        </td>
                                        <td>
                                          <asp:TextBox runat="server" ID="txtStartDate" Text='<%#DateTime.Today.ToString("yyyy-MM-dd")%>'>
                                          </asp:TextBox>(如:2009-01-25)
                                          <asp:RegularExpressionValidator runat="server" ID="rvStartDate" ControlToValidate="txtStartDate" ErrorMessage="*"
                                           SetFocusOnError="true" ValidationExpression="^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$"></asp:RegularExpressionValidator>
                                        </td> 
                                        <td>
                                          <asp:TextBox runat="server" ID="txtEndDate"></asp:TextBox>(如:2009-01-25) 
                                          <asp:RegularExpressionValidator runat="server" ID="rvEndDate" ControlToValidate="txtEndDate" ErrorMessage="*"
                                           SetFocusOnError="true" ValidationExpression="^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$"></asp:RegularExpressionValidator>

                                        </td> 
                                        <td>
                                          &#160;
                                        </td>   
                                        <td>
                                          <asp:LinkButton runat="server" ID="lbInsert" CommandName="Insert" Text="保存"></asp:LinkButton>
                                          &#160;
                                          <asp:LinkButton runat="server" ID="lbCancel" Text="取消" CausesValidation="false" CommandName="Cancel"></asp:LinkButton>
                                        </td>                                                                       
                                  </tr>
                                </InsertItemTemplate>
                                <EditItemTemplate>
                                    <tr class='altitem'>                                        
                                        <td><%# Eval("EvalPlanId")%></td>
                                        <td><asp:TextBox runat="server" ID="txtTitle" Text='<%#Eval("Title")%>'></asp:TextBox></td>
                                        <td>
                                          <asp:TextBox runat="server" ID="txtStartDate" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:TextBox> 
                                        </td> 
                                        <td>
                                          <asp:TextBox runat="server" ID="txtEndDate" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:TextBox>
                                        </td> 
                                        <td>
										 &#160;
                                        </td>
                                        <td>
                                              <asp:LinkButton ID="btnSave" runat="server" Text="保存" CommandName="Update" />
                                              &#160;
                                             <asp:LinkButton ID="btnCancel" runat="server" Text="取消" CommandName="Cancel" />
                                        </td>
                                    </tr>                                               
                                </EditItemTemplate>
                            </asp:ListView>                           
             
    </div>
    <!-- 列表区 -->
    </td></tr></table>     
    </div>
    
     <asp:ObjectDataSource runat="server" ID="odsEvalPlans" EnablePaging="True" 
             StartRowIndexParameterName="StartRowIndex" 
             MaximumRowsParameterName="MaxRows" 
             onobjectcreating="odsEvalPlans_ObjectCreating" 
             onselecting="odsEvalPlans_Selecting" SelectCountMethod="GetEvalPlansCount" 
             SelectMethod="GetEvalPlans"  
             InsertMethod="AddEvalPlan"           
             TypeName="MCES.Services.EvalPlanService" 
            oninserting="odsEvalPlans_Inserting">
     <SelectParameters>
        <asp:Parameter Name="StartRowIndex" Type="Int32" />
        <asp:Parameter Name="MaxRows" Type="Int32" />
     </SelectParameters>
    </asp:ObjectDataSource>  
</asp:Content>

