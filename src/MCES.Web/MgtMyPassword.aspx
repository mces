﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MgtMyPassword.aspx.cs" Inherits="MCES.Web.MgtMyPasswordPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" src="assets/scripts/passwordstrength.js"></script>
    <script type="text/javascript">
     ValidNewPassword = function(sender, arguments)
     {
        arguments.IsValid = arguments.Value.length >= 6;
     }
     LogoutAndClearUserNameCookie = function()
     {
         //window.parent.location.href = "logout.aspx?clrCookie=true";
         window.parent.location.href = "login.aspx?cp=true";
     }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    <div>
     <table cellpadding="0"  cellspacing="0" style=" width:100%" border="1">
    <tr><td colspan="2" style="height:24px; padding-left:6px; border-bottom:1px solid gray; border-left:1px solid gray;">
     当前用户&#160;&raquo;&#160;帐号密码
    </td></tr>
    <tr>
    <td valign="top" style="border:0px;border-right:gray 1px solid;width:100%">
    <!-- 列表区 -->
    
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
     <tr>
     <td style="height:24px; padding-top:0px;padding-left:4px; border:1px solid gray;border-left:0px;border-top:0px;" valign="top">

      <table cellpadding="0" cellspacing="2" border="0" style="margin-left:14px; margin-top:4px;margin-bottom:4px">
       <tr><td>原密码:</td><td><anthem:TextBox runat="server" ID="txtPassword" TextMode="Password" Width="160px"/>
           <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPassword"
               ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator></td></tr>
       <tr><td nowrap>新密码:</td><td>
            <script type="text/javascript">
                var ps = new PasswordStrength();
                ps.setSize("200", "20");
                ps.setMinLength(5);
             </script>
             <anthem:TextBox runat="server" ID="txtNewPassword" TextMode="Password" Width="160px" onKeyUp="ps.update(this.value);"/>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNewPassword"
               ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
           <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidNewPassword"
               ControlToValidate="txtNewPassword" Display="Dynamic" ErrorMessage="密码长度至少6" SetFocusOnError="True"></asp:CustomValidator></td></tr>
       <tr><td>确认新密码:</td><td><anthem:TextBox runat="server" ID="txtNewPassword2"  TextMode="Password" Width="160px"/>
           <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtNewPassword2"
               ErrorMessage="*" SetFocusOnError="True" Display="dynamic"></asp:RequiredFieldValidator>
           <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNewPassword"
               ControlToValidate="txtNewPassword2" ErrorMessage="与新密码不一致" SetFocusOnError="True"></asp:CompareValidator>&nbsp;
               </td></tr>
       <tr style="height:1px"><td colspan="2"><hr style="height:1px" /></td></tr>
       <tr><td></td><td><anthem:Button runat="server" ID="btnUpdate" Text="&#160;更新&#160;" OnClick="btnUpdate_Click" /></td></tr>
      </table>      
      
     </td>
     </tr>
     </table>
    
    <!-- 列表区 -->
    </td></tr></table>     
    </div>
</asp:Content>

