<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MgtDepartment.aspx.cs" Inherits="MCES.Web.MgtDepartmentPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <link rel="stylesheet" type="text/css" href="assets/skins/vista/core.css" />     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    <div>
     <table cellpadding="0"  cellspacing="0" style=" width:100%" border="1">
    <tr><td colspan="2" style="height:24px; padding-left:6px; border-bottom:1px solid gray; border-left:1px solid gray;">
     系统管理&#160;&raquo;&#160;部门设置
    </td></tr>
    <tr>
    <td valign="top" style="border:0px;border-right:gray 1px solid;width:100%">
    <!-- 列表区 -->
     <div id="demo-grid" class="vista-grid" style="width:100%">
                                    <div class="titlebar">南京供电公司-部门\科室                                       
                                      <asp:LinkButton runat="server" ID="lbNew" CommandName="New" Text="新建" 
                                            onclick="lbNew_Click" ToolTip="新建下属单位\部门科室"/>                                      
                                    </div>
    <asp:ListView ID="lvDepartments" runat="server" DataSourceID="odsDepartments" 
            oniteminserting="lvDepartments_ItemInserting" 
            onitemcommand="lvDepartments_ItemCommand" 
            onitemcanceling="lvDepartments_ItemCanceling" 
            onitemediting="lvDepartments_ItemEditing" 
            oniteminserted="lvDepartments_ItemInserted">
                                <LayoutTemplate>
                                    <table class="datatable" cellpadding="0" cellspacing="0">
                                        <tr class="header">
                                           <th>#</th>
                                            <th>名称</th>
                                            <th>类别</th>
                                            <th>操作</th>
                                        </tr>
                                         <tr id="itemPlaceholder" runat="server" />
                                        </table>
                                    <table class="datapager" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <asp:DataPager ID="pager" runat="server" PageSize="8">
                                                <Fields>
                                                 <asp:TemplatePagerField OnPagerCommand="PagerCommand">
                                                        <PagerTemplate>
                                                            <td class="commands">
                                                                <asp:LinkButton ID="btnFirst" runat="server" CommandName="First" CssClass="first-page page-command" AlternateText="First Page" ToolTip="首页" />
                                                                <asp:LinkButton ID="btnPrevious" runat="server" CommandName="Previous" CssClass="prev-page page-command" AlternateText="Previous Page" ToolTip="前一页" />    
                                                                <asp:LinkButton ID="btnNext" runat="server" CommandName="Next" CssClass="next-page page-command" AlternateText="Next Page" ToolTip="下一页" />
                                                                <asp:LinkButton ID="btnLast" runat="server" CommandName="Last" CssClass="last-page page-command" AlternateText="Last Page" ToolTip="尾页" />                                                                                                           
                                                            </td>
                                                            <td class="info">
                                                                第
                                                                <b>
                                                                    <%# Container.TotalRowCount > 0 ? Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) : 0 %>
                                                                </b>
                                                                /
                                                                <b>
                                                                    <%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows)%>
                                                                </b>
                                                                页 (共<%# Container.TotalRowCount %> 项)  
                                                            </td>                                                          
                                                        </PagerTemplate>
                                                    </asp:TemplatePagerField>
                                                </Fields>
                                            </asp:DataPager>
                                        </tr>                                 
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr class='<%# Container.DisplayIndex % 2 == 0 ? "item" : "altitem" %>'>                                        
                                        <td><%# Eval("DepartmentId") %></td>
                                        <td><%# Eval("Name") ?? "&nbsp;" %></td>
                                        <td><%# Eval("CategoryName")%></td>
                                        <td>
                                          <asp:LinkButton ID="btnEdit" runat="server" Text="编辑" CommandName="Edit" />
                                          &#160;
                                          <asp:LinkButton ID="btnDelete" runat="server" Text="删除" CommandName="Delete" />
                                        </td>
                                    </tr>
                                </ItemTemplate>                               
                                <InsertItemTemplate>
                                  <tr class="altitem">            
                                       <td>&#160;</td>                            
                                       <td>
                                         <asp:TextBox runat="server" ID="txtName"></asp:TextBox>
                                        </td>
                                        <td>
                                           <asp:DropDownList runat="server" ID="ddlCategory">
                                             <asp:ListItem Value="1" Text="机关"></asp:ListItem>
                                             <asp:ListItem Value="2" Text="基层"></asp:ListItem>
                                           </asp:DropDownList>
                                         </td>   
                                        <td>
                                          <asp:LinkButton runat="server" ID="lbInsert" CommandName="Insert" Text="保存"></asp:LinkButton>
                                          &#160;
                                          <asp:LinkButton runat="server" ID="lbCancel" Text="取消" CausesValidation="false" CommandName="Cancel"></asp:LinkButton>
                                        </td>                                                                       
                                  </tr>
                                </InsertItemTemplate>
                                <EditItemTemplate>
                                    <tr class='altitem'>                                        
                                        <td><%# Eval("DepartmentId")%></td>
                                        <td><asp:TextBox runat="server" ID="txtName" Text='<%#Eval("Name")%>'></asp:TextBox></td>
                                        <td>
                                           <asp:DropDownList runat="server" ID="ddlCategory" SelectedValue='<%#Eval("Category")%>'>
                                            <asp:ListItem Value="1" Text="机关"></asp:ListItem>
                                             <asp:ListItem Value="2" Text="基层"></asp:ListItem>
                                           </asp:DropDownList>
                                        </td>
                                        <td>
                                              <asp:LinkButton ID="btnSave" runat="server" Text="保存" CommandName="Update" />
                                              &#160;
                                             <asp:LinkButton ID="btnCancel" runat="server" Text="取消" CommandName="Cancel" />
                                        </td>
                                    </tr>                                               
                                </EditItemTemplate>
                            </asp:ListView>                           
             
    </div>
    <!-- 列表区 -->
    </td></tr></table>     
    </div>
    
     <asp:ObjectDataSource runat="server" ID="odsDepartments" EnablePaging="True" 
             StartRowIndexParameterName="StartRowIndex" 
             MaximumRowsParameterName="MaxRows" 
             onobjectcreating="odsDepartments_ObjectCreating" 
             onselecting="odsDepartments_Selecting" SelectCountMethod="GetSubDepartmentsCount" 
             SelectMethod="GetSubDepartments"  
             InsertMethod="AddDepartment"           
             TypeName="MCES.Services.DepartmentService" 
            oninserting="odsDepartments_Inserting">
     <SelectParameters>
        <asp:Parameter Name="StartRowIndex" Type="Int32" />
        <asp:Parameter Name="MaxRows" Type="Int32" />
     </SelectParameters>
    </asp:ObjectDataSource>  
    
 
</asp:Content>

