﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MCES.Domain;
using System.Collections;
using System.Collections.Specialized;

namespace MCES.Web
{
    public partial class MgtUserPage : BasePage, IPageAuthorize
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                InitPage();
            }
        }

        private void InitPage()
        {
            //ddlRoles
            this.ddlRoles.DataSource = RoleInfo.GetBizTextValues();
            this.ddlRoles.DataTextField = "Text";
            this.ddlRoles.DataValueField = "Value";
            this.ddlRoles.DataBind();

            //ddlDepartments
            var depts =  this.DepartmentService.GetAllDepartments();
            foreach (Department dept in depts)
            {
                string space = dept.ParentId == 0 ? string.Empty : "—";
                ListItem item = new ListItem(string.Format("{0}{1}[{2}]", space, dept.Name, dept.CategoryName), dept.DepartmentId.ToString());
                this.ddlDepartments.Items.Add(item);
            }
        }

        protected void odsUsers_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = this.UserService;
        }
        protected void odsUsers_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["params"] = GetParameters();
        }
        protected void odsUsers_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            HashtableHelper.Copy(this._inputParameters, e.InputParameters, true);
        }

        private Hashtable GetParameters()
        {
            DataPager pager = (DataPager)lvwUsers.FindControl("pager");
            //
            Hashtable @params = new Hashtable();
            @params["EndIndex"] = pager.StartRowIndex;
            @params["DepartmentId"] = this.ddlDepartments.SelectedValue;
            string roleName = this.ddlRoles.SelectedValue;
            @params["RoleName"] = string.IsNullOrEmpty(roleName) ? null : roleName;
            //
            return @params;
        }
      

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbNew_Click(object sender, EventArgs e)
        {
            lvwUsers.EditIndex = -1;
            lvwUsers.InsertItemPosition = InsertItemPosition.FirstItem;
            this.lbNew.Visible = false;
            this.lvwUsers.DataBind();
        }

        private OrderedDictionary _inputParameters = new OrderedDictionary();
        //
        protected void lvwUsers_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
            _inputParameters.Clear();
            //
            string userName = ((TextBox)e.Item.FindControl("txtUserName")).Text;
            string roleName = ((DropDownList)e.Item.FindControl("ddlRoles")).SelectedValue;
            int departmentId = int.Parse(((DropDownList)e.Item.FindControl("ddlDepartments")).SelectedValue);
            string password = "123456"; //缺省密码
            //
            _inputParameters["userName"] = userName;
            _inputParameters["password"] = password;
            _inputParameters["roleName"] = roleName;
            _inputParameters["departmentId"] = departmentId;
            //
        }
        protected void lvwUsers_ItemInserted(object sender, ListViewInsertedEventArgs e)
        {
            CloseInsertMode();
        }
        protected void odsDepartments_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = this.DepartmentService;
        }
        protected void lvwUsers_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            if (e.CancelMode == ListViewCancelMode.CancelingEdit)
            {
                lvwUsers.EditIndex = -1;
                this.lvwUsers.DataBind();
            }
            else if (e.CancelMode == ListViewCancelMode.CancelingInsert)
            {
                CloseInsertMode();             
            }
        }

        private void CloseInsertMode()
        {
            lvwUsers.InsertItemPosition = InsertItemPosition.None;
            this.lvwUsers.DataBind();
            this.lbNew.Visible = true;
        }

        protected void BindUsers(object sender, EventArgs e)
        {
            this.lvwUsers.DataBind();
        }
        
}
}