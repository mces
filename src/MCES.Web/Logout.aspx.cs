﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common.Utils;

namespace MCES.Web
{
    public partial class LogoutPage : BasePage, IPageAuthorizeless
    {
        private bool _clrCookie = false;
        //
        //[AutoBind]
        public string clrCookie
        {
            set
            {
                if (ConvertHelper.TryBool(value, true))
                {
                    this._clrCookie = true;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (this._clrCookie)
                {
                    //Login 页面设置的Cookie
                    HttpCookie cookie = new HttpCookie(UserNameCookieName);
                    cookie.Expires = DateTime.Now.AddDays(-365);
                    Response.Cookies.Set(cookie);
                }
                //
                FormsAuthentication.SignOut();
                Response.Redirect("~/");
            }
        }


        private string UserNameCookieName
        {
            get
            {
                return string.Format("{0}/{1}", Request.ApplicationPath.ToUpper(), "Login.UserName");
            }
        }
    }
}
