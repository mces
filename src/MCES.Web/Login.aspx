﻿<%@ Page Language="C#" Title="::登录::" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="MCES.Web.LoginPage" MasterPageFile="~/MasterPage.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">   
    <style type="text/css">
    #login{ margin-top:160px;}
    </style>
    <script type="text/javascript">
    IsTopWindow = function()
    {
       if(parent == window)
       {
          return true;
       }
       //
       parent.location.href = window.location.href;
       return false;
    }
    //
    if(! IsTopWindow()) PageOnLoad = null;
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <center>
    <div>
  <!-- border start -->
    <div style="width:320px;" class="x-box-blue" id="login">        
	<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>
    <div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc">
    <!-- border start -->
           <h3 style="margin-bottom:5px;"><%=SystemName%>&#160;<%=SystemVersion%></h3>
			  <div class="x-form-bd" id="container">
                <fieldset>
                    <legend>登录认证</legend>
					<div id="form-ct">					
					 <asp:Login ID="loginCtrl" runat="server" DisplayRememberMe="False" OnAuthenticate="loginCtrl_Authenticate" OnLoginError="loginCtrl_LoginError">
        <LayoutTemplate>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
          <td>          
         <!-- Login Control Template Start --> 
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
         <td nowrap><asp:Label ID="lblLoginName" runat="server" Text="登&nbsp;录&nbsp;名:" /></td>
         <td style="padding-left:5px;white-space:nowrap">                    
         <asp:TextBox ID="UserName" runat="server" Width="180px"  MaxLength="20"/>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserName"
                  Display="static" ErrorMessage="*" EnableClientScript="True"></asp:RequiredFieldValidator>
          </td>        
        </tr>
        <tr>
         <td style="height: 26px;"><asp:Label ID="lblPassword" runat="server" Text="密&nbsp;&nbsp;&nbsp;&nbsp;码:"/></td>
         <td style="height: 26px;padding-left:5px; white-space:nowrap"><asp:TextBox ID="Password"  runat="server" Width="180px" TextMode="Password" MaxLength="16" />
         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Password"
                  Display="Static" ErrorMessage="*" EnableClientScript="True"></asp:RequiredFieldValidator>         
         </td>
        </tr>
         <tr><td colspan="2"><hr size="1" width="100%" /></td></tr>
        <tr><td colspan="2" align="right">
            <asp:Button ID="btnLogin" Text="&nbsp;登&nbsp;录&nbsp;" runat="server" CommandName="Login" Height="22px"/>
        </td></tr>
           <tr style="height:4px"><td colspan="2" style="height:4px"></td></tr>
        </table>
        <!-- Login Control Template End --> 
        </td></tr>
        </table>     
        </LayoutTemplate>
        </asp:Login>
        
					</div>	
				</fieldset>
   			</div> 
    
    
    <!-- border end -->
    </div></div></div>
     <div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>
    <!-- border end -->
    </div>
    </div>
    </center>
</asp:Content>

