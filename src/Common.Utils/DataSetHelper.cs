using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;

namespace Common.Utils
{
    public static class DataSetHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="FieldList"></param>
        /// <returns></returns>
        public  static DataTable CreateTable(string TableName, string FieldList)
        {
            DataTable dt = new DataTable(TableName);
            DataColumn dc;
            string[] Fields = FieldList.Split(',');
            string[] FieldsParts;
            string Expression;
            foreach (string Field in Fields)
            {
                FieldsParts = Field.Trim().Split(" ".ToCharArray(), 3); // allow for spaces in the expression
                // add fieldname and datatype			
                if (FieldsParts.Length == 2)
                {
                    dc = dt.Columns.Add(FieldsParts[0].Trim(), Type.GetType("System." + FieldsParts[1].Trim(), true, true));
                    dc.AllowDBNull = true;
                }
                else if (FieldsParts.Length == 3)  // add fieldname, datatype, and expression
                {
                    Expression = FieldsParts[2].Trim();
                    if (Expression.ToUpper() == "REQUIRED")
                    {
                        dc = dt.Columns.Add(FieldsParts[0].Trim(), Type.GetType("System." + FieldsParts[1].Trim(), true, true));
                        dc.AllowDBNull = false;
                    }
                    else
                    {
                        dc = dt.Columns.Add(FieldsParts[0].Trim(), Type.GetType("System." + FieldsParts[1].Trim(), true, true), Expression);
                    }
                }
                else
                {
                    throw new ArgumentException("Invalid field definition: '" + Field + "'.");
                }
            }
           
            return dt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="FieldList"></param>
        /// <param name="KeyFieldList"></param>
        /// <returns></returns>
        public static DataTable CreateTable(string TableName, string FieldList, string KeyFieldList)
        {
            DataTable dt = CreateTable(TableName, FieldList);
            string[] KeyFields = KeyFieldList.Split(',');
            if (KeyFields.Length > 0)
            {
                DataColumn[] KeyFieldColumns = new DataColumn[KeyFields.Length]; int i;
                for (i = 1; i == KeyFields.Length - 1; ++i)
                {
                    KeyFieldColumns[i] = dt.Columns[KeyFields[i].Trim()];
                }
                dt.PrimaryKey = KeyFieldColumns;
            }
            return dt;  // You do not have to add to DataSet - The CreateTable call does that
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static bool HasRowsInTable(DataSet ds, string tableName)
        {
            if (ds.Tables.Contains(tableName))
            {
                return ds.Tables[tableName].Rows.Count > 0;
            }
            //
            return false;
        }


        /// <summary>
        /// 将DataSet中某表组装成Hashtable
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="tableName"></param>
        /// <param name="rowIndex"></param>
        /// <param name="drState"></param>
        /// <returns></returns>
        public static Hashtable Populate(DataSet ds, string tableName, int rowIndex, DataRowState drState)
        {
            Hashtable map = new Hashtable();
            //
            if (ds == null)
            {
                throw new ArgumentNullException("ds");
            }
            if (rowIndex < 0)
            {
                throw new ArgumentOutOfRangeException("index");
            }
            //
            if (ds.Tables.Count == 0)
            {
                throw new ArgumentException("not a table in the dataset:"+ds.DataSetName);
            }
            //
            string _tableName = string.IsNullOrEmpty(tableName) ? string.Empty : tableName;
            //
            DataTable dt = null;
            //
            if (string.IsNullOrEmpty(_tableName))
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = ds.Tables[_tableName];
            }
            //
            if (dt == null)
            {
                throw new ArgumentException("not find a table named '" + tableName + "' in the dataset.");
            }
            //
            if (dt.Rows.Count == 0)
            {
                throw new ArgumentException("not a rows in the table:'"+ dt.TableName+"'.");
            }
            //
            if(rowIndex > dt.Rows.Count - 1)
            {
                throw new ArgumentOutOfRangeException("rowIndex");
            }
            //
            if (dt.Rows[rowIndex].RowState == drState)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    map.Add(dc.ColumnName, dt.Rows[rowIndex][dc]);
                }
            }
            //
            return map;
        }


        /// <summary>
        ///  从DataTable中第rowIndex行生成Hashtable
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        public static Hashtable Populate(DataTable dt, int rowIndex, out DataRowState drState)
        {
            if (dt == null)
            {
                throw new ArgumentNullException("dt");
            }
            if (dt.Rows.Count == 0)
            {
                throw new ArgumentOutOfRangeException("dt rows count=0.");
            }
            //
            if (rowIndex > dt.Rows.Count - 1) // rowIndex > -1 => 0 > -1
            {
                throw new ArgumentOutOfRangeException("rowIndex");
            }
            //
            Hashtable @params = new Hashtable();
            DataRow dr = dt.Rows[rowIndex];
            drState = dr.RowState;
            //            
            foreach (DataColumn dc in dt.Columns)
            {
                @params.Add(dc.ColumnName,dr[dc]);
            }
            //
            return @params;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colName"></param>
        /// <param name="dfValue"></param>
        /// <param name="converter"></param>
        /// <returns></returns>
        public static T GetData<T>(DataTable dt, int rowIndex, string colName, T dfValue , Converter<object,T> converter)
        {
            if (dt.Rows.Count == 0 || rowIndex < 0 || string.IsNullOrEmpty(colName))
            {
                return dfValue;
            }
            //
            if (rowIndex > dt.Rows.Count - 1)
            {
                return dfValue;
            }
            //
            try
            {                
                return converter(dt.Rows[rowIndex][colName]);
            }catch
            {
                return dfValue;
            }
        }
    }
}
