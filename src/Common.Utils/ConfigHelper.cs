using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Security.Cryptography;

namespace Common.Utils
{
    /// <summary>
    /// ConfigHelper ���ò���Э����
    /// </summary>
    public static class ConfigHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetConfig(string key, string value)
        {
            string cfgVar = null;
            try
            {
                cfgVar = ConfigurationManager.AppSettings[key];
            }
            catch
            {
                return value;
            }
            //
            return cfgVar == null ? value : cfgVar;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfig(string key)
        {
            return GetConfig(key, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool GetConfigBool(string key)
        {
            bool result = false;
            string cfgVal = GetConfig(key);
            if (null != cfgVal && string.Empty != cfgVal)
            {
                try
                {
                    result = bool.Parse(cfgVal);
                }
                catch (FormatException)
                {
                    // Ignore format exceptions.
                }
            }
            return result;
        }

        public static bool GetConfigBool(string key, bool defautValue)
        {
            string cfgVal = GetConfig(key);
            if (string.IsNullOrEmpty(cfgVal))
            {
                return defautValue;
            }
            //
            bool rslt;
            if (bool.TryParse(cfgVal, out rslt))
            {
                return rslt;
            }
            //
            return defautValue;
        }

        public static int GetConfigInt(string p, int defaultValue)
        {
            string configValue = GetConfig(p);
            try
            {
                return int.Parse(configValue);
            }
            catch
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="bProtectedConnString"></param>
        /// <returns></returns>
        public static string UnprotectConnectionString(string connString, bool bProtectedConnString)
        {
            string _connString = connString;
            //
            try
            {
                if (bProtectedConnString)
                {
                    byte[] optionalEntrpy = { };
                    //BASE64 ==> byte[]
                    byte[] encryptedData = Convert.FromBase64String(connString);
                    //
                    byte[] plainData = ProtectedData.Unprotect(encryptedData, optionalEntrpy, DataProtectionScope.CurrentUser);
                    //
                    //byte[] ==> string
                    _connString = Encoding.UTF8.GetString(plainData);
                }
                //
            }
            catch
            {
                _connString = connString;
            }
            //
            return _connString;
        }

        public static string GetConnectionString(string connectionStringName, string defaultConnectionString)
        {
            ConnectionStringSettings css = ConfigurationManager.ConnectionStrings[connectionStringName];
            if(css == null)
            {
                return defaultConnectionString;
            }else
            {
                return css.ConnectionString;
            }
        }
    }
}
