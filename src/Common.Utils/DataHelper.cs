using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Common.Utils
{
    public static class DataHelper
    {
        public static DateTime ToDateTime(object src, DateTime rplObj)
        {
            if (Convert.IsDBNull(src))
            {
                return rplObj;
            }
            else
            {
                try
                {
                    return Convert.ToDateTime(src);
                }
                catch
                {
                    return rplObj;
                }
            }
        }

        public static DateTime? ToDateTime(object src, DateTime? rplObj)
        {
            try
            {
                return new DateTime?(Convert.ToDateTime(src));
            }
            catch
            {
                return rplObj;
            }
        }

        public static Decimal ToDecimal(object src, Decimal rplObj)
        {
            try
            {
                return Convert.ToDecimal(src);
            }
            catch
            {
                return rplObj;
            }
        }

        public static Decimal? ToDecimal(object src, Decimal? rplObj)
        {
            try
            {
                return new Decimal?(Convert.ToDecimal(src));
            }
            catch
            {
                return rplObj;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="src"></param>
        /// <param name="rplObj"></param>
        /// <param name="decimals">舍入的小数位，一般2</param>
        /// <returns></returns>
        public static Decimal? ToDecimal(object src, Decimal? rplObj, int decimals)
        {
            try
            {
                Decimal d = Convert.ToDecimal(src);
                return Decimal.Round(d, decimals);
            }
            catch
            {
                return rplObj;
            }
        }

        public static string ToString(object src, string rplObj)
        {
            try
            {
                return Convert.ToString(src);
            }
            catch
            {
                return rplObj;
            }
        }

        /// <summary>
        /// 将 src 转化为 Int String， 
        /// src 可能为 科学计数法 1.343434E+19
        /// 
        /// </summary>
        /// <param name="src"></param>
        /// <param name="rplObj"></param>
        /// <returns></returns>
        public static string ToIntString(object src, string rplObj)
        {
            try
            {
                double rslt;
                if (double.TryParse(src.ToString(), NumberStyles.Float, new NumberFormatInfo(), out rslt))
                {
                    return rslt.ToString("####");
                }
                else
                {
                    return rplObj;
                }
            }
            catch
            {                
            }
            //
            return rplObj;
        }
    }
}
