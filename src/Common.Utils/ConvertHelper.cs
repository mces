using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Utils
{
    public class ConvertHelper
    {
       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Guid? TryGuid(object obj)
        {
            if (obj == null)
            {
                return null;
            }
            //
            try
            {
                return new Guid(obj.ToString());
            }
            catch { }
            //
            return null;
        }

        public static Guid TryGuid(object obj, Guid defaultGuid)
        {
            Guid? guid = TryGuid(obj);
            return guid == null || !guid.HasValue ? defaultGuid : guid.Value;
        }

        public static Guid[] ToGuidArray(string[] guidStrings)
        {
            if (guidStrings == null || guidStrings.Length == 0)
            {
                return null;
            }
            //
            List<Guid> guids = new List<Guid>();
            //
            foreach (string gs in guidStrings)
            {
                 Guid? g = TryGuid(gs);
                 if (g.HasValue)
                 {
                     guids.Add(g.Value);
                 }
            }
            //
            return guids.Count > 0 ? guids.ToArray() : null;
        }

        public static DateTime? TryDateTime(object obj)
        {
            if (obj == null)
            {
                return null;
            }
            //
            try
            {
                return DateTime.Parse(obj.ToString());
            }
            catch
            {
            }
            return null;
        }

        public static DateTime TryDateTime(object obj, DateTime defaultDateTime)
        {
            DateTime? dt = TryDateTime(obj);
            return dt == null || !dt.HasValue ? defaultDateTime : dt.Value;
        }

        /// <summary>
        /// String 转化(全角/半角:VbStrConv.Wide,Narrow; 
        /// 简体/繁体: VbStrConv.SimplifiedChinese,TraditionalChinese;每个单词首字母大写:VbStrConv.ProperCase)
        /// </summary>
        /// <param name="s"></param>
        /// <param name="vbStrConv"></param>
        /// <returns></returns>
        public static string StrConv(string s, Microsoft.VisualBasic.VbStrConv vbStrConv)
        {
            return Microsoft.VisualBasic.Strings.StrConv(s, vbStrConv, 0);
        }

        /// <summary>
        /// 转换Enum的值或者名称为Enum对象
        /// </summary>
        /// <typeparam name="EnumType"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static EnumType Parse<EnumType>(string value)
        {
            return (EnumType)Enum.Parse(typeof(EnumType), value);
        }

        public static int? TryInt(object obj)
        {
            if (obj == null)
            {
                return null;
            }
            //
            try
            {
                return int.Parse(obj.ToString());
            }
            catch
            {
            }
            return null;
        }

        public static int TryInt(object obj, int defaultInt)
        {
            int? val = TryInt(obj);
            return val == null || !val.HasValue ? defaultInt : val.Value;
        }

        public static long? TryLong(object obj)
        {
            if (obj == null)
            {
                return null;
            }
            //
            try
            {
                long rslt = 0;
                if (long.TryParse(obj.ToString(), out rslt))
                {
                    return rslt;
                }
            }
            catch
            {
            }
            //
            return null;
        }

        public static long TryLong(object obj, long defaultLong)
        {
            long? val = TryLong(obj);
            return val == null || !val.HasValue ? defaultLong : val.Value;
        }


        public static int[] ToIntArray(string[] sIds)
        {
            List<int> ints = new List<int>();
            foreach (string sId in sIds)
            {
               int? oInt = TryInt(sId);
               if (oInt != null && oInt.HasValue)
               {
                   ints.Add(oInt.Value);
               }
            }
            //
            return ints.ToArray();
        }

        /// <summary>
        /// 转化 Dictionary<int,List&lt;int>> 为 string
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public static string DictionaryToString(Dictionary<int, List<int>> dic)
        {
            StringBuilder _sb =new StringBuilder();
            foreach(int key in dic.Keys)
            {
                _sb.Append(key).Append(":");
                foreach(int value in dic[key])
                {
                    _sb.Append(value).Append(",");
                }
                _sb.Append(";");
            }
            //
            return _sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Dictionary<int,List<int>> StringToDictionary(string s)
        {
            Dictionary<int,List<int>> dic = new Dictionary<int, List<int>>();
            //
            if(string.IsNullOrEmpty(s))
            {
                return dic;
            }
            // 1:1,2,;2:1,2,;
            string[] ss0 = s.Split(';');
            foreach(string s0 in ss0)
            {
                if(! string.IsNullOrEmpty(s0))
                {
                    int k = int.Parse(s0.Substring(0, s0.IndexOf(":")));
                    dic[k] = new List<int>();
                    string[] ss1 = s0.Substring(s0.IndexOf(":")+1).Split(',');
                    foreach (string s1 in ss1)
                    {
                        if(! string.IsNullOrEmpty(s1))
                        {
                            dic[k].Add(int.Parse(s1));
                        }
                    }
                }
            }

            //
            return dic;
        }

        public static bool TryBool(object obj, bool defaultVal)
        {
            if (obj == null)
            {
                return defaultVal;
            }
            //
            try
            {
                bool rslt;
                if (bool.TryParse(obj.ToString(), out rslt))
                {
                    return rslt;
                }
            }
            catch
            {
            }
            //
            return defaultVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <param name="defaultVal"></param>
        public static string TryString(object p, string defaultVal)
        {
            try
            {
                if (object.Equals(null, p))
                {
                    return defaultVal;
                }
                //
                return p.ToString();
            }
            catch
            {
                return defaultVal;
            }
        }
    }
}
