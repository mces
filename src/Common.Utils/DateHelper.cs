using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Common.Utils
{
    /// <summary>
    /// 日期帮助类
    /// </summary>
    public static class DateHelper
    {
        public static string GetWeekName(DateTime dt)
        {
            //
            if (Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName.IndexOf("zh") == -1)
            {
                return string.Format("{0}", dt.DayOfWeek);
            }
            //
            string weekName = "星期{0}";

            #region Weeks
            switch (dt.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    {
                        return string.Format(weekName, "一");
                    }
                case DayOfWeek.Tuesday:
                    {
                        return string.Format(weekName, "二");
                    }
                case DayOfWeek.Wednesday:
                    {
                        return string.Format(weekName, "三");
                    }
                case DayOfWeek.Thursday:
                    {
                        return string.Format(weekName, "四");
                    }
                case DayOfWeek.Friday:
                    {
                        return string.Format(weekName, "五");
                    }
                case DayOfWeek.Saturday:
                    {
                        return string.Format(weekName, "六");
                    }
                case DayOfWeek.Sunday:
                    {
                        return string.Format(weekName, "日");
                    }
            }
            #endregion

            return string.Empty;
        }


        /// <summary>
        ///  获得缺省时间
        /// </summary>
        /// <param name="yOrymOrymd">4位年份,年份-月份，年份-月份-日期</param>
        /// <returns></returns>
        public static DateTime GetDefaultDateTime(string yOrymOrymd)
        {
            if (string.IsNullOrEmpty(yOrymOrymd))
            {
                throw new ArgumentNullException("yOrymOrymd");
            }
            //
            string sYear, sMonth = "01", sDate = "01";
            //
            string[] ss = yOrymOrymd.Split('-', '/', ' ');
            sYear = ss[0];
            //
            if (ss.Length > 1) sMonth = ss[1];
            if (ss.Length > 2) sDate = ss[2];
            //
            string sDateTime = string.Format("{0}-{1}-{2}", sYear, sMonth, sDate);
            DateTime d;
            if (DateTime.TryParse(sDateTime, out d))
            {
                return d;
            }
            //
            throw new ArgumentNullException("yOrymOrymd");         
        }

        /// <summary>
        /// 空值总小于非空值
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static DateTime? Max(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null && endDate == null)
            {
                return null;
            }
            //
            if (startDate == null || endDate == null)
            {
                return startDate == null ? endDate : startDate;
            }
            //
            return new DateTime(Math.Max(startDate.Value.Ticks, endDate.Value.Ticks));
        }

        /// <summary>
        /// 空值总小于非空值
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static DateTime? Min(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null && endDate == null)
            {
                return null;
            }
            //
            if (startDate == null || endDate == null)
            {
                return startDate == null ? startDate : endDate;
            }
            //
            return new DateTime(Math.Min(startDate.Value.Ticks, endDate.Value.Ticks));
        }

    }
}
