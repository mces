using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Reflection;

namespace Common.Utils
{
    public static class ObjectUtils
    {
        public static T BuilderObject<T>(IDictionary paramters)
        {         
            T obj = Activator.CreateInstance<T>();
            Type t = typeof(T);
            object value = null;
            foreach (PropertyInfo pi in t.GetProperties())
            {
                if (paramters.Contains(pi.Name))
                {
                    value = paramters[pi.Name];
                    try
                    {
                        if (pi.PropertyType.Equals(typeof(Guid)))
                        {
                            value = new Guid(value.ToString());
                        }
                        else
                        {
                            value = Convert.ChangeType(value, pi.PropertyType);
                        }
                        //
                        pi.SetValue(obj, value, new object[] { });
                    }
                    catch { }
                }
            }
            //            
            return obj;
        }

        public static void UpdateObject<T>(T obj, IDictionary paramters)
        {
            //T obj = Activator.CreateInstance<T>();
            Type t = typeof(T);
            object value = null;
            foreach (PropertyInfo pi in t.GetProperties())
            {
                if (paramters.Contains(pi.Name))
                {
                    value = paramters[pi.Name];
                    try
                    {
                        if (pi.PropertyType.Equals(typeof(Guid)))
                        {
                            value = new Guid(value.ToString());
                        }
                        else
                        {
                            value = Convert.ChangeType(value, pi.PropertyType);
                        }
                        //
                        pi.SetValue(obj, value, new object[] { });
                    }
                    catch { }
                }
            }
        }
    }
}
