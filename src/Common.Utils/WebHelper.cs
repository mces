using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;
using System.Security.Cryptography;
using System.Globalization;
using System.Reflection;

namespace Common.Utils
{
    public static class WebHelper
    {
        public static string GetFullWebAppUrl(string url)        
        {
            string url0 = HttpContext.Current.Request.ApplicationPath; // /WebAppName 或者 /
            string s = url.StartsWith(url0,true,CultureInfo.InvariantCulture) 
                ? url : string.Format("{0}/{1}", url0, url.StartsWith("/") ? url.Substring(1) : url);
            return s.StartsWith("//") ? s.Substring(1) : s;
        }

        public static void SetControlValue(Page page, string ctlUniqueId, string value)
        {
            Control ctl = page.FindControl(ctlUniqueId);
            if (ctl == null)
            {
                throw new ArithmeticException("ctlUniqueId");
            }
            //
            if (ctl is HtmlControl)
            {
                HtmlControl hctl = ctl as HtmlControl;
                hctl.Attributes["value"] = value;
                return;
            }
            //
            if (ctl is WebControl)
            {
                if (ctl is TextBox)
                {
                    TextBox box = ctl as TextBox;
                    if (box.TextMode == TextBoxMode.Password)
                    {
                        box.Attributes["value"] = value;
                    }
                    else
                    {
                        box.Text = value;
                    }
                    return;
                }
                //
                if (ctl is DropDownList)
                {
                    DropDownList ddl = ctl as DropDownList;
                    ListItem item = ddl.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddl.SelectedIndex = -1;
                        item.Selected = true;
                    }
                    return;
                }
            }
            //
            throw new NotSupportedException(string.Format("{0}'s webcontrol not support", ctlUniqueId));
        }

        public static string TruncateString(string oriStr, int needLength, string trailStr)
        {
            if (string.IsNullOrEmpty(oriStr))
            {
                return oriStr;
            }
            //
            if (oriStr.Length > needLength)
            {
                return new StringBuilder(oriStr.Substring(0, needLength)).Append(string.IsNullOrEmpty(trailStr) ? string.Empty : trailStr).ToString();
            }
            //
            return oriStr;
        }

        //------------------------------------------------------------------------------------------------------
        #region Download File Method(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fs"></param>
        /// <param name="downloadFileName"></param>
        public static void DownloadFile(Stream fs, string downloadFileName)
        {
            HttpContext context = HttpContext.Current;
            Byte[] buffer = new Byte[10000];
            int ReadLength;
            long TotleLength;

            try
            {
                if (fs == null)
                {
                    throw new Exception("stream is null.");
                }

                //string filename = Path.GetFileName(fullFilePath);
                //fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);

                TotleLength = fs.Length;
                context.Response.ContentType = "application/octec-stream";
                context.Response.AddHeader("Content-Disposition", String.Concat("attachment;filename=", HttpUtility.UrlEncode(downloadFileName)));
                //             
                long p = 0;
                if (context.Request.Headers["Range"] != null)
                {
                    context.Response.StatusCode = 206;
                    p = long.Parse(context.Request.Headers["Range"].Replace("bytes=", "").Replace("-", ""));
                }
                if (p != 0)
                {
                    context.Response.AddHeader("Content-Range", "bytes " + p.ToString() + "-" + ((long)(TotleLength - 1)).ToString() + "/" + TotleLength.ToString());
                }
                context.Response.AddHeader("Content-Length", ((long)(TotleLength - p)).ToString());
                fs.Position = p;
                TotleLength = TotleLength - p;

                while (TotleLength > 0)
                {
                    if (context.Response.IsClientConnected)
                    {
                        ReadLength = fs.Read(buffer, 0, 10000);
                        context.Response.OutputStream.Write(buffer, 0, ReadLength);
                        context.Response.Flush();
                        buffer = new Byte[10000];
                        TotleLength -= ReadLength;
                    }
                    else
                    {
                        TotleLength = -1;
                    }
                }
                //
                context.Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }                
            }
        }

        /// <summary>
        /// 下载文件(TransmitFile)
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="fullFilePath"></param>
        /// <param name="downloadFileName"></param>
        public static void TransmitFile(HttpContext ctx, string fullFilePath, string downloadFileName)
        {
            try
            {
                if (!File.Exists(fullFilePath))
                {
                    throw new ArgumentException(string.Concat("fullFilePath:", fullFilePath, " not found."));
                }
                //
                ctx.Response.ContentType = "application/octec-stream";
                ctx.Response.AddHeader("Content-Disposition", String.Concat("attachment; filename=", HttpUtility.UrlEncode(downloadFileName)));
                //
                if (!string.IsNullOrEmpty(ctx.Request["cache"]) && !string.IsNullOrEmpty(ctx.Request["maxage"]))
                {
                    int maxage = ConvertHelper.TryInt(ctx.Request["maxage"], 3600);
                    //
                    ctx.Response.Cache.SetCacheability(HttpCacheability.Public);
                    ctx.Response.AddFileDependency(fullFilePath);
                    ctx.Response.Cache.SetETagFromFileDependencies();
                    ctx.Response.Cache.SetLastModifiedFromFileDependencies();
                    ctx.Response.Cache.SetExpires(DateTime.Now.Add(new TimeSpan(0, 0, maxage)));
                    //
                    try
                    {
                        FieldInfo maxAge = ctx.Response.Cache.GetType().GetField("_maxAge", BindingFlags.Instance | BindingFlags.NonPublic);
                        maxAge.SetValue(ctx.Response.Cache, new TimeSpan(0, 0, maxage));
                    }
                    catch
                    {
                        ctx.Response.Headers.Add("max-age", new TimeSpan(0, 0, maxage).Seconds.ToString());
                    }
                    //
                    ctx.Response.Cache.AppendCacheExtension("must-revalidate, proxy-revalidate");
                }               
                //
                ctx.Response.TransmitFile(fullFilePath);
                ctx.ApplicationInstance.CompleteRequest();
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// 计算ComputerBufferSize
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="fullFilePath"></param>
        /// <returns></returns>
        public delegate int ComputeBufferSizeHandler(HttpContext ctx, string fullFilePath);

        private static int DefaultComputeBufferSize(HttpContext ctx, string fullFilePath)
        {
            const int DEFAULT_BLOCKSIZE = 32 * 1024;
            return DEFAULT_BLOCKSIZE;
        }
      
        /// <summary>
        /// 下载文件V2
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="fullFilePath"></param>
        /// <param name="downloadFileName"></param>
        /// <param name="cbsHandler"></param>
        public static void DownloadFileEx(HttpContext ctx, string fullFilePath, string downloadFileName, ComputeBufferSizeHandler cbsHandler)
        {
            cbsHandler = cbsHandler ?? new  ComputeBufferSizeHandler(DefaultComputeBufferSize);
            //       
            byte[] buffer;
            int blockSize;
            //
            try
            {
                if (!File.Exists(fullFilePath))
                {
                    throw new ArgumentException(string.Concat("fullFilePath:", fullFilePath, " not found."));
                }
                //
                long totleLength = new FileInfo(fullFilePath).Length;
                ctx.Response.ContentType = "application/octec-stream";
                ctx.Response.AddHeader("Content-Disposition", String.Concat("attachment; filename=", HttpUtility.UrlEncode(downloadFileName)));
                //
                long p = 0;
                string range = ctx.Request.Headers["Range"];
                if (! string.IsNullOrEmpty(range))
                {
                    ctx.Response.StatusCode = 206;
                    p = long.Parse(range.Replace("bytes=", "").Replace("-", ""));
                }
                //
                if (p != 0)
                {
                    ctx.Response.AddHeader("Content-Range", "bytes " + p.ToString() + "-" + ((long)(totleLength - 1)).ToString() + "/" + totleLength.ToString());
                }
                ctx.Response.AddHeader("Content-Length", ((long)(totleLength - p)).ToString());
                //---------------------------------------------------------------------------------------------------
                //cache
                if (!string.IsNullOrEmpty(ctx.Request["cache"]) && !string.IsNullOrEmpty(ctx.Request["maxage"]))
                {
                    int maxage = ConvertHelper.TryInt(ctx.Request["maxage"], 3600);
                    //
                    ctx.Response.Cache.SetCacheability(HttpCacheability.Public);
                    ctx.Response.AddFileDependency(fullFilePath);
                    ctx.Response.Cache.SetETagFromFileDependencies();
                    ctx.Response.Cache.SetLastModifiedFromFileDependencies();
                    ctx.Response.Cache.SetExpires(DateTime.Now.Add(new TimeSpan(0, 0, maxage)));
                    //
                    try
                    {
                        FieldInfo maxAge = ctx.Response.Cache.GetType().GetField("_maxAge", BindingFlags.Instance | BindingFlags.NonPublic);
                        maxAge.SetValue(ctx.Response.Cache, new TimeSpan(0, 0, maxage));
                    }
                    catch
                    {
                        ctx.Response.Headers.Add("max-age", new TimeSpan(0, 0, maxage).Seconds.ToString());
                    }
                    //
                    ctx.Response.Cache.AppendCacheExtension("must-revalidate, proxy-revalidate");
                }               


                //---------------------------------------------------------------------------------------------------
                totleLength = totleLength - p;
                int readLength = 0;
                //
                using (FileStream fs = File.OpenRead(fullFilePath))
                {
                    fs.Position = p;
                    while (totleLength > 0 && ctx.Response.IsClientConnected)
                    {
                        blockSize = cbsHandler(ctx, fullFilePath);
                        buffer = new byte[blockSize];
                        readLength = fs.Read(buffer, 0, blockSize);
                        if (readLength == 0)
                        {
                            totleLength = -1;
                        }
                        else
                        {
                            ctx.Response.OutputStream.Write(buffer, 0, readLength);
                            ctx.Response.Flush();
                            totleLength -= readLength;
                        }                        
                    }
                    //
                    fs.Close();
                }
                //
                ctx.ApplicationInstance.CompleteRequest();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="fullFilePath"></param>
        /// <param name="downloadFileName"></param>
        public static void DownloadFile(System.Web.UI.Page Page, string fullFilePath, string downloadFileName)
        {
            int blockSize = 16 * 1024; //blockSize 16K
            HttpContext context = HttpContext.Current;
            Byte[] buffer = new Byte[blockSize];
            int ReadLength;
            long TotleLength;
            Stream fs = null;

            try
            {
                if (!File.Exists(fullFilePath))
                {
                    throw new Exception("file were not found.");
                }

                string filename = Path.GetFileName(fullFilePath);

                fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);

                TotleLength = fs.Length;
                context.Response.ContentType = "application/octec-stream";
                context.Response.AddHeader("Content-Disposition", String.Concat("attachment;filename=", HttpUtility.UrlEncode(downloadFileName)));

                long p = 0;
                if (context.Request.Headers["Range"] != null)
                {
                    context.Response.StatusCode = 206;
                    p = long.Parse(context.Request.Headers["Range"].Replace("bytes=", "").Replace("-", ""));
                }
                if (p != 0)
                {
                    context.Response.AddHeader("Content-Range", "bytes " + p.ToString() + "-" + ((long)(TotleLength - 1)).ToString() + "/" + TotleLength.ToString());
                }
                context.Response.AddHeader("Content-Length", ((long)(TotleLength - p)).ToString());
                fs.Position = p;
                TotleLength = TotleLength - p;

                while (TotleLength > 0)
                {
                    if (context.Response.IsClientConnected)
                    {
                        ReadLength = fs.Read(buffer, 0, blockSize);
                        context.Response.OutputStream.Write(buffer, 0, ReadLength);
                        context.Response.Flush();
                        buffer = new Byte[blockSize];
                        TotleLength -= ReadLength;
                    }
                    else
                    {
                        TotleLength = -1;
                    }
                }
                //
                context.Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }             
            }
        }

        #endregion

        public static void CreateSafeSerialCookies(HttpContext ctx, string cookieName, string cookieValue, string secPassword, SymmetricAlgorithm sa)
        {
            if (ctx == null)
            {
                throw new ArgumentNullException("ctx");
            }
            if (string.IsNullOrEmpty(cookieName))
            {
                throw new ArgumentNullException("cookieName");
            }
            if (secPassword == null)
            {
                throw new ArgumentNullException("secPassword");
            }            
            //
            byte[] inBytes = Encoding.UTF8.GetBytes(cookieValue);
            //
            byte[] keys = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(secPassword));
            Array.Copy(keys, 0, sa.Key, 0, Math.Min(keys.Length, sa.KeySize / 8));
            //
            byte[] outBytes = null;
            using (ICryptoTransform trans = sa.CreateEncryptor())
            {
                outBytes = trans.TransformFinalBlock(inBytes, 0, inBytes.Length);
            }
            //最大 4*1024 bytes 一组
            outBytes = Encoding.UTF8.GetBytes(Convert.ToBase64String(outBytes));
            //
            int cookieValueSize = 4 * 1024; //安全
            //
            int size = outBytes.Length % cookieValueSize == 0 ? outBytes.Length / cookieValueSize : outBytes.Length / cookieValueSize + 1;
            if (size > 20) //每个Web Application最多20个Cookie
            {
                throw new ArgumentNullException("cookieValue encrypted too large, every web appliction cookie size less than 20.");
            }
            //
            byte[] rBytes = null;
            for (int i = 0; i < size; i++)
            {
                rBytes = new byte[Math.Min(outBytes.Length - i * cookieValueSize, cookieValueSize)];
                Array.Copy(outBytes, i * cookieValueSize, rBytes, 0, rBytes.Length);               
                string value = Encoding.UTF8.GetString(rBytes);
                string name = string.Format("{0}.{1}",cookieName, i);            
                HttpCookie cookie = new HttpCookie(name, value);
                cookie.HttpOnly = true;
                cookie.Expires = DateTime.Now.AddYears(1);
                ctx.Response.Cookies.Set(cookie);
            }
            //
        }

        /// <summary>
        /// 若SerialCookies 不存在, 或者 CookieVlaues无效, 或者 解密失败, 则返回string.Empty
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="cookieName"></param>
        /// <param name="secPassword"></param>
        /// <param name="sa"></param>
        /// <returns></returns>
        public static string GetSafeSerialCookiesValue(HttpContext ctx, string cookieName, string secPassword, SymmetricAlgorithm sa)
        {
            if (ctx == null)
            {
                throw new ArgumentNullException("ctx");
            }
            if (string.IsNullOrEmpty(cookieName))
            {
                throw new ArgumentNullException("cookieName");
            }
            if (secPassword == null)
            {
                throw new ArgumentNullException("secPassword");
            }         
            //
            StringBuilder _sb =new StringBuilder();
            for (int i = 0; i < 20; i++) //每个Web Application最多20个Cookie
            {
                string key = string.Format("{0}.{1}", cookieName, i);
                HttpCookie cookie = ctx.Request.Cookies[key];
                if (cookie == null) break;
                //
                _sb.Append(cookie.Value);
            }
            //
            if (_sb.Length == 0)
            {
                return string.Empty;
            }
            //
            byte[] inBytes = null;
            try
            {
                inBytes = Convert.FromBase64String(_sb.ToString());
                //
                byte[] keys = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(secPassword));
                Array.Copy(keys, 0, sa.Key, 0, Math.Min(keys.Length, sa.KeySize / 8));
                //
                byte[] outBytes  = null;
                using (ICryptoTransform trans = sa.CreateDecryptor())
                {
                    outBytes = trans.TransformFinalBlock(inBytes, 0, inBytes.Length);
                }
                //
                return Encoding.UTF8.GetString(outBytes);
            }
            catch
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="curPage"></param>
        /// <param name="webCtrl"></param>
        /// <param name="xlsFileName"></param>
        public static void ExportAsExcel(Page curPage, Control webCtrl, string xlsFileName)
        {
            if (curPage == null)
            {
                throw new ArgumentNullException("curPage");
            }
            if (webCtrl == null)
            {
                throw new ArgumentNullException("webCtrl");
            }
            //
            xlsFileName = string.IsNullOrEmpty(xlsFileName) ? "Export" : xlsFileName;
            xlsFileName = Path.GetFileNameWithoutExtension(xlsFileName) + ".xls";
            xlsFileName = HttpUtility.UrlEncode(xlsFileName, Encoding.UTF8);
            //            
            HttpResponse Response = curPage.Response;
            //
            //
            curPage.EnableViewState = false;
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
            webCtrl.RenderControl(htw);
            curPage.EnableViewState = true;
            //
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = System.Text.Encoding.UTF8.WebName;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + xlsFileName); //导出为excel格式 
            Response.ContentType = "application/vnd.ms-excel";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>");
            Response.Write(sw.ToString());
            Response.End();
        }  
    }
}
